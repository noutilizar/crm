import React, { Component } from "react";
import Tabs, { TabPane } from "rc-tabs";
import TabContent from "rc-tabs/lib/TabContent";
import Grid from "../Utils/Grid";
import ScrollableInkTabBar from "rc-tabs/lib/ScrollableInkTabBar";
import { standardActions } from "../Utils/Grid/StandardActions";
import { FormSection } from "redux-form";

import "../../../../style/fuentes.css";
import "./budgetstyle.css";
import BudgetForm from "./BudgetForm";
import IntegrationForm from "./IntegrationForm";
import { renderField } from "../Utils/renderField/renderField";
import { api } from "../../../utility/api";

// const getConfig = (search) => {
//     let configurations = [];
//     return api
//         .get("configurations", { search })
//         .then((response) => {
//             configurations = response.results.map((config) => ({
//                 value: config.id,
//                 week_for_month: config.week_for_month,
//                 hour_for_week: config.hour_for_week,
//             }));

//             return configurations;
//         })
//         .catch((err) => {
//             return configurations;

//         });
// };

export default class BudgetNew extends Component {
    state = { week_for_month: 0, hour_for_week: 0 };
    componentWillMount = () => {
        const { match, detalle } = this.props;
        if (match.params.id) {
            const id = match.params.id;
            detalle(id);
        }
    };
    render() {
        const {
            data,
            setAceptado,
            onSubmit,
            SetCommissionPercentage,
            commission_percentage,
            SetVendedorPercentage,
            vendedor_percentage,
            SetSprint,
            sprint,
            SetDesigner,
            SetProgrammer,
            SetTotal_HistoryPoints,
            SetTotal_HistoryPoints_Error,
            history_points_error,
            total_historypoints,
            programmer,
            designer,
            actualizar,
            detalle,
            match,
            location,
        } = this.props;
        const { week_for_month, hour_for_week } = this.state;
        const fn = match.params.id ? actualizar : onSubmit;
        const isActualizar = match.params.id ? true : false;
        return (
            <div>
                <br />
                <h4 className="anual_goals_nueva ">
                    {isActualizar
                        ? location.pathname.includes("ver")
                            ? "VER PRESUPUESTO"
                            : "ACTUALIZAR PRESUPUESTO"
                        : "NUEVO PRESUPUESTO"}
                </h4>
                <div className="budget-div1">
                    {/* <BudgetForm
                        onSubmit={onSubmit}
                        SetCommissionPercentage={SetCommissionPercentage}
                        SetVendedorPercentage={SetVendedorPercentage}
                        SetSprint={SetSprint}
                        SetDesigner={SetDesigner}
                        SetProgrammer={SetProgrammer}
                    /> */}

                    <div className="budget-div">
                        <div className="uk-flex div-cards">
                            <div className="budget-cards uk-width-1-3">
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Comision canal de venta
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        {commission_percentage}%
                                    </label>
                                </div>
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Total comision canal
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        000.00
                                    </label>
                                </div>
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Meses del proyecto
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        {(total_historypoints /
                                            sprint /
                                            (Number(designer) +
                                                Number(programmer)) /
                                            week_for_month) |
                                            0}
                                    </label>
                                </div>
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Total de horas de proyecto
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        00
                                    </label>
                                </div>
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Total de horas aadministrativas
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        00
                                    </label>
                                </div>
                            </div>

                            <div className="budget-cards uk-width-1-3">
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Comision vendedor
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        {vendedor_percentage}%
                                    </label>
                                </div>
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Total comision vendedor
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        000.00
                                    </label>
                                </div>
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Total de semanas margen de error
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        00
                                    </label>
                                </div>
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Total de horas margen de error
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        00
                                    </label>
                                </div>
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Total de semanas administrativas
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        00
                                    </label>
                                </div>
                            </div>

                            <div className="budget-cards uk-width-1-3">
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Total de tiempo estimado
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        {(total_historypoints / sprint) | 0}
                                    </label>
                                </div>
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Semanas del proyecto
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        {(total_historypoints /
                                            sprint /
                                            (Number(designer) +
                                                Number(programmer))) |
                                            0}
                                    </label>
                                </div>
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Precio base desarrollo
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        00
                                    </label>
                                </div>
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Gastos administrativos
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        00
                                    </label>
                                </div>
                                <div className="uk-child-width-1-2@s uk-grid">
                                    <label className="presupuesto_subtitle">
                                        Total de meses administrativos
                                    </label>
                                    <label className="presupuesto_subtitle_number">
                                        00
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="uk-child-width-1-2@s uk-grid budget-button-div">
                        <div className="budget-form-input-div">
                            <div>
                                <label className="presupuesto_subtitle_number uk-width-1-2">
                                    Duracion total del proyecto
                                </label>
                                <label className="presupuesto_subtitle uk-width-1-2 ">
                                    00
                                </label>
                            </div>
                        </div>
                        <div className="budget-form-input-div">
                            <label className="presupuesto_subtitle_number uk-width-1-2">
                                Precio total
                            </label>
                            <label className="presupuesto_subtitle uk-width-1-2">
                                00
                            </label>
                        </div>
                    </div>

                    <div>
                        <IntegrationForm
                            setAceptado={setAceptado}
                            SetCommissionPercentage={SetCommissionPercentage}
                            SetVendedorPercentage={SetVendedorPercentage}
                            SetSprint={SetSprint}
                            SetDesigner={SetDesigner}
                            SetProgrammer={SetProgrammer}
                            onSubmit={fn}
                            actualizar={!!match.params.id}
                            ver={location.pathname.includes("ver") && true}
                        />
                    </div>
                    <div>
                        {location.pathname.includes("ver") ? (
                            <div></div>
                        ) : (
                            <div>
                                {/* Botones */}
                                <div className="form-budget-buttons">
                                    <div className="budgetf1 button-container1 uk-display-inline button-cancelar1">
                                    <a
                            className="uk-button uk-button-secondary uk-button-small uk-border-rounded "
                            type="button"
                            href="#/presupuestos"
                        >
                            Cancelar
                        </a>
                                    </div>
                                    <div className="budgetf2 button-container1 uk-display-inline  button-guardar1">
                                        <button
                                            className="uk-button uk-button-primary uk-button-small uk-border-rounded uk-margin-left "
                                            form="budget-form"
                                            type="submit"
                                            onClick={fn}
                                        >
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}
