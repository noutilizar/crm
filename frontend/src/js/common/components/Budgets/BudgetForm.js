import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { validate, validators } from "validate-redux-form";
import {
    renderField,
    AsyncSelectField,
    SelectField,
    renderCurrency,
} from "../Utils/renderField/renderField";
import "../../../../style/fuentes.css";
import "./budgetstyle.css";
import { api } from "../../../utility/api";
import { conformsTo } from "lodash";
const typeClient = [
    { value: 1, label: "Local" },
    { value: 2, label: "Internacional" },
];


const getVendedores = (search) => {
    let vendedores = [];
    return api
        .get("vendedor", { search })
        .then((response) => {
            vendedores = response.results.map((vendedor) => ({
                value: vendedor.id,
                label: `${vendedor.nombre} ${vendedor.apellido} ${vendedor.id}`,
                vendedor_percentage: vendedor.vendedor_percentage,
            }));
            return vendedores;
        })
        .catch((err) => {
            return vendedores;
        });
};

const getTipoClientes = (search) => {
    let tipoClientes = [];
    return api
        .get("type_client", { search })
        .then((response) => {
            tipoClientes = response.results.map((tipo) => ({
                value: tipo.id,
                label: tipo.name_type,
            }));
            return tipoClientes;
        })
        .catch((err) => {
            return tipoClientes;
        });
};

const getTipoProyecto = (search) => {
    let tipoProyecto = [];
    return api
        .get("type_project", { search })
        .then((response) => {
            tipoProyecto = response.results.map((proyecto) => ({
                value: proyecto.id,
                label: proyecto.name_project,
                average_sprint: proyecto.average_sprint,
            }));
            return tipoProyecto;
        })
        .catch((err) => {
            return tipoProyecto;
        });
};

const getLeads = (search) => {
    let leads = [];
    return api
        .get("leads", { search })
        .then((response) => {
            leads = response.results.map((lead) => ({
                value: lead.id,
                label: lead.name,
            }));
            return leads;
        })
        .catch((err) => {
            return leads;
        });
};
const getTipoCambio = (search) => {
    let tipoCambio = [];
    return api
        .get("config/change-coin", { search })
        .then((response) => {
            tipoCambio = response.results.map((cambio) => ({
                value: cambio.id,
                label: cambio.coin_type,
                type: cambio.change_type,
            }));
            return tipoCambio;
        })
        .catch((err) => {
            return tipoCambio;
        });
};

const getCanalVentas = (search) => {
    let canalVentas = [];
    return api
        .get("sales_channel", { search })
        .then((response) => {
            canalVentas = response.results.map((canal) => ({
                value: canal.id,
                label: canal.name,
                commission_percentage: canal.commission_percentage,
            }));
            return canalVentas;
        })
        .catch((err) => {
            return canalVentas;
        });
};

export default class BudgetForm extends Component {
    state = { valor_quetzales: 0.0, change_type: 0, precio_nominal: 0 };
    render() {
        const {
            ver,
            SetCommissionPercentage,
            SetVendedorPercentage,
            SetSprint,
            SetDesigner,
            SetProgrammer,
            programmer,
            designer,
            setTypeProject,
        } = this.props;
        const { change_type, precio_nominal } = this.state;
        console.log("Valor en quetzal: ", change_type * precio_nominal);

        const datos = getTipoProyecto('')
            console.log('datos', datos)
        return (
            <div>
       
                    <div className="uk-flex div-cards">
                        <div className="budget-cards uk-width-1-3">
                            <div className="budget-form-input-divs">
                                <label className="presupuesto_subtitle">
                                    Lead
                                </label>

                                <Field
                                    name="lead"
                                    component={AsyncSelectField}
                                    className="budget-form-select"
                                    loadOptions={getLeads}
                                    disabled={ver}
                                />
                            </div>
                            <div className="budget-form-input-divs">
                                <label className="presupuesto_subtitle">
                                    Programadores
                                </label>
                                <Field
                                    name="cantidad_programadores"
                                    className="uk-input uk-border-rounded uk-form-width-medium budget-form-input"
                                    component={renderField}
                                    placeholder="0.00"
                                    type="text"
                                    onChange={(e, value) => {
                                        SetProgrammer(value);
                                    }}
                                    disabled={ver}
                                />
                            </div>
                            <div className="budget-form-input-divs">
                                <label className="presupuesto_subtitle">
                                    Tipo de cambio
                                </label>

                                <Field
                                    name="tipocambio"
                                    component={AsyncSelectField}
                                    className="budget-form-select"
                                    loadOptions={getTipoCambio}
                                    onChange={(value) =>
                                        this.setState({
                                            change_type: value.type,
                                        })
                                    }
                                    disabled={ver}
                                />
                            </div>

                            <div className="budget-form-input-divs">
                                <label className="presupuesto_subtitle">
                                    Precio nominal
                                </label>

                                <Field
                                    name="precio_nominal"
                                    component={renderCurrency}
                                    className="budget-form-select"
                                    type="text"
                                    onChange={(e, value) =>
                                        this.setState({
                                            precio_nominal: value
                                                ? parseFloat(value)
                                                : 0,
                                        })
                                    }
                                    disabled={ver}
                                />
                                <small>{precio_nominal * change_type}</small>
                            </div>
                        </div>

                        <div className="budget-cards uk-width-1-3">
                            <div className="budget-form-input-divs">
                                <label className="presupuesto_subtitle">
                                    Tipo de cliente
                                </label>

                                <Field
                                    name="tipo_cliente"
                                    component={AsyncSelectField}
                                    disabled={ver}
                                    className="budget-form-select"
                                    loadOptions={getTipoClientes}
                                    type="text"
                                    disabled={ver}
                                />
                            </div>
                            <div className="budget-form-input-divs">
                                <label className="presupuesto_subtitle">
                                    Diseñadores
                                </label>
                                <Field
                                    name="cantidad_diseñadores"
                                    className="uk-input uk-border-rounded uk-form-width-medium budget-form-input"
                                    component={renderField}
                                    placeholder="0.00"
                                    onChange={(e, value) => {
                                        SetDesigner(value);
                                    }}
                                    disabled={ver}
                                />
                            </div>
                            <div className="budget-form-input-divs">
                                <label className="presupuesto_subtitle">
                                    Canal de ventas
                                </label>

                                <Field
                                    name="canal_venta"
                                    component={AsyncSelectField}
                                    disabled={ver}
                                    className="budget-form-select"
                                    type="text"
                                    loadOptions={getCanalVentas}
                                    onChange={(value) => {
                                        console.log(value);
                                        SetCommissionPercentage(
                                            value.commission_percentage
                                        );
                                    }}
                                    disabled={ver}
                                />
                            </div>
                        </div>

                        <div className="budget-cards uk-width-1-3">
                            <div className="budget-form-input-divs">
                                <label className="presupuesto_subtitle">
                                    Tipo de proyecto
                                </label>

                                <Field
                                    name="tipo_proyecto"
                                    component={AsyncSelectField}
                                    disabled={ver}
                                    className="budget-form-select"
                                    type="text"
                                    loadOptions={getTipoProyecto}
                                    onChange={(value) => {
                                        setTypeProject(value.value);
                                        // SetSprint(value.average_sprint);
                                    }}
                                    disabled={ver}
                                />
                            </div>
                            <div className="budget-form-input-divs">
                                <label className="presupuesto_subtitle">
                                    Margen de error
                                </label>
                                <Field
                                    name="porcentaje_error"
                                    className="uk-input uk-border-rounded uk-form-width-medium budget-form-input"
                                    component={renderField}
                                    type="text"
                                    placeholder="0.00"
                                    disabled={ver}
                                />
                            </div>
                            <div className="budget-form-input-divs">
                                <label className="presupuesto_subtitle">
                                    Vendedor
                                </label>

                                <Field
                                    name="vendedor"
                                    component={AsyncSelectField}
                                    disabled={ver}
                                    type="text"
                                    className="budget-form-select"
                                    loadOptions={getVendedores}
                                    onChange={(value) => {
                                        console.log(value);
                                        SetVendedorPercentage(
                                            value.vendedor_percentage
                                        );
                                    }}
                                    disabled={ver}
                                />
                            </div>
                        </div>
                    </div>
            
            </div>
        );
    }
}

// export default reduxForm({
//     form: "createBudget", // a unique identifier for this form
// })(BudgetForm);
