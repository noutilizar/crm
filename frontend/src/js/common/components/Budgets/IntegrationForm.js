import React, { Component } from "react";
import { FormSection, reduxForm, FieldArray } from "redux-form";
import FormTabs from "./FormTabs";
import BudgetForm from "./BudgetForm";

class IntegrationForm extends Component {
    state = { tipo_proyecto:0  }
  

    setTypeProject=(id) =>{
        this.setState({tipo_proyecto:id})
    }

    render(){    
        const {
            setAceptado,
            handleSubmit,
            SetCommissionPercentage,
            SetVendedorPercentage,
            SetSprint,
            SetDesigner,
            SetProgrammer,
            SetTotal_HistoryPoints,
            SetTotal_HistoryPoints_Error,
            ver,
            actualizar,
        } = this.props;
        return (
            <form onSubmit={handleSubmit}>
                <FormSection name="budget">
                    <BudgetForm
                        SetCommissionPercentage={SetCommissionPercentage}
                        SetVendedorPercentage={SetVendedorPercentage}
                        SetSprint={SetSprint}
                        SetDesigner={SetDesigner}
                        SetProgrammer={SetProgrammer}
                        setTypeProject={this.setTypeProject}
                        // ver={location.pathname.includes("ver") && true}
                    />
                </FormSection>
                <FormSection name="modules">
                    <FormTabs
                        setAceptado={setAceptado}
                        tipo_proyecto={this.state.tipo_proyecto}
                        // ver = {ver}
                    />
                </FormSection>
            </form>
        );
    }
}

export default reduxForm({
    form: "budgetForm2", // a unique identifier for this form
})(IntegrationForm);
