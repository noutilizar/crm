
import React, { Component } from 'react'
import Tabs, { TabPane } from 'rc-tabs';
import {FieldArray, Field, change} from "redux-form"
import {api} from "../../../utility/api";
import Grid from "../Utils/Grid";
import ScrollableInkTabBar from 'rc-tabs/lib/ScrollableInkTabBar';
import { standardActions } from "../Utils/Grid/StandardActions";

import TabContent from 'rc-tabs/lib/TabContent';
import "../../../../style/fuentes.css";
import "./budgetstyle.css";
import { AsyncSelectField, renderField } from "../Utils/renderField/renderField";
import { render } from 'react-dom';
import { search } from 'superagent';
import svgDelete from '../../../../assets/img/delete.svg';

const getRelationModules = (search)  =>{
    const modules=[];
    return api.get("budget_modules", {search}).catch((error) => {})
        .then((data) => {
            if (data){
                data.results.forEach(module=>{
                    modules.push({value: module.id, label: module.name_modules, 
                        name_modules: module.name_modules, 
                        history_points: module.history_points,
                        history_points_error: module.history_points_error,
                        total_history_points: module.total_history_points,
                        total_start_hours: module.total_start_hours,
                        total_start_hours_error: module.total_start_hours_error,
                        total_hours: module.total_hours,
                    })
                })
                console.log(modules)
                return modules;
            }
            else{
                return []
            }
        }).catch(e => { return []; })
}

const getAdminModules = (search, type_project_id)  =>{
   const formData = {search, type_project_id}

    const modules=[];
    return api.get("administrative_expenses_header/get_gastos", formData).catch((error) => {})
        .then((data) => {
            if (data){
                data.results.forEach(module=>{
                    modules.push({value: module.id, label: module.name_expense, 
                        name_expense: module.name_expense, 
                            number_hours: module.number_hours,
                            total_quantity: module.total_quantity,
                            rate_hour: module.rate_hour,
                            total_hours: module.total_hours,
                    })
                })
                console.log(modules)
                return modules;
            }
            else{
                return []
            }
        }).catch(e => { return []; })
}

const AddRow = () => {
    fields.push({})
}



const renderModules = ({ fields,  meta: { error, submitFailed } }) => (
    <tbody>
      <div>
        {submitFailed && error && <span>{error}</span>}
      </div>
      {fields.map((member, index) => (
        <tr key={index}>
            <td>
                <a className="px-2" style={{cursor: "pointer"}}>
                    <img className="icon_svg" src={svgDelete} style={{cursor: "pointer"}} onClick={() => fields.remove(index)}/>
                </a> 
            </td>
            <td style={{textAlign: "center"}}>
                <Field
                    name={`${member}.name_modules`}
                    type="text"
                    component={renderField}
                    label="First Name"
                />
            </td>
            <td>
              <Field
                  name={`${member}.history_points`}
                  type="text"
                  component={renderField}
                  label="Last Name"
               />
            </td>
            <td>
              <Field
                  name={`${member}.history_points_error`}
                  type="text"
                  component={renderField}
                  label="Last Name"
              />
            </td>
            <td>
              <Field
                 name={`${member}.total_history_points`}
                 type="text"
                 component={renderField}
                 label="Last Name"
              />
            </td>
            <td>
               <Field
                    name={`${member}.total_start_hours`}
                    type="text"
                    component={renderField}
                    label="Last Name"
               />
            </td>
            <td>
                <Field
                    name={`${member}.total_start_hours_error`}
                    type="text"
                    component={renderField}
                    label="Last Name"
                />
            </td>
            <td>
                <Field
                name={`${member}.total_hours`}
                type="text"
                component={renderField}
                label="Last Name"
                />
            </td>
            
        </tr>
      ))}
        <tr>
            <td colSpan={3}> 
                <Field
                    name={`budget_modules`}
                    type="text"
                    component={AsyncSelectField}
                    label="Last Name"
                    loadOptions={getRelationModules}
                    onChange={(module)=>{
                        fields.push({
                            name_modules: module.name_modules, 
                            history_points: module.history_points,
                            history_points_error: module.history_points_error,
                            total_history_points: module.total_history_points,
                            total_start_hours: module.total_start_hours,
                            total_start_hours_error: module.total_start_hours_error,
                            total_hours: module.total_hours,
                        })
                    }}
                />
                                                
            </td>
            <td colSpan={7}></td>
            <td colSpan={2}>
                <button className="btn-warn-dt" type="button" onClick={() => fields.push({  })}>
                Agregar módulo
                </button> 
            </td>
        </tr>
    </tbody>
)

const renderAdminModules = ({ fields, tipo_proyecto, meta: { error, submitFailed } }) => (
    <tbody>
      <div>
        {submitFailed && error && <span>{error}</span>}
      </div>
      {fields.map((member, index) => (
        <tr key={index}>
            <td>
                <a className="px-2" style={{cursor: "pointer"}}>
                    <img className="icon_svg" src={svgDelete} style={{cursor: "pointer"}} onClick={() => fields.remove(index)}/>
                </a> 
            </td>
            <td style={{textAlign: "center"}}>
                <Field
                    name={`${member}.name_expense`}
                    type="text"
                    component={renderField}
                    label="First Name"
                />
            </td>
            <td>
              <Field
                  name={`${member}.number_hours`}
                  type="text"
                  component={renderField}
                  label="Last Name"
               />
            </td>
            <td>
              <Field
                  name={`${member}.total_quantity`}
                  type="text"
                  component={renderField}
                  label="Last Name"
              />
            </td>
            <td>
              <Field
                 name={`${member}.rate_hour`}
                 type="text"
                 component={renderField}
                 label="Last Name"
              />
            </td>
            <td>
               <Field
                    name={`${member}.total_hours`}
                    type="text"
                    component={renderField}
                    label="Last Name"
               />
            </td>            
        </tr>
      ))}
  
        <tr>
            <td colSpan={3}>
                <Field
                    key={tipo_proyecto}
                    name={`budget_admin_modules`}
                    type="text"
                    component={AsyncSelectField}
                    label="Last Name"
                    loadOptions={(search)=> getAdminModules (search, tipo_proyecto)}
                    onChange={(module)=>{
                        fields.push({
                            name_expense: module.name_expense, 
                            number_hours: module.number_hours,
                            total_quantity: module.total_quantity,
                            rate_hour: module.rate_hour,
                            total_hours: module.total_hours,
                        })
                    }}
                />
            </td>
            <td colSpan={8}></td>
            <td colSpan={1}>
                <button className="btn-warn-dt" type="button" onClick={() => fields.push({  })}>
                    Agregar gasto administrativo
                </button>                                
            </td>
        </tr>
    </tbody>
)

export default class FormTabs extends Component{
    
    render(){
        const { setAceptado, tipo_proyecto, fields } = this.props;
        console.log('fields', fields)
        return (
            <div className="row">
                <div className="mb-4 col-lg-12">
                    <div className="mb-4   py-1">
                        
                        <div className="p-0 px-3 pt-3">
                            <Tabs
                                defaultActiveKey="BUDGET"
                                tabBarPosition="top"
                                onChange={this.callback}
                                renderTabBar={() => <ScrollableInkTabBar />}
                                renderTabContent={() => <TabContent />}
                            >
                                <TabPane tab="Modulos" key="Modulos">
                                    <div className="react-bs-container-header table-header-wrapper">
                                    <table className="table table-striped table-bordered grid-table-head">
                                        <thead>
                                            <tr>            
                                                <th>    
                                                                                           
                                                {/* <Field
                                                    name={`${member}.budget_modules`}
                                                    type="text"
                                                    component={AsyncSelectField}
                                                    label="Last Name"
                                                    loadOptions={getRelationModules}
                                                    onChange={changeArrayField}
                                                /> */}
                                                </th>
                                                <th className="text-primary">NOMBRE</th>
                                                <th className="text-primary">PUNTOS</th>
                                                <th className="text-primary">P.M. ERROR</th>
                                                <th className="text-primary">P. DE HISTORIA</th>
                                                <th className="text-primary">HORAS INICIALES</th>
                                                <th className="text-primary">H.M. ERROR</th>
                                                <th className="text-primary">H. Totales</th>
                                            </tr>
                                        </thead>
                                        <FieldArray name="modules" component={renderModules} setAceptado={setAceptado} />
                                    </table>
                                    </div>
                                </TabPane>
                                <TabPane tab="Gastos Admin" key="Gastos_Admin">
                                <div className="react-bs-container-header table-header-wrapper">
                                    <table className="table table-striped table-bordered grid-table-head">
                                        <thead>
                                            <tr>
                                                <th>                
                                                {/* <Field
                                                    name={`${member}.budget_modules`}
                                                    type="text"
                                                    component={AsyncSelectField}
                                                    label="Last Name"
                                                    loadOptions={getRelationModules}
                                                    onChange={changeArrayField}
                                                /> */}
                                                </th>
                                                <th className="text-primary">NOMBRE</th>
                                                <th className="text-primary">CANTIDAD DE HORAS</th>
                                                <th className="text-primary">CANTIDAD TOTAL</th>
                                                <th className="text-primary">TARIFA POR HORA</th>
                                                <th className="text-primary">TOTAL</th>
                                            </tr>
                                        </thead>
                                        <FieldArray name="adminmodules" component={renderAdminModules} tipo_proyecto={tipo_proyecto}/>
                                    </table>
                                    </div>
                                </TabPane>
                            </Tabs>
                        </div>
                    </div>
                </div>
            </div>                             
        )
    }
}


 