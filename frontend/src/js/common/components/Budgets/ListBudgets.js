import React, { Component } from "react";
import "../../../../style/fuentes.css";
import Grid from "../Utils/Grid";
import { standardActions } from "../Utils/Grid/StandardActions";
export default class ListBudgets extends Component {
    componentWillMount = () => {
        const { listar } = this.props;
        console.log(this.props.data);
        listar();
    };

    render() {
        const {
            data,
            loader,
            onSortChange,
            eliminar,
            listar,
            page,
        } = this.props;
        return (
            <React.Fragment>
                <br />
                <h4 className="contacts_title ">PRESUPUESTOS </h4>

                <div className="uk-card  uk-margin-auto contact-divprincipal ">
                    <div className="list-button-budget uk-flex uk-flex-between uk-padding uk-padding-remove-bottom uk-margin-auto-top@s ">
                        <button className="uk-button uk-button-primary uk-border-rounded uk-margin-small-bottom uk-button-small uk-flex link-contact">
                            <a
                                className="uk-align-center"
                                href="/#/presupuestos/create"
                            >
                                {/* <i style={{marginRight: "4px"}} className="material-icons">add_circle_outline</i> */}
                                Crear Presupuesto
                            </a>
                        </button>
                        <input
                            className="uk-input uk-border-rounded uk-width-1-5"
                            placeholder="Buscar"
                            onChange={this.handleSearch}
                        />
                    </div>
                </div>

                <Grid
                    className="uk-padding "
                    data={data}
                    loading={loader}
                    onPageChange={listar}
                    page={page}
                    striped
                    hover
                >
                    <TableHeaderColumn
                        dataField="id"
                        dataAlign="center"
                        dataSort
                        dataFormat={standardActions({
                            editar: "presupuestos",
                            ver: "presupuestos",
                            eliminar,
                        })}
                    ></TableHeaderColumn>
                    <TableHeaderColumn
                        isKey
                        dataField="lead"
                        dataSort
                        className="fuente_header_table"
                    >
                       Lead
                    </TableHeaderColumn>
                    <TableHeaderColumn
                                                dataFormat={(cell) =>{
                                                    if(cell){
                                                    return cell.name_type
                                                    }
                                                    
                                                }}
                        dataField="tipo_cliente"
                        dataSort
                        className="fuente_header_table"
                    >
                       Cliente
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        
                        dataFormat={(cell) =>{
                            if(cell){
                            return cell.name_project
                            }
                            
                        }}
                        dataField="tipo_proyecto"
                        dataSort
                        className="fuente_header_table"
                    >
                      Proyecto
                    </TableHeaderColumn>
                    <TableHeaderColumn
                    
                        dataField="duracion_totalProyecto"
                        dataSort
                        className="fuente_header_table"
                    >
                      Duracion del proyeto
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      
                        dataField="precio_total"
                        dataSort
                        className="fuente_header_table"
                    >
                     Costo
                    </TableHeaderColumn>
                </Grid>
            </React.Fragment>
        );
    }
}
