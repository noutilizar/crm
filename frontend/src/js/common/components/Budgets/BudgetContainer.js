import { connect } from "react-redux";
import { actions } from "../../../redux/modules/budget/budget";
// import newBudget from './AnnualGoals';
import ListBudget from "./ListBudgets";
import BudgetNew from "./BudgetNew";
import FormTabs from "./FormTabs";

const ms2p = (state) => {
    return {
        ...state.budget,
    };
};
const md2p = { ...actions };

//= =================
// Conection List Budget
//= =================
const listBudget = connect(ms2p, md2p)(ListBudget);

//= =========================
// Conection Create Budget
//= ========================?
const newBudget = connect(ms2p, md2p)(BudgetNew);

//= =========================
// Conection Create Budget
//= ========================?
const formTabs = connect(ms2p, md2p)(FormTabs);

export const ConnectionBudget = {
    newBudget,
    listBudget,
    formTabs,
};
