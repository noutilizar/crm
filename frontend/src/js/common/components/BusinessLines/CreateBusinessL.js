
import React, {Component} from 'react';
import BusinessLinesForm from "./BusinessLForm";
import '../../../../style/fuentes.css';

class CreateBusinessLines extends Component{
    componentWillMount = () => {
        const {match, editarLinea} = this.props;
        if(match.params.id){
            const id = match.params.id;
            editarLinea(id);
        }
    }

    render(){
        const {match, registrarLinea, actualizarLinea, location} = this.props;
        const funcionEnvio = match.params.id ? actualizarLinea : registrarLinea;

        return(
            <div>
               <br />
               <br />
                <h4 className="lnegocios_nueva_title">
                    NUEVA LINEA DE NEGOCIOS
                </h4>
                <BusinessLinesForm
                    onSubmit={funcionEnvio}
                    actualizarLinea={match.params.id ? true : false}
                    ver={location.pathname.includes('ver')}
                />
            </div>
        )
    }
}

export default CreateBusinessLines;
