import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Grid from "../Utils/Grid";
import { standardActions } from "../Utils/Grid/StandardActions";
import '../../../../style/fuentes.css';

class BusinessLines extends Component {
    componentWillMount = () => {
        const { listarLinea } = this.props;

        listarLinea();
    }

    handleSearch = (e) => {
        const { listarLinea, filterBusiness } = this.props;
        if (e.target.value != '') {
            filterBusiness(e.target.value);
        } else {
            listarLinea();
        }
    }

    render() {
        const { data, loader, onSortChange, eliminarLinea, listarLinea, page } = this.props;
        return (
            <React.Fragment>
                <br />
                <h4 className="lnegocios_title">LINEA DE NEGOCIOS</h4>
                <div className="uk-card uk-margin-auto">
                    <div className="uk-flex uk-flex-between uk-padding-remove-bottom uk-margin-auto-top@s">

                        <Link
                            className="uk-button uk-button-primary uk-border-rounded uk-margin-small-bottom  uk-button-small uk-flex"
                            to="/business-lines/create"
                        >
                            Crear Linea
                            {/* <i style={{marginLeft: "2px"}} className="material-icons">add_circle_outline</i> */}
                        </Link>
                        <div className="uk-inline">
                            <span className="uk-form-icon uk-form-icon-flip"><i className="material-icons text-primary">search</i></span>
                                <input
                                    type="text"
                                    className="uk-input uk-border-rounded uk-width-1"
                                    onChange={this.handleSearch}
                                    
                                />
                        </div>
                    </div>
                    <div className="job-padding">
                        <Grid
                            hover
                            striped
                            data={data}
                            loading={loader}
                            onPageChange={listarLinea}
                            onSortChange={onSortChange}
                            page={page}

                        >
                            <TableHeaderColumn
                                dataField="id"
                                dataAlign="center"
                                dataSort
                                dataFormat={standardActions({ editar: "business-lines", ver: "business-lines", eliminar: eliminarLinea })}
                            >
                                
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                isKey
                                dataField="name"
                                dataSort
                                className="text-primary"
                            >
                                NOMBRE LINEA
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="description"
                                dataSort
                                className="text-primary"
                            >
                                DESCRIPCION
                            </TableHeaderColumn>

                        </Grid>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default BusinessLines;
