import React, { Component } from "react";
import ContactForm from "./ContactForm";
import "../../../../style/fuentes.css";
import "./contactstyle.css";
export default class Contacts extends Component {
    componentWillMount = () => {
        const { match, detalle } = this.props;
        if (match.params.id) {
            const { id } = match.params;
            detalle(id);
        }
    };

    componentWillUnmount = () => {
        const { closeModal } = this.props;
        closeModal();
    };

    render() {
        const {
            onSubmit,
            getUsuarios,
            getLifeCycles,
            getIndustries,
            registarEmpresa,
            openModal,
            closeModal,
            getCompanies,
            actualizar,
            location,
            match,
            stateModal,
        } = this.props;

        const fn = match.params.id ? actualizar : onSubmit;

        return (
            <div className="contact-divprincipal">
                <br />
                <h4 className="contacts_nuevo_title titulo-contact">NUEVO CONTACTO </h4>
                <div className="contact-div">
                    <ContactForm
                        onSubmit={fn}
                        registarEmpresa={registarEmpresa}
                        getUsuarios={getUsuarios}
                        getLifeCycles={getLifeCycles}
                        getCompanies={getCompanies}
                        getIndustries={getIndustries}
                        openModal={openModal}
                        closeModal={closeModal}
                        stateModal={stateModal}
                        actualizar={!!match.params.id}
                        ver={location.pathname.includes("ver") && true}
                    />
                </div>
            </div>
        );
    }
}
