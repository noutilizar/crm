import React, { Component } from "react";
import LeadModal from "../leads/LeadModal";
import Grid from "../Utils/Grid";
import { standardActions } from "../Utils/Grid/StandardActions";
import "../../../../style/fuentes.css";

export default class ListContacts extends Component {
    componentWillMount = () => {
        const { listar } = this.props;

        listar();
    };

    // Menja el input de busqueda
    handleSearch = (e) => {
        const { listar, filterContact } = this.props;

        if (e.target.value != "") {
            filterContact(e.target.value);
        } else {
            listar();
        }
    };

    render() {
        const {
            data,
            loader,
            onSortChange,
            eliminar,
            listar,
            page,
        } = this.props;

        return (
            <React.Fragment>
                <br />
                <h4 className="contacts_title ">CONTACTOS </h4>

                <div className="uk-card  uk-margin-auto contact-divprincipal ">
                    <div className="list-button-contact  uk-flex uk-flex-between uk-padding uk-padding-remove-bottom uk-margin-auto-top@s ">
                        <button className="uk-button uk-button-primary uk-border-rounded uk-margin-small-bottom uk-button-small uk-flex link-contact"> 
                        <a
                            className="uk-align-center"
                            href="/#/contacts/create"
                        >
                            {/* <i style={{marginRight: "4px"}} className="material-icons">add_circle_outline</i> */}
                            Crear Contacto
                        </a></button>
                        <input
                            className="uk-input uk-border-rounded uk-width-1-5"
                            placeholder="Buscar"
                            onChange={this.handleSearch}
                        />
                    </div>

                    <Grid
                        className="uk-padding "
                        data={data}
                        loading={loader}
                        onPageChange={listar}
                        onSortChange={onSortChange}
                        page={page}
                        striped
                        hover
                    >
                        <TableHeaderColumn
                            dataField="id"
                            dataAlign="center"
                            dataSort
                            dataFormat={standardActions({
                                editar: "contacts",
                                ver: "contacts",
                                eliminar,
                            })}
                        ></TableHeaderColumn>
                        <TableHeaderColumn isKey dataField="name" dataSort className="fuente_header_table">
                            Nombre
                        </TableHeaderColumn>
                        <TableHeaderColumn dataField="position" dataSort className="fuente_header_table">
                            Puesto
                        </TableHeaderColumn>

                        <TableHeaderColumn dataField="company" dataSort className="fuente_header_table">
                            Empresa
                        </TableHeaderColumn>

                        <TableHeaderColumn dataField="phone_staff" dataSort className="fuente_header_table">
                            Teléfono
                        </TableHeaderColumn>
                        <TableHeaderColumn dataField="email" dataSort className="fuente_header_table">
                            Email
                        </TableHeaderColumn>

                        {/* 


                        <TableHeaderColumn
                            dataField="life_cycles"
                            dataSort
                        >
                            Ciclo de vida
                        </TableHeaderColumn> */}
                    </Grid>

                    <LeadModal />
                </div>
            </React.Fragment>
        );
    }
}
