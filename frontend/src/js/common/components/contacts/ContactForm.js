import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { validate, validators } from 'validate-redux-form';

import { renderField, AsyncSelectField, SelectField } from '../Utils/renderField/renderField';
import '../../../../style/fuentes.css';

import { email, phone } from '../../../utility/validation';
import EmpresaModal from './EmpresaModal';
import './contactstyle.css'

const typeClient = [
    { value: 1, label: 'Local'},
    { value: 2, label: 'Internacional'},
];


const ContactForm = (props) => {
    const { handleSubmit, registarEmpresa, getUsuarios, getLifeCycles, getCompanies,
            getIndustries, openModal, closeModal, ver, actualizar, stateModal } = props;


    return (
        <form onSubmit={handleSubmit} className="contact-form">
            <div className="uk-margin-bottom">

                <div className="uk-child-width-1-2@s uk-grid">

                    <div>
                        <label className="contacts_nombre">Nombre contacto</label>
                        <Field
                            name="name"
                            className="uk-input uk-border-rounded uk-form-width-medium contact-form-input"
                            type="text"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>

                    <div>
                        <label className="contacts_mail">E-mail</label>
                        <Field
                            name="email"
                            className="uk-input uk-border-rounded uk-form-width-medium contact-form-input"
                            type="email"
                            validate={email}
                            component={renderField}
                            disabled={ver}
                        />
                    </div>
                </div>

                <div className="uk-child-width-1-2@s uk-grid">
                    <div>
                        <label className="contacts_telefono">Teléfono personal</label>
                        <Field
                            name="phone_staff"
                            className="uk-input uk-border-rounded uk-form-width-medium contact-form-input"
                            type="tel"
                            validate={phone}
                            component={renderField}
                            disabled={ver}
                        />
                    </div>

                    <div>
                        <label className="contacts_tel_empresa">Teléfono empresa</label>
                        <Field
                            name="phone_business"
                            className="uk-input uk-border-rounded uk-form-width-medium contact-form-input"
                            type="tel"
                            validate={phone}
                            component={renderField}
                            disabled={ver}
                        />
                    </div>
                </div>


                <div className="uk-child-width-1-2@s uk-grid">
                    <div>
                        <label className="contacts_puesto">Puesto</label>
                        <Field
                            name="position"
                            className="uk-input uk-border-rounded uk-form-width-medium contact-form-input"
                            type="text"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>
                </div>

                <div className="uk-child-width-1-2@s uk-grid ">
                    <div>
                    <div>
                        <label className="contacts_nombre_empresa">Nombre empresa</label>

                        
                            <Field
                                name="company"
                                type="text"
                                component={AsyncSelectField}
                                loadOptions={getCompanies}
                                disabled={ver}
                                className="contact-form-select "
                            />
                        </div>


                        <div>
                            <button
                                className="uk-button uk-button-link uk-align-right uk-margin-remove-bottom "
                                disabled={ver}
                                type="button"
                                onClick={() => {
                                    openModal();
                                }}
                            >
                                + Empresa
                            </button>
                        </div>

                    </div>

                    <div>
                        <label className="contacts_propietario">Contacto propietario</label>
                        <Field
                            name="userowner"
                            type="text"
                            component={AsyncSelectField}
                            loadOptions={getUsuarios}
                            disabled={ver}
                            className="contact-form-select "
                        />
                    </div>
                </div>

                <div className="uk-margin-bottom uk-child-width-1-2@s uk-grid">
                    <div className="contacts_form_input_tipo_cliente">
                        <label className="contacts_tipo_cliente">Tipo de cliente</label>
                        <Field
                            name="client_type"
                            type="text"
                            component={SelectField}
                            options={typeClient}
                            disabled={ver}
                            className="" 
                        />
                    </div>

                    <div>
                        <label className="contacts_estado_ciclovida">Estado del ciclo de vida</label>
                        <Field
                            name="life_cycles"
                            type="text"
                            component={AsyncSelectField}
                            loadOptions={getLifeCycles}
                            disabled={ver}
                            className="contact-form-select "
                        />
                    </div>
                </div>

                <div className="uk-child-width-1-2@s uk-grid contact-buttons">
                    <div className="contact-div-button-cancelar ">
                    <a
                        className="uk-button uk-button-secondary uk-button-small uk-border-rounded uk-flex contact-button-cancelar"
                        href="/#/contacts"
                    >

                        {/* <i style={{marginRight: "4px"}} className="material-icons">cancel</i> */}
                        { ver ? 'Regresar' : 'Cancelar' }
                    </a></div>
                    <div>
                    {

                        !ver
                        && (
                            <button
                                className="uk-button uk-button-primary uk-margin-left
                                        uk-button-small uk-border-rounded uk-flex contact-button-guardar" 
                                type="submit"
                            >
                                {/* <i style={{marginRight: "4px"}} className="material-icons">save</i> */}
                                { actualizar ? 'Actualizar' : 'Guardar' }

                            </button>
                        )
                    }</div>
                </div>


                <EmpresaModal
                    onSubmit={registarEmpresa}
                    stateModal={stateModal}
                    closeModal={closeModal}
                    getIndustries={getIndustries}
                />

            </div>
        </form>
    );
};

export default reduxForm({
    form: 'contactForm', // a unique identifier for this form
    validate: (data) => {
        return validate(data, {
            name: validators.exists()('Este campo es requerido'),
            email: validators.exists()('Este campo es requerido'),
            phone_staff: validators.exists()('Este campo es requerido'),
            position: validators.exists()('Este campo es requerido'),
            company: validators.exists()('Este campo es requerido'),
            user: validators.exists()('Este campo es requerido'),
            typeclient: validators.exists()('Este campo es requerido'),
            lifecycle: validators.exists()('Este campo es requerido'),
        });
    },
})(ContactForm);
