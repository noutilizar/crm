import React from "react";
import currency from "currency-formatter";
import "./reportesemanalstyle.css";
export const Cards = (props) => {
    const { meta_anual, meta_mensual, venta_semanal } = props;
    return (
        <div className="reporte-semanal-divprincipal">
            {/* <div className="uk-child-width-1-2@s uk-grid"> */}
            <div className="uk-child-width-1-2@s uk-grid">
            <div>
                <div className="reporte-semanal-cards ">
                    <div className=" uk-padding-small uk-child-width-1-2@s uk-grid ">
                        <label className="reportesem_meta_anual">
                            Meta Anual
                        </label>
                        <h5 className="uk-margin-remove reportesem_numeros">
                            <span>
                                {currency.format(meta_anual.meta_anual, {
                                    code: "GTQ",
                                })}
                            </span>
                        </h5>
                    </div>

                    <div className="uk-padding-small">
                        <div className=" uk-child-width-1-2@s uk-grid">
                            <div className="">
                                <h6 className="reportesem_sub-texto">
                                    Lead Recibidos
                                </h6>
                                <h5 className="reportesem_meta_anual">
                                    {meta_anual.total_lead || 0}
                                </h5>
                            </div>
                            <div className="">
                                <h6 className="reportesem_sub-texto">
                                    % Cierre
                                </h6>
                                <h5 className="reportesem_meta_anual">
                                    {meta_anual.porcentaje_alcanzado || 0} %
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="reporte-semanal-cards">
                    <div className="uk-child-width-1-2@s uk-grid  uk-padding-small">
                        <h6 className="reportesem_meta_mes ">
                            Ventas de la semana
                        </h6>
                        <label className="reportesem_numeros">
                            {currency.format(venta_semanal, { code: "GTQ" })}
                        </label>
                    </div>

                    <div className=" uk-padding-small uk-grid uk-child-width-1-2@s ">
                        <h5 className="reportesem_sub-texto">
                            Alcanzado del mes
                        </h5>
                    </div>

                    <div>
                        <h5 className="uk-text-center">
                            <progress
                                class="uk-progress progress-bar-alcanzadoanual"
                                value={meta_mensual.porcentaje || 50}
                                max="100"
                            ></progress>
                        </h5>
                    </div>
                </div>
                </div>
               
               
                <div>
                <div className="reporte-semanal-cards ">
                    <div className="uk-child-width-1-2@s uk-grid  uk-padding-small">
                        <h6 className="reportesem_meta_mes ">Meta del mes</h6>
                        <label className="reportesem_numeros">
                            <span>
                                {currency.format(meta_mensual.meta_mensual, {
                                    code: "GTQ",
                                })}
                            </span>
                        </label>
                    </div>

                    {/* REPORTE ANUAL */}
                    <div className=" uk-padding-small">
                        <div className="uk-child-width-1-2@s uk-grid ">
                            <h6 className=" reportesem_sub-texto">
                                Alcanzado Anual
                            </h6>
                            <h6 className="reportesem_meta_anual">
                                {currency.format(meta_anual.alcanzado_anual, {
                                    code: "GTQ",
                                })}
                            </h6>
                        </div>
                        <br></br>
                        <div>
                            <progress
                                class="uk-progress progress-bar-alcanzadoanual"
                                value={meta_anual.porcentaje_alcanzado || 50}
                                max="100"
                            ></progress>
                        </div>
                        {/* <div className="uk-width-expand">
                                <h5 className="uk-text-success">%</h5>
                                <h6 className="uk-text-success">{ meta_anual.porcentaje_alcanzado || 0 } %</h6>
                            </div> */}
                    </div>

                    <div className=" uk-padding-small">
                        <div className="uk-child-width-1-2@s uk-grid ">
                            <h6 className="uk-text-danger reportesem_sub-texto">
                                Restante Anual
                            </h6>
                            <h6 className="reportesem_meta_anual">
                                {currency.format(meta_anual.restante_anual, {
                                    code: "GTQ",
                                })}
                            </h6>
                        </div>
                        <br></br>
                        <div>
                            <progress
                                class="uk-progress progress-bar-alcanzadoanual"
                                value={meta_anual.porcentaje_alcanzado || 50}
                                max="100"
                            ></progress>
                        </div>
                        {/* <div className="">
                                <h5 className="uk-text-danger">%</h5>
                                <label className="reportesem_restante_anual ">{ meta_anual.restante_porcentaje || 0 } %</label>
                            </div> */}
                    </div>

                    {/* </div> */}
                </div>
                <div className="reporte-semanal-cards ">
                    <div className=" uk-padding-small uk-child-width-1-2@s uk-grid">
                        <h6 className="reportesem_meta_mes ">Ventas del mes</h6>
                        <label className="reportesem_numeros">
                            {currency.format(meta_mensual.total, {
                                code: "GTQ",
                            })}
                        </label>
                    </div>
                </div>
                </div>
            
            </div>
            {/* </div> */}
        </div>
    );
};
