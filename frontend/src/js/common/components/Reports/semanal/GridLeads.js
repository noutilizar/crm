import React from 'react'
import Grid from '../../Utils/Grid';
import currency from 'currency-formatter'
import '../../../../../style/fuentes.css'
import './reportesemanalstyle.css'
export const GridLeads = (props) => {

    const { data_lead, data_possibility,total_lead, listPageLeads, page_posibility,
        listPageLeadsPossibility, total_possibility, page, loader_lead} = props
    
    return (
        <React.Fragment>
            <div className="reportesem-div-gridlead">

            
                <div className="uk-child-width-1-2@s uk-grid">
                    
                    <div className="uk-margin-large-bottom reporte-semanal-grids">
                        <div className="uk-child-width-1-2@s uk-grid"> 
                        <label className="reportesem_leads_semanal">Leads de la semana</label>
                        <label className="reportesem_leads_posibilidad">Total &ensp; <span className="reportesem_numeros">{ currency.format( total_lead, { code: 'GTQ'} ) }</span></label>
                        </div>
                        <Grid
                        data={data_lead}
                        loading={ loader_lead }
                        onPageChange={listPageLeads}
                        page={page}
                        striped
                        hover
                        >

                            <TableHeaderColumn
                                isKey
                                dataField="name"
                                headerAlign='center'
                                dataSort
                                className="fuente_header_table"
                            >
                                Nombre 
                            </TableHeaderColumn>

                            <TableHeaderColumn
                                dataField="business_stage"
                                headerAlign='center'
                                dataFormat={ cell => {
                                    if( cell === 'Ganado' ){
                                        return <span style={{ color: '#06d6a0', fontWeight:'bold'}}>{ cell }</span>
                                    }else if( cell === 'Perdido' ){
                                        return <span style={{ color: '#FA7D69', fontWeight:'bold'}}>{ cell }</span>
                                    }else{
                                        return <span>{ cell }</span>
                                    }
                                }}

                                dataSort
                                className="fuente_header_table"
                            >
                                Estado
                            </TableHeaderColumn> 

                            <TableHeaderColumn
                                dataField="quetzal"
                                headerAlign='center'
                                dataAlign='right'
                                dataFormat = { (cell)=> currency.format( cell, { code: 'GTQ'} ) }
                                dataSort
                                className="fuente_header_table"
                            >
                                Monto
                            </TableHeaderColumn>        

                        </Grid>
                    
                       
                        
                    </div>






                    <div className="uk-margin-large-bottom reporte-semanal-grids" >
                    <div className="uk-child-width-1-2@s uk-grid">
                        <label className="reportesem_leads_semanal">Leads con mayor posibilidad</label>
                        <label className="reportesem_leads_posibilidad">Total &ensp; <span className="reportesem_numeros">{ currency.format ( total_possibility, { code: 'GTQ'} ) }</span></label>
                        </div>
                        {/* <div className="uk-margin-medium-bottom"> */}

                        
                            <Grid
                            data={data_possibility}
                            // loading={loader}
                            onPageChange={listPageLeadsPossibility}
                            // onSortChange={onSortChange}
                            page={ page_posibility }
                            striped
                            hover
                            >

                                <TableHeaderColumn
                                    isKey
                                    dataField="name"
                                    headerAlign='center'
                                    dataSort
                                    className="fuente_header_table"
                                >
                                    Nombre
                                </TableHeaderColumn>

                                <TableHeaderColumn
                                    dataField="business_stage"
                                    headerAlign='center'
                                    dataSort
                                    className="fuente_header_table"
                                >
                                    Estado
                                </TableHeaderColumn> 

                                <TableHeaderColumn
                                    dataField="quetzal"
                                    headerAlign='center'
                                    dataAlign='right'
                                    dataFormat = { (cell)=> currency.format( cell, { code: 'GTQ'} ) }
                                    dataSort
                                    className="fuente_header_table"
                                >
                                    Monto
                                </TableHeaderColumn>        

                            </Grid>
                        
                        {/* </div> */}

                        
                    </div>

                </div>
            </div>
            
        </React.Fragment>
    )
}
