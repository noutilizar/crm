import React, { Component } from 'react'
import ReactQuill from 'react-quill'; 
import 'react-quill/dist/quill.snow.css'
import './reportesemanalstyle.css'
export default class Editor extends Component {

  // constructor(props) {
  //   super(props)
    
  //   // this.state = { text: '' } // You can also pass a Quill Delta here
  //   // this.handleChange = this.handleChange.bind(this)
  // }


  // handleChange(value) {
  //   this.setState({ text: value })
  // }
 
  render() {
    // const { text } = this.state;
    const { registrarNoticia, handleChange, textWeek } = this.props;

    return (
      <div className="uk-child-width-1-2@s uk-grid">
        <div className="report-quill-div">
        <ReactQuill 
            value={ textWeek || '' }
            onChange={handleChange}
            className="reporte-semanal-quill"
        /></div>
          
        <button
          className="uk-button uk-button-primary 
                      uk-border-rounded uk-flex uk-margin-small-top uk-margin-left"
          onClick={ ()=>{
            
            registrarNoticia( textWeek ) 
            // this.handleChange('')

          }}
        >
          Guardar
        </button>
      </div>
    )
  }

}
