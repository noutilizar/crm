import React from "react";
import { Field, reduxForm } from "redux-form";
import { validate, validators } from "validate-redux-form";
import {
    renderField,
    AsyncSelectField,
    SelectField,
} from "../Utils/renderField/renderField";
import "../../../../style/fuentes.css";
import "./budgetmodulesstyle.css";

const BudgetModulesForm = (props) => {
    const {
        handleSubmit,
        ver,
        actualizar,
    } = props;


    return (
        <div>
        <form onSubmit={handleSubmit} className="contact-form">
            <div className="uk-margin-bottom">

                <div className="uk-child-width-1-2@s uk-grid">

                    <div>
                        <label className="modules_nombre">Nombre modulo</label>
                        <Field
                            name="name_modules"
                            className="uk-input uk-border-rounded uk-form-width-medium contact-form-input"
                            type="text"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>

                    <div>
                        <label className="modules_history_points">Puntos de historia</label>
                        <Field
                            name="history_points"
                            className="uk-input uk-border-rounded uk-form-width-medium contact-form-input"
                            type="number"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>
                </div>



         

                <div className="uk-child-width-1-2@s uk-grid contact-buttons">
                    <div className="contact-div-button-cancelar ">
                        <a
                            className="uk-button uk-button-secondary uk-button-small uk-border-rounded uk-flex contact-button-cancelar"
                            href="/#/budget-modules"
                        >
                            { ver ? 'Regresar' : 'Cancelar' } 
                        </a>
                    </div>
                    <div>
                    {
                     !ver
                        && ( 
                            <button
                                className="uk-button uk-button-primary uk-margin-left
                                        uk-button-small uk-border-rounded uk-flex contact-button-guardar" 
                                type="submit"
                            >
                                { actualizar ? 'Actualizar' : 'Guardar' }
                            </button>
                         )
                    } 
                    </div>
                </div>

            </div>
        </form>
      </div>
    );
};

export default reduxForm({
    form: "budgetmodulesForm", // a unique identifier for this form
})(BudgetModulesForm);
