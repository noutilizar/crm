import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Grid from "../Utils/Grid";
import { standardActions } from "../Utils/Grid/StandardActions";
import '../../../../style/fuentes.css';

export default class Modules extends Component {
    componentWillMount = () => {
        const { listar } = this.props;

        listar();
    }

    handleSearch = (e) => {
        const { listar, filterBudgetModules } = this.props;

        if (e.target.value != '') {
            filterBudgetModules(e.target.value);
        } else {
            listar();
        }
    }


    render() {
        const { data, loader, onSortChange, eliminar, page, listar } = this.props;
        console.log('props' ,this.props)
        return (
            <React.Fragment>
                <br />
                <h4 className="companias_title">Modulos</h4>
                <div className="uk-card uk-padding-small uk-padding uk-margin-auto">
                    <div className="uk-flex uk-flex-between uk-padding-remove-bottom uk-margin-auto-top@s">

                        <Link
                            className="uk-button uk-button-primary uk-border-rounded uk-margin-small-bottom uk-button-small uk-flex"
                            to="/budget-modules/create"
                        >
                            Crear Modulos
                            {/* <i style={{marginLeft: "2px"}} className="material-icons">add_circle_outline</i> */}
                        </Link>
                        <div className="uk-inline">
                            <span className="uk-form-icon uk-form-icon-flip"><i className="material-icons text-primary">search</i></span>
                                <input
                                    type="text"
                                    className="uk-input uk-border-rounded uk-width-1"
                                    onChange={this.handleSearch}
                                />
                        </div>
                    </div>
                    <div className="uk-card job-padding">
                        <Grid
                            hover
                            striped
                            data={data}
                            loading={loader}
                            onPageChange={listar}
                            onSortChange={onSortChange}
                            page={page}
                        >
                            <TableHeaderColumn
                                dataField="id"
                                dataAlign="center"
                                dataSort
                                dataFormat={standardActions({ 
                                    editar: "budget-modules", 
                                    ver: "budget-modules", 
                                    eliminar 
                                })}
                            >
                                
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                isKey
                                dataField="name_modules"
                                dataSort
                                className="text-primary"
                            >
                                NOMBRE
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="history_points"
                                dataSort
                                className="text-primary"
                            >
                                PUNTOS DE HISTORIA
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="history_points_error"
                                dataSort
                                className="text-primary"
                            >
                                P.H. MARGEN ERROR
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="total_history_points"
                                dataSort
                                className="text-primary"
                            >
                                TOTAL PUNTOS HISTORIA
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="total_start_hours"
                                dataSort
                                className="text-primary"
                            >
                                TOTAL HORAS INICIALES
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="total_start_hours_error"
                                dataSort
                                className="text-primary"
                            >
                                T.H. MARGEN ERROR
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="total_hours"
                                dataSort
                                className="text-primary"
                            >
                                HORAS TOTALES
                            </TableHeaderColumn>
                        </Grid>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
