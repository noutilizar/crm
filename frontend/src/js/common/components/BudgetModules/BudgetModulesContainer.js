import { connect } from 'react-redux'
import { actions } from  '../../../redux/modules/budgermodules/budgetmodules';

import ListBudgetModules from './ListBudgetModules'
import BudgetModules from './BudgetModules'

const ms2p = (state) => {
  return {
    ...state.budgetModules,
  };
};

const md2p = { ...actions };

//==================
// Conection List Company
//==================
  const ListBudget = connect(ms2p, md2p)(ListBudgetModules);
  
//==========================
// Conection Create Company
//==========================
const CreateBudgetModules = connect(ms2p, md2p)(BudgetModules);


  export const connectionBudgetModules = {
    ListBudget,
    CreateBudgetModules,
  }