import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { TableHeaderColumn } from "react-bootstrap-table";
import { Link } from 'react-router-dom';
import Grid from "../Utils/Grid";
import {standardActions} from "../Utils/Grid/StandardActions";
import ListadoSalesChannelContainer from './ListadoSalesChannelContainer';
import '../../../../style/fuentes.css';

class ListadoSalesChannel extends Component {
    componentWillMount = () => {
        this.props.listar();
    }


    render() {
        const {data, loader, searchChange, onPageChange, onSortChange, eliminar, page, listar} = this.props;

        return (
            <div>
                <br />
                <h4 className="canal_title">
                    CANALES DE VENTAS
                </h4>

                <div className="uk-card uk-margin-auto">

                    <div className="uk-flex uk-flex-between uk-padding-remove-bottom uk-margin-auto-top@s">
                        <Link
                            className="uk-button uk-button-primary uk-border-rounded uk-button-small uk-margin-small-bottom uk-flex"
                            to="/sales_channel/create"
                        >
                            Crear Canal
                            {/* <i style={{marginLeft: "2px"}} className="material-icons">add_circle_outline</i> */}
                        </Link>
                        <div className="uk-inline">
                            <span className="uk-form-icon uk-form-icon-flip"><i className="material-icons text-primary">search</i></span>
                                <input
                                    type="text"
                                    className="uk-input uk-border-rounded uk-width-1"
                                    onChange={e => searchChange(e.target.value)
                                    }
                                />
                        </div>
                    </div>

                    <Grid
                        hover
                        striped
                        className="uk-card job-padding"
                        data={data}
                        loading={loader}
                        onPageChange={listar}
                        onSortChange={onSortChange}
                        page={page}
                    >
                        <TableHeaderColumn
                            dataField="id"
                            dataAlign="center"
                            dataSort
                            dataFormat={standardActions({ editar: "sales_channel", ver: "sales_channel", eliminar })}
                        >
                        </TableHeaderColumn>

                        <TableHeaderColumn
                            isKey
                            dataField="name"
                            dataSort
                            className="text-primary"
                        >
                            NOMBRE CANAL
                        </TableHeaderColumn>

                        <TableHeaderColumn
                            dataField="commission_percentage"
                            dataSort
                            className="text-primary"
                        >
                            PORCENTAJE
                        </TableHeaderColumn>

                    </Grid>
                </div>
            </div>
        );
    }
}

export default ListadoSalesChannel;
