import { connect } from 'react-redux';
import { actions } from '../../../redux/modules/sales_channel/sales_channel';
import ListadoSalesChannel from './ListadoSalesChannel';


const ms2p = (state) => {
    return {
        ...state.sales_channel,
    };
};

const md2p = { ...actions };

export default connect(ms2p, md2p)(ListadoSalesChannel);