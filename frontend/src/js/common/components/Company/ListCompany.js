import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Grid from "../Utils/Grid";
import { standardActions } from "../Utils/Grid/StandardActions";
import '../../../../style/fuentes.css';

export default class Companies extends Component {
    componentWillMount = () => {
        const { listar } = this.props;

        listar();
    }

    handleSearch = (e) => {
        const { listar, filterCompany } = this.props;

        if (e.target.value != '') {
            filterCompany(e.target.value);
        } else {
            listar();
        }
    }


    render() {
        const { data, loader, onSortChange, eliminar, page, listar } = this.props;
        console.log(data);
        return (
            <React.Fragment>
                <br />
                <h4 className="companias_title">Companias</h4>
                <div className="uk-card uk-padding-small uk-padding uk-margin-auto">
                    <div className="uk-flex uk-flex-between uk-padding-remove-bottom uk-margin-auto-top@s">

                        <Link
                            className="uk-button uk-button-primary uk-border-rounded uk-margin-small-bottom uk-button-small uk-flex"
                            to="/company/create"
                        >
                            Crear Compañia
                            {/* <i style={{marginLeft: "2px"}} className="material-icons">add_circle_outline</i> */}
                        </Link>
                        <div className="uk-inline">
                            <span className="uk-form-icon uk-form-icon-flip"><i className="material-icons text-primary">search</i></span>
                                <input
                                    type="text"
                                    className="uk-input uk-border-rounded uk-width-1"
                                    onChange={this.handleSearch}
                                />
                        </div>
                    </div>
                    <div className="uk-card job-padding">
                        <Grid
                            hover
                            striped
                            data={data}
                            loading={loader}
                            onPageChange={listar}
                            onSortChange={onSortChange}
                            page={page}
                        >
                            <TableHeaderColumn
                                dataField="id"
                                dataAlign="center"
                                dataSort
                                dataFormat={standardActions({ editar: "company", ver: "company", eliminar })}
                            >
                                
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                isKey
                                dataField="name"
                                dataSort
                                className="text-primary"
                            >
                                NOMBRE
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="description"
                                dataSort
                                className="text-primary"
                            >
                                DESCRIPCION
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="industry"
                                dataSort
                                className="text-primary"
                                /*  dataFormat={(cell) => {
                                    return cell.label
                                }} */
                            >
                                INDUSTRIA
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="phone_number"
                                dataSort
                                className="text-primary"
                            >
                                TELEFONO
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="cities"
                                dataSort
                                className="text-primary"
                            >
                                CIUDAD
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="employee_number"
                                dataSort
                                className="text-primary"
                            >
                                # EMPLEADOS
                            </TableHeaderColumn>
                            
                        </Grid>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
