import React, { Component } from 'react'
import CreateForm from './CreateForm';
import '../../../../style/fuentes.css';

export default class Form extends Component {
    componentWillMount = () => {
        const {match, detalle} = this.props;
        if(match.params.id){
            const id = match.params.id;
            detalle(id);
        }
    }

    componentWillUnmount = () => {
        const {closeModal} = this.props;
        closeModal()
    }
    render() {
    

        const { match, location, actualizar,getIndustries, onSubmit} = this.props;
        const {openModal, closeModal, stateModal} = this.props;
        const funcionEnvio = match.params.id ? actualizar : onSubmit;
        const isActualizar = (match.params.id ) ? true : false
        return (
            <div>
                <br />
                <h4 className="companias_nueva_title">
                    {
                        (isActualizar) 
                        ?'DETALLE COMPAÑIA'
                        :'NUEVA COMPAÑIA'
                    }
                </h4>
                    <CreateForm
                        ver={location.pathname.includes('ver')}
                        onSubmit={funcionEnvio}
                        openModal={openModal}
                        closeModal={closeModal}
                        stateModal={stateModal}
                        funcionRegistro={this.props.registerIndustry}
                        getIndustries = { getIndustries }
                    />
            </div>
        )
    }
}
