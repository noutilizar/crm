import React from 'react';
import { Field, reduxForm, clearFields } from 'redux-form';
import { validate, validators } from 'validate-redux-form';
import { api } from "api";
import { renderField, renderNumber, AsyncSelectField } from '../Utils/renderField';
import CreateModal from '../Utils/renderField/createModal';
import Modal from '../Utils/Modal/ReactModal';
import CreateForm from '../industry/CreateForm';
import { phone } from '../../../utility/validation';
import '../../../../style/fuentes.css';

const CompanyForm = (props) => {
    const { handleSubmit, ver, getIndustries } = props;
    const { stateModal, openModal, closeModal } = props;
    return (
        <form onSubmit={handleSubmit} className="uk-card uk-card-job uk-padding uk-margin-auto">

            <div className="uk-child-width-1-3@s uk-grid uk-flex-center">
                <div>
                    <label className="companias_nombre">Nombre compañia</label>
                    <Field
                        name="name"
                        type="text"
                        component={renderField}
                        className="uk-input uk-border-rounded"
                        disabled={ver}
                    />
                </div>

                <div>
                    <label className="compania_descripcion">Descripción</label>
                    <Field
                        name="description"
                        type="text"
                        component={renderField}
                        className="uk-input uk-border-rounded"
                        disabled={ver}
                    />
                </div>
            </div>
            <div className="uk-child-width-1-3@s uk-grid uk-flex-center">
                <div>
                    <label className="companias_industria">Industria</label>
                    <div>
                        <Field
                            name="industry"
                            type="text"
                            placeholder="Seleccionar..."
                            loadOptions={getIndustries}
                            /* component={CreateModal} */
                            component={AsyncSelectField}
                            /* className="uk-input uk-border-rounded" */
                            disabled={ver}
                        />

                    </div>

                    <div>
                        <button
                            disabled={ver}
                            className="uk-button uk-button-link uk-align-right uk-margin-remove-bottom "
                            onClick={() => openModal()}
                        >
                            + Industria

                        </button>
                    </div>
                </div>
                <div>
                    <label className="compania_telefono">Telefono</label>
                    <Field
                        name="phone_number"
                        type="tel"
                        /* validate={ phone } */
                        component={renderField}
                        className="uk-input uk-border-rounded"
                        disabled={ver}
                    />
                </div>
            </div>

            <div className="uk-child-width-1-3@s uk-grid uk-flex-center">
                <div>
                    <label className="compania_ciudad">Ciudad</label>
                    <Field
                        name="cities"
                        type="text"
                        component={renderField}
                        className="uk-input uk-border-rounded"
                        disabled={ver}
                    />
                </div>
                <div>
                    <label className="compania_empleados">Numero de Empleado</label>
                    <Field
                        name="employee_number"
                        type="text"
                        component={renderNumber}
                        className="uk-input uk-border-rounded"
                        disabled={ver}
                    />
                </div>
            </div>
            <div className="uk-child-width-1-3@s uk-grid uk-flex-center">
                <div>

                    <label className="compania_link">Pagina LinkedIn</label>
                    <Field
                        name="linkedIn_page"
                        type="text"
                        component={renderField}
                        className="uk-input uk-border-rounded"
                        disabled={ver}
                    />
                </div>

                <div>
                    <label className="compania_facebook">Pagina Facebook</label>
                    <Field
                        name="facebook_page"
                        type="text"
                        component={renderField}
                        className="uk-input uk-border-rounded"
                        disabled={ver}
                    />
                </div>
            </div>
            <div className="uk-child-width-1-3@s uk-grid uk-flex-center">
                <div>
                    <label className="ingresos_anual">Ingresos Anuales</label>
                    <Field
                        name="annual_income"
                        type="text"
                        component={renderNumber}
                        className="uk-input uk-border-rounded"
                        disabled={ver}
                    />
                </div>
                <div/>
            </div>
            <br />
            <br/>
            <div className="uk-flex uk-flex-center">
                <a
                    className="uk-button uk-button-secondary uk-border-rounded uk-button-small uk-flex"
                    href="/#/company"
                    etapas={null}
                >
                    Cancelar
                    {/* <i style={{ marginLeft: "2px" }} className="material-icons">cancel</i> */}

                </a>

                {

                    !ver
                    && (
                        <button
                            type="submit"
                            className="uk-button uk-button-primary uk-border-rounded uk-button-small uk-margin-large-left uk-flex"
                        >
                            Guardar
                            {/* <i style={{ marginLeft: "2px" }} className="material-icons">save</i> */}
                        </button>
                    )
                }
            </div>

            <Modal showModal={stateModal}>
                <CreateForm
                    isNested
                    closeModal={closeModal}
                    onSubmit={props.funcionRegistro}
                />
            </Modal>

        </form>
    );
};


export default reduxForm({
    form: 'companyForm', // a unique identifier for this form
    validate: (data) => {
        return validate(data, {
            name: validators.exists()('Este campo es requerido'),
            /* business_lines: validators.exists()('Este campo es requerido'),
            sales_channel: validators.exists()('Este campo es requerido'), */
        });
    },
})(CompanyForm);
