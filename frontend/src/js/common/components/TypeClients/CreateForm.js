import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { validate, validators } from 'validate-redux-form'
import { renderField, SelectField,renderCurrencyDolar} from '../Utils/renderField/renderField'

const typeClient = [
    { value: 1, label: 'Local'},
    { value: 2, label: 'Internacional'},
];

const CreateForm=(props)=>{
    const {handleSubmit, ver, isNested, closeModal}=props
    return(
        <form on onSubmit={handleSubmit} className="uk-card uk-card-default uk-padding uk-margin-auto uk-flex-1 uk-flex-center">
            <div className="uk-margin-bottom">
                <div className="uk-child-width-1-2@s uk-grid">
                    <div>
                        <label>Tipo de cliente</label>
                        <Field
                            name="name_type"
                            className="uk-input"
                            type="text"
                            component={SelectField}
                            options={typeClient}
                            disabled={ver}
                        />
                    </div>

                    <div>
                        <label>Tarifa por hora</label>
                        <Field
                            name="rate_for_hour"
                            className="uk-input uk-border-rounded"
                            type="number"
                            /*component={renderField}*/
                            component={renderCurrencyDolar}
                            disabled={ver}
                        />
                    </div>
                    <div>
                        <label>Tarifa hora de alta prioridad soporte</label>
                        <Field
                            name="rate_for_hour_priority_support"
                            className="uk-input uk-border-rounded"
                            type="number"
                            /*component={renderField}*/
                            component={renderCurrencyDolar}
                            disabled={ver}
                        />
                    </div>
                    <div>
                        <label>Tarifa por hora de soporte normal</label>
                        <Field
                            name="rate_for_hour_support_normal"
                            className="uk-input uk-border-rounded"
                            type="number"
                            /*component={renderField}*/
                            component={renderCurrencyDolar}
                            disabled={ver}
                        />
                    </div>
                    <div>
                        <label>Tarifa por mes </label>
                        <Field
                            name="rate_for_month"
                            className="uk-input uk-border-rounded"
                            type="number"
                            /*component={renderField}*/
                            component={renderCurrencyDolar}
                            disabled={ver}
                        />
                    </div>

                </div>
            </div>
            <div className="uk-width-1-2@m uk-margin-auto">
                <div className="uk-flex uk-flex-center">

                    {isNested ?
                        <button
                            type="button"
                            className='uk-button uk-button-secondary uk-button-small uk-border-rounded uk-flex'
                            onClick={() => closeModal()}
                        >
                            Cancelar
                        {<i style={{ marginLeft: "2px" }} className="material-icons">cancel</i>}
                        </button>
                        :
                        <a
                            className='uk-button uk-button-secondary uk-button-small uk-border-rounded uk-flex'
                            href='/#/type_client'
                        >
                            Cancelar
                        {<i style={{ marginLeft: "2px" }} className="material-icons">cancel</i>}
                        </a>
                    }

                    {

                        !ver &&
                        (
                            <button
                                type={isNested ? "button" : "submit"}
                                onClick={isNested ? handleSubmit : null}
                                className='uk-button uk-button-primary uk-button-small uk-border-rounded uk-flex uk-margin-small-left'
                            >
                                Guardar
                                {<i style={{ marginLeft: "2px" }} className="material-icons">save</i>}
                            </button>
                        )
                    }

                </div>

            </div>

        </form>
    )
}
export default reduxForm({
    form: 'createTypeClient', // a unique identifier for this form
    validate: (data) => {
        return validate(data, {
            /*name_type: validators.exists()('Este campo es requerido'),*/
        });
    },
})(CreateForm);
