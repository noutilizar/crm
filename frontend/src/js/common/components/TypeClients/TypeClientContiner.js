import { connect } from 'react-redux'
import { actions } from  '../../../redux/modules/type_client/type_client'

import TypeClienstScreen from './ListTypeClients'
import TypeClientScreen from './TypeClient'


const ms2p = (state) => {
  return {
    ...state.type_client,
  };
};

const md2p = { ...actions };

//===========================
// Conection List Typeclient
//===========================
const ListTypeClient = connect(ms2p, md2p)(TypeClienstScreen);
  
//==========================
// Conection Create Type Client
//=========================?
const CreateTypeClient = connect(ms2p, md2p)(TypeClientScreen);


  export const connectionTypeClient = {
    ListTypeClient,
    CreateTypeClient
  }