import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Grid from "../Utils/Grid";
import { standardActions } from "../Utils/Grid/StandardActions";
import '../../../../style/fuentes.css';

export default class SalesFunnel extends Component {
    componentWillMount = () => {
        const { listar } = this.props;

        listar();
    }

    handleSearch = (e) => {
        const { listar, filterSalesFunnel } = this.props;

        if (e.target.value != '') {
            filterSalesFunnel(e.target.value);
        } else {
            listar();
        }
    }


    render() {
        const { data, loader, onSortChange, eliminar, page, listar } = this.props;
        console.log(data)
        return (
            <React.Fragment>
                <br />
                <h4 className="embudo_title">EMBUDO DE VENTAS</h4>
                
                <div className="uk-card uk-margin-auto">
                    <div className="uk-flex uk-flex-between bottom-padding uk-padding-remove-bottom uk-margin-auto-top@s">

                        <Link
                            className="uk-button uk-button-primary uk-border-rounded uk-margin-small-bottom uk-button-small uk-flex"
                            to="/sales-funnel/create"
                        >
                            Crear Embudo
                            {/* <i style={{marginLeft: "2px"}} className="material-icons">add_circle_outline</i> */}
                        </Link>
                        <div className="uk-inline">
                            <span className="uk-form-icon uk-form-icon-flip"><i className="material-icons text-primary">search</i></span>
                                <input
                                    type="text"
                                    className="uk-input uk-border-rounded uk-width-1"
                                    onChange={this.handleSearch}
                                />
                        </div>

                    </div>
                    <div className="job-padding">
                        <Grid
                            hover
                            striped
                            data={data}
                            loading={loader}
                            onPageChange={listar}
                            onSortChange={onSortChange}
                            page={page}
                        >
                            <TableHeaderColumn
                                dataField="id"
                                dataAlign="center"
                                dataSort
                                dataFormat={standardActions({ editar: "sales-funnel", ver: "sales-funnel", eliminar })}
                            >
                                
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                isKey
                                dataField="name"
                                dataSort
                                className="text-primary"
                            >
                                NOMBRE
                            </TableHeaderColumn>

                            <TableHeaderColumn
                                dataField="business_lines"
                                dataSort
                                dataFormat={(cell) => {
                                    return cell.label;
                                }}
                                className="text-primary"
                            >
                                LINEA DE NEGOCIO
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="percentage_line"
                                dataSort
                                className="text-primary"
                            >
                                PORCENTAJE
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="sales_channel"
                                dataSort
                                dataFormat={(cell) => {
                                    return cell.label;
                                }}
                                className="text-primary"
                            >
                                CANAL DE VENTAS
                            </TableHeaderColumn>

                        </Grid>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
