import React, {Component} from 'react';
import { TableHeaderColumn } from "react-bootstrap-table";
import Grid from "../Utils/Grid";
import {standardActions} from "../Utils/Grid/StandardActions";
import '../../../../style/fuentes.css'


class ListUsers extends Component {
    componentWillMount = () => {
        const { listar } = this.props;

        listar();
    }

    handleSearch = (e) => {
        const { listar, filterUsers } = this.props;
        if (e.target.value != '') {
            filterUsers(e.target.value);
        } else {
            listar();
        }
    }

    render() {
        const {data, loader, onSortChange, eliminar, listar, page } = this.props;
        return (
            <React.Fragment>
                <br />
                <h4 className="fuente">
                    USUARIOS
                </h4>
   
                <div className="uk-card  uk-margin-auto">
                    <div className="uk-flex uk-flex-between botton-padding uk-padding-remove-bottom uk-margin-auto-top@s">
                        
                        <a
                            className="uk-button uk-button-primary uk-border-rounded uk-button-small uk-margin-small-bottom uk-flex"
                            href="/#/users/create"
                        >
                            Crear Usuario
                            {/* <i style={{marginLeft: "2px"}} className="material-icons">add_circle_outline</i> */}
                        </a>
                        
                        <div className="uk-inline">
                            <span className="uk-form-icon uk-form-icon-flip"><i className="material-icons text-primary">search</i></span>
                            <input
                            type="text"
                            className="uk-input uk-border-rounded "
                            onChange={this.handleSearch}
                            
                            />
                        </div>



                    </div>

                    <Grid
                        hover
                        striped
                        className="job-padding"
                        data={data}
                        loading={loader}
                        onPageChange={listar}
                        onSortChange={onSortChange}
                        page={page}
                        
                        >
                        <TableHeaderColumn
                            dataField="id"
                            dataAlign="center"
                            dataSort
                            dataFormat={standardActions({ editar: "users", ver: "users", eliminar })}
                        >
                            
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            isKey
                            dataField="username"
                            dataSort
                            className="text-primary"
                        >
                            USUARIO
                        </TableHeaderColumn>

                        <TableHeaderColumn
                            dataField="first_name"
                            dataSort
                            className="text-primary"
                        >
                            NOMBRE
                        </TableHeaderColumn>
                        {/* <TableHeaderColumn
                            dataField="last_name"
                            dataSort
                        >
                            Apellido
                        </TableHeaderColumn> */}
                        <TableHeaderColumn
                            dataField="role"
                            dataSort
                            className="text-primary"
                        >
                            ROLL
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            dataField="email"
                            dataSort
                            className="text-primary"
                        >
                            E-MAIL
                        </TableHeaderColumn>

                    </Grid>
                </div>
            </React.Fragment>
        );
    }
}

export default ListUsers;
