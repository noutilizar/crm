import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { validate, validators } from 'validate-redux-form'
import { renderField, SelectField,renderCurrencyDolar} from '../Utils/renderField/renderField'

const typeClient = [
    { value: 1, label: 'Local'},
    { value: 2, label: 'Internacional'},
];

const CreateForm=(props)=>{
    const {handleSubmit, ver, isNested, closeModal}=props
    return(
        <form on onSubmit={handleSubmit} className="uk-card uk-card-default uk-padding uk-margin-auto uk-flex-1 uk-flex-center">
            <div className="uk-margin-bottom">
                <div className="uk-child-width-1-2@s uk-grid">

                    <div>
                        <label>Nombre Project</label>
                        <Field
                            name="name_project"
                            className="uk-input"
                            type="text"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>

                    
                    <div>
                        <label>Nombre Tipo</label>
                        <Field
                            name="name_type"
                            className="uk-input"
                            type="text"
                            component={SelectField}
                            options={typeClient}
                            disabled={ver}
                        />
                    </div>

                    <div>
                        <label>Promedio de sprint</label>
                        <Field
                            name="average_sprint"
                            className="uk-input uk-border-rounded"
                            type="number"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>
                    <div>
                        <label>Margen de error</label>
                        <Field
                            name="error_range"
                            className="uk-input uk-border-rounded"
                            type="number"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>
                    <div>
                        <label>Meses de soporte</label>
                        <Field
                            name="month_support"
                            className="uk-input uk-border-rounded"
                            type="number"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>
                    <div>
                        <label>Horas de soporte por mes</label>
                        <Field
                            name="hour_for_support_month"
                            className="uk-input uk-border-rounded"
                            type="number"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>
                    <div>
                        <label>Horas de implementacion</label>
                        <Field
                            name="hour_implementation"
                            className="uk-input uk-border-rounded"
                            type="number"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>
                    <div>
                        <label>Horas de QA</label>
                        <Field
                            name="hour_for_QA"
                            className="uk-input uk-border-rounded"
                            type="number"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>
                    <div>
                        <label>Horas de manual de usuario</label>
                        <Field
                            name="hour_userManual"
                            className="uk-input uk-border-rounded"
                            type="number"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>
                    <div>
                        <label>Horas de manual tecnico</label>
                        <Field
                            name="hour_technicalManual"
                            className="uk-input uk-border-rounded"
                            type="number"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>
                    <div>
                        <label>Horas de despliegue</label>
                        <Field
                            name="hour_deployment"
                            className="uk-input uk-border-rounded"
                            type="number"
                            component={renderField}
                            disabled={ver}
                        />
                    </div>



                </div>
            </div>
            <div className="uk-width-1-2@m uk-margin-auto">
                <div className="uk-flex uk-flex-center">

                    {isNested ?
                        <button
                            type="button"
                            className='uk-button uk-button-secondary uk-button-small uk-border-rounded uk-flex'
                            onClick={() => closeModal()}
                        >
                            Cancelar
                        {/* {<i style={{ marginLeft: "2px" }} className="material-icons">cancel</i>} */}
                        </button>
                        :
                        <a
                            className='uk-button uk-button-secondary uk-button-small uk-border-rounded uk-flex'
                            href='/#/type_project'
                        >
                            Cancelar
                        {/* {<i style={{ marginLeft: "2px" }} className="material-icons">cancel</i>} */}
                        </a>
                    }

                    {

                        !ver &&
                        (
                            <button
                                type={isNested ? "button" : "submit"}
                                onClick={isNested ? handleSubmit : null}
                                className='uk-button uk-button-primary uk-button-small uk-border-rounded uk-flex uk-margin-small-left'
                            >
                                Guardar
                                {/* {<i style={{ marginLeft: "2px" }} className="material-icons">save</i>} */}
                            </button>
                        )
                    }

                </div>

            </div>

        </form>
    )
}
export default reduxForm({
    form: 'createTypeProject', // a unique identifier for this form
    validate: (data) => {
        return validate(data, {
            /*name_type: validators.exists()('Este campo es requerido'),*/
        });
    },
})(CreateForm);
