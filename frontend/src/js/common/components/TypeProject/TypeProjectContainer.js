import { connect } from 'react-redux'
import { actions } from  '../../../redux/modules/type_project/type_project'

import TypeProjectsScreen from './ListTypeProject'
import TypeProjectScreen from './TypeProject'


const ms2p = (state) => {
  return {
    ...state.type_project,
  };
};

const md2p = { ...actions };

//===========================
// Conection List Project
//===========================
const ListTypeProject = connect(ms2p, md2p)(TypeProjectsScreen);
  
//==========================
// Conection Create Project
//=========================?
const CreateTypeProject = connect(ms2p, md2p)(TypeProjectScreen);


  export const connectionTypeProject = {
    ListTypeProject,
    CreateTypeProject
  }