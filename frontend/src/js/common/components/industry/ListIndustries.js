import React, { Component } from 'react';
import Grid from '../Utils/Grid';
import {standardActions} from '../Utils/Grid/StandardActions';
import '../../../../style/fuentes.css';

export default class ListIndustries extends Component {
    componentWillMount = () => {
        const { listar } = this.props;

        listar();
    }

    handleSearch = (e) => {
        const { listar, filterIndustry } = this.props;

        if (e.target.value != '') {
            filterIndustry(e.target.value);
        } else {
            listar();
        }
    }

    render() {
        const {data, loader, onSortChange, eliminar, listar, page } = this.props;
        return (
            <React.Fragment>
                <br />
                <h4 className="contacts_title ">INDUSTRIAS</h4>

                <div className="uk-card  uk-padding-small uk-padding uk-margin-auto industry-list-div">
                    <div className=" list-button-industry uk-flex uk-flex-between uk-padding uk-padding-remove-bottom uk-margin-auto-top@s">
                       <a
                            className="uk-button uk-button-primary uk-border-rounded uk-margin-small-bottom uk-button-small uk-flex"
                            href="/#/industry/create"
                        >

                            {/* <i style={{marginRight: "4px"}} className="material-icons">add_circle_outline</i> */}
                            Crear Industria
                        </a>
                        <input
                            className="uk-input uk-border-rounded uk-width-1-5"
                            placeholder="Buscar ..."
                            onChange={this.handleSearch}
                        />
                    </div>

                    <Grid
                        className="uk-padding"
                        data={data}
                        loading={loader}
                        onPageChange={listar}
                        onSortChange={onSortChange}
                        page={page}
                        striped
                        hover
                    >
                                                <TableHeaderColumn
                            dataField="id"
                            dataAlign="center"
                            dataSort
                            dataFormat={standardActions({ editar: "industry", ver: "industry", eliminar })}
                            className="fuente_header_table"
                        >
                            Acciones
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            isKey
                            dataField="name"
                            dataSort
                            className="fuente_header_table"
                        >
                            Nombre industria
                        </TableHeaderColumn>

                    </Grid>
                </div>
            </React.Fragment>
        );
    }
}
