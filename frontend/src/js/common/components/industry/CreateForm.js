import React from "react";
import { Field, reduxForm } from "redux-form";
import { validate, validators } from "validate-redux-form";
import { renderField } from "../Utils/renderField/renderField";
import "./industrystyle.css";

const CreateForm = (props) => {
    const { handleSubmit, ver, isNested, closeModal } = props;

    return (
        <form onSubmit={handleSubmit}>
            <fieldset className="uk-fieldset">
                <div className="uk-margin">
                    <label className="uk-align-center form-industry-label">
                        Nombre industria
                    </label>
                    <Field
                        name="name"
                        label="Nombre"
                        type="text"
                        component={renderField}
                        className="uk-input  uk-form-width-medium uk-align-center uk-border-rounded form-industry-input"
                        disabled={ver}
                    />
                </div>

                <div className=" form-industry-buttons">
                    {/* <div className="uk-flex uk-flex-center "> */}
                    <div className="button-container uk-display-block uk-float-left button-cancelar">
                        {isNested ? (
                            <button
                                type="button"
                                className="uk-button uk-button-secondary uk-button-small uk-border-rounded "
                                onClick={() => closeModal()}
                            >
                                Cancelar
                                {
                                    // <i
                                    //     style={{ marginLeft: "2px" }}
                                    //     className="material-icons"
                                    // >
                                    //     cancel
                                    // </i>
                                }
                            </button>
                        ) : (

                            <button className="uk-button uk-button-secondary uk-button-small uk-border-rounded ">
                                <a
                                    
                                    href="/#/industry"
                                >
                                    Cancelar
                                    {
                           
                                    }
                                </a>
                            </button>
                        )}
                    </div>
                    <div className=" button-container uk-display-block uk-float-left  button-guardar" >
                        {!ver && (
                            <button
                                type={isNested ? "button" : "submit"}
                                onClick={isNested ? handleSubmit : null}
                                className="uk-button uk-button-primary uk-button-small uk-border-rounded uk-margin-left "
                            >
                                Guardar
                                {
                                    
                                }
                            </button>
                        )}
                    </div>
                    {/* </div> */}
                </div>
            </fieldset>
        </form>
    );
};

export default reduxForm({
    form: "createIndustry", // a unique identifier for this form
    validate: (data) => {
        return validate(data, {
            name: validators.exists()("Este campo es requerido"),
        });
    },
})(CreateForm);
