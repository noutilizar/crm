import React, { Component } from 'react'
import CreateForm from './CreateForm';
import '../../../../style/fuentes.css';
import "./industrystyle.css";
export default class Industry extends Component {

    componentWillMount = () => {

        const { match, detalle } = this.props;
        if (match.params.id) {
            const id = match.params.id;
            detalle(id);
        }
    }

    render() {


        const { onSubmit, actualizar, match, location } = this.props
        const fn = match.params.id ? actualizar : onSubmit
        const isActualizar = (match.params.id) ? true : false


        return (
            <div>
                <br />
                <h4 className="industrias_nueva_title ">
                    {
                        (isActualizar)
                            ? 'Detalle Industria'
                            : 'NUEVA INDUSTRIA'
                    }
                </h4>
                <div className="industry-div">
                <CreateForm
                    onSubmit={fn}
                    ver={location.pathname.includes('ver') && true}
                />
            </div>
            </div>
        )
    }
}
