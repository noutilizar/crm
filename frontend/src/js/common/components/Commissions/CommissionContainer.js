import { connect } from 'react-redux'
import {actions} from '../../../redux/modules/commissions/commission';


import CommissionsScreen from './ListCommissions'
import CommissionScreen from './Commission'


const ms2p = (state) => {
  return {
    ...state.commission,
  };
};

const md2p = { ...actions };

//===========================
// Conection Commission
//===========================
const ListCommission = connect(ms2p, md2p)(CommissionsScreen);
  
//==========================
// Conection Commission
//=========================?
const CreateCommission= connect(ms2p, md2p)(CommissionScreen);


export const connectionCommission = {
  ListCommission,
  CreateCommission
}