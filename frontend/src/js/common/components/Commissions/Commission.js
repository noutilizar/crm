import React, { Component } from 'react'
import CreateForm from './CreateForm';

export default class Commission extends Component{

    componentWillMount=()=>{
        const{match,detalle}=this.props

        if(match.params.id)
            detalle(match.params.id)
        
        



    }

    render() {


        const { registrar,actualizar,match} = this.props

        const funcion = match.params.id ? actualizar : registrar
        const isActualizar = (match.params.id) ? true : false


        return (
            <div>
                <br />
                <h2 className="uk-margin-auto uk-text-bold uk-text-lead">
                   Configuracion General
                </h2>
                <CreateForm
                    onSubmit={funcion}
                    isActualizar={isActualizar}
                />
            </div>
        )
    }
}

