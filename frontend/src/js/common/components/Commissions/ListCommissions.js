import React, { Component } from 'react';
import Grid from '../Utils/Grid';
import {standardActions} from '../Utils/Grid/StandardActions';

export default class ListCommissions extends Component {
    componentWillMount = () => {
        const { listar } = this.props;

        listar();
    }

    handleSearch = (e) => {
        const { listar, filterTypeClient } = this.props;

        if (e.target.value != '') {
            filterTypeClient(e.target.value);
        } else {
            listar();
        }
    }

    render() {
        const {data, loader, onSortChange, eliminar, listar, page } = this.props;
        console.log('DATA DEL CONPONENTE',data);
        return (
            <React.Fragment>
                <br />
                <h3 className="uk-text-lead uk-text-bold">Comisiones de usuarios</h3>

                <div className="uk-card uk-card-default uk-padding-small uk-padding uk-margin-auto">
                    <div className="uk-flex uk-flex-between uk-padding uk-padding-remove-bottom uk-margin-auto-top@s">
                       <a
                            className="uk-button uk-button-primary uk-border-rounded uk-margin-small-bottom uk-button-small uk-flex"
                            href="/#/commission/create"
                        >

                            {/* <i style={{marginRight: "4px"}} className="material-icons">add_circle_outline</i> */}
                            Agregar
                        </a>
                        <input
                            className="uk-input uk-border-rounded uk-width-1-5"
                            placeholder="Buscar ..."
                            onChange={this.handleSearch}
                        />
                    </div>
                    {data && 

                    <Grid
                        className="uk-padding"
                        data={data}
                        loading={loader}
                        onPageChange={listar}
                        onSortChange={onSortChange}
                        page={page}
                    >
                        <TableHeaderColumn
                            dataField="username"
                            dataFormat={(cell,row)=>{
                                let name=cell
                                if (row.fist_name!=""&& row.last_name!="")
                                    name=row.first_name+" "+row.last_name

                                    return name
                            }}
                            dataSort
                        >
                            Nombre del usuario
                        </TableHeaderColumn>

                        {/*<TableHeaderColumn
                            dataField="name_type"
                            dataSort
                        >
                            Nombre Tipo
                        </TableHeaderColumn>

                        <TableHeaderColumn
                            dataField="average_sprint"
                            dataSort
                        >
                            Promedio del sprint
                        </TableHeaderColumn>


                        <TableHeaderColumn
                            dataField="error_range"
                            dataSort
                        >
                            Margen de error
                        </TableHeaderColumn>

                        <TableHeaderColumn
                            dataField="month_support"
                            dataSort
                        >
                            Meses de soporte
                        </TableHeaderColumn>

                        <TableHeaderColumn
                            dataField="rate_for_hour"
                            dataSort
                            dataFormat={cell => `$. ${cell.toFixed(2)}`
                            }
                        >
                            Tarifa por hora
                        </TableHeaderColumn>

                        <TableHeaderColumn

                            dataField="rate_for_hour_priority_support"
                            dataSort
                            dataFormat={cell => `$. ${cell.toFixed(2)}`
                            }
                        >
                            Tarifa hora de alta prioridad soporte
                        </TableHeaderColumn>

                        <TableHeaderColumn
                            dataField="rate_for_hour_support_normal"
                            dataSort
                            dataFormat={cell => `$. ${cell.toFixed(2)}`
                            }
                        >
                            Tarifa por hora de soporte normal
                        </TableHeaderColumn>

                        <TableHeaderColumn
                            dataField="rate_for_month"
                            dataSort
                            dataFormat={cell => `$. ${cell.toFixed(2)}`
                            }
                        >
                            Tarifa por mes
                        </TableHeaderColumn>*/}
                        
                        <TableHeaderColumn
                            dataField="id"
                            dataAlign="center"
                            dataSort
                            isKey
                            dataFormat={standardActions({ editar: "commission", ver: "commission", eliminar })}
                        >
                            Acciones
                        </TableHeaderColumn>
                    </Grid>
    }
                </div>
            </React.Fragment>
        );
    }
}
