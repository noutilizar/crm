import React from 'react'
import { Field, FieldArray, reduxForm } from 'redux-form'
import { search } from 'superagent';
import { validate, validators } from 'validate-redux-form'
import { renderField, SelectField,AsyncSelectField} from '../Utils/renderField/renderField'
import {api} from '../../../utility/api'

import './style.css'

const typeClient = [
    { value: 1, label: 'Local'},
    { value: 2, label: 'Internacional'},
];

const getUsuarios=(search)=>{

  let users = []

  return api.get('user',{search}).then((response)=>{
          
         users = response.results.map( user => ({ value: user.id, label: user.username }) )
          return users
      })
      .catch((err)=>{
          return users
      })

}
const renderCommissions = ({ fields, meta: { error, submitFailed } }) => (
    <ul>
      <li className="list-style">
        <button 
          className="uk-button uk-button-secondary uk-button-small uk-border-rounded uk-flex uk-margin-small-left" 
          type="button" onClick={() => fields.push({})}
        >
          Agregar comisiones
        </button>
        {submitFailed && error && <span>{error}</span>}
      </li>
      {fields.map((commission, index) => (
        <li className="list-style" key={index}>
          <h4>Comision #{index + 1}</h4>
          <div>
              <label>Rango inicial</label>
              <Field
                name={`${commission}.starting_range`}
                type="number"
                component={renderField}
              />
          </div>
          <div>
              <label>Rango final</label>
              <Field
                name={`${commission}.final_rank`}
                type="number"
                component={renderField}
              />
          </div>
          <div>
              <label>Porcentaje de comision</label>
              <Field
                name={`${commission}.percentage_commission`}
                type="number"
                component={renderField}
              />
          </div>
          <div>
              <label>Porcentaje de comision</label>
              <Field
                name={`${commission}.name_type`}
                type="text"
                component={SelectField}
                options={typeClient}
              />
          </div>
          <div className="style-cancel-commission">
            <button
              type="button"
              title="Remove Member"
              onClick={() => fields.remove(index)}
              className="uk-button uk-button-danger uk-button-small uk-border-rounded uk-flex uk-margin-small-left"
            >
              Cancelar Comision
              {/* {<i style={{ marginLeft: "2px" }} className="material-icons">cancel</i>} */}
            </button>
          </div>

        </li>
      ))}
    </ul>
)

const CreateForm=(props)=>{
    const {handleSubmit, ver, isNested, closeModal}=props

    return(
        <form onSubmit={handleSubmit}  className="uk-card uk-card-default uk-padding uk-margin-auto uk-flex-1 uk-flex-center">
            <div className="uk-margin-bottom">
                <div className="uk-child-width-1-2@s uk-grid">
                    <div>
                        <label>Nombre de usuario</label>
                        <Field
                            name="name_user"
                            className=""
                            type="text"
                            component={AsyncSelectField}
                            loadOptions={getUsuarios}
                            disabled={ver}
                        />
                    </div>
                    
                    <div>
                        <FieldArray
                            name="commissions"
                            component={renderCommissions}
                        />
                    </div>

                    {/*<button
                      type="submit"
                      className="uk-button uk-button-primary uk-border-rounded uk-margin-small-bottom uk-button-small uk-flex"
                    >
                      Agregar

                    </button>*/}
                    <div className="uk-width-1-2@m uk-margin-auto">
                      <div className="uk-flex uk-flex-left">

                    {isNested ?
                        <button
                            type="button"
                            className='uk-button uk-button-secondary uk-button-small uk-border-rounded uk-flex'
                            onClick={() => closeModal()}
                        >
                            Cancelar
                        {/* {<i style={{ marginLeft: "2px" }} className="material-icons">cancel</i>} */}
                        </button>
                        :
                        <a
                            className='uk-button uk-button-secondary uk-button-small uk-border-rounded uk-flex'
                            href='/#/commission'
                        >
                            Cancelar
                        {/* {<i style={{ marginLeft: "2px" }} className="material-icons">cancel</i>} */}
                        </a>
                    }

                    {

                        !ver &&
                        (
                            <button
                                type={isNested ? "button" : "submit"}
                                onClick={isNested ? handleSubmit : null}
                                className='uk-button uk-button-primary uk-button-small uk-border-rounded uk-flex uk-margin-small-left'
                            >
                                Guardar
                                {/* {<i style={{ marginLeft: "2px" }} className="material-icons">save</i>} */}
                            </button>
                        )
                    }

                </div>

            </div>
                </div>
            </div>
        </form>
    )

}

export default reduxForm({
    form: 'createCommissions', // a unique identifier for this form
    validate: (data) => {
        return validate(data, {
            /*name_type: validators.exists()('Este campo es requerido'),*/
        });
    },
})(CreateForm);