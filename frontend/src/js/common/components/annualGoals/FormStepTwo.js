import React from "react";
import { reduxForm, Field, FieldArray } from "redux-form";
import { Link } from "react-router-dom";
import { renderField, renderCurrency } from "../Utils/renderField";
import { required } from "../../../utility/validation";

const parseNumber = (value) => {
    if (value) {
        return Number(value);
    }
    return "";
};
const renderFields = ({ fields, onChangeFieldArray, ver }) => (
    <div>
        {fields.map((field) => (
            <div
                key={`${field}.id`}
                className="uk-child-width-1-2@s uk-grid annual-goals-divcol2"
            >
                <div>
                    <h4 className="uk-width-1-2 uk-text-bold presupuesto_subtitle">
                        Lineas de negocios
                    </h4>
                    <span>
                        <Field
                            name={`${field}.name`}
                            disabled
                            className="uk-input uk-border-rounded annualgoal-step2-field annualgoal-form-input1"
                            component={renderField}
                        />
                    </span>
                </div>
                <div className="uk-child-width-1-2@s uk-grid annual-goals-divcol2">
                    <div>
                    <h4 className="uk-text-bold presupuesto_subtitle">
        Porcentaje
    </h4>
                <span className="uk-width-1-6">
                    <Field
                        name={`${field}.percentage`}
                        className="uk-input uk-border-rounded annualgoal-form-input1 "
                        component={renderField}
                        type="number"
                        parse={parseNumber}
                        validate={required}
                        onChange={(e) => {
                            onChangeFieldArray(e.target);
                        }}
                        disabled={ver}
                    />
                </span></div>
                <div>
                <h4 className=" uk-text-bold presupuesto_subtitle">
        Equivalente en quetzales
    </h4>
                <span className="uk-width-1-3">
                    <Field
                        name={`${field}.percentage_quetzal`}
                        disabled
                        className="uk-input uk-border-rounded annualgoal-form-input1 "
                        component={renderCurrency}
                    />
                </span>
           </div>
</div>
           
           
           
           
           
           
           
           
           
           
           
           
           
           
           
            </div>
        ))}
    </div>
);
const FormStepTwo = (props) => {
    const {
        handleSubmit,
        previousPage,
        onChangeFieldArray,
        ver,
        actualizar,
    } = props;
    return (
        <form
            onSubmit={handleSubmit}
            className="uk-card  uk-padding uk-margin-auto annualgoals-form2"
        >
            <div className="uk-card uk-card-primary uk-width-1-1 uk-margin">
                <h3 className="uk-card-title uk-text-lead title__text anual_f2text">
                    LINEAS DE NEGOCIO
                </h3>
            </div>

            <FieldArray
                name="lines"
                onChangeFieldArray={onChangeFieldArray}
                ver={ver}
                component={renderFields}
            />

            <br></br>
            <div className="uk-flex uk-flex-between uk-margin annual-form2-button">
                <button
                    className="uk-button uk-button-secondary uk-border-rounded uk-button-small uk-flex"
                    type="button"
                    onClick={() => previousPage()}
                >
                    Regresar
                    {/* <i style={{marginLeft: "2px"}} className="material-icons">arrow_back</i> */}
                </button>

                {!ver && (
                    <button
                        type="submit"
                        className="uk-button uk-button-primary uk-border-rounded uk-button-small uk-flex"
                    >
                        {actualizar ? "Actualizar" : "Registrar"}
                        {/* <i style={{marginLeft: "2px"}} className="material-icons">save</i> */}
                    </button>
                )}
            </div>
        </form>
    );
};

export default reduxForm({
    form: "annualGoalsForm",
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
})(FormStepTwo);
