import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Grid from '../Utils/Grid';
import { standardActions } from '../Utils/Grid/StandardActions';
import 'uikit/dist/js/uikit-icons';
import '../../../../style/fuentes.css';

export default class AnnualGoalsList extends Component {
    componentWillMount = () => {
        const { listar } = this.props;
        listar();
    };

    render() {
        const {
            data,
            loader,
            onSortChange,
            eliminar,
            listar,
            page,
        } = this.props;
        return (
            <React.Fragment>
                <br />
                <h4 className="anual_goals_title">
                    METAS ANUALES
                </h4>
                <div className="uk-card uk-padding-small uk-padding uk-margin-auto">
                <div className="list-button-annual uk-flex uk-flex-between uk-padding uk-padding-remove-bottom uk-margin-auto-top@s">
                        <Link
                            className="uk-button
                            uk-button-primary
                            uk-border-rounded
                            uk-button-small
                            uk-flex
                            meta-annual-button"
                            
                            to="/annual-goals/create"
                        >
                            Crear Tipo

                            {/* <i style={{marginLeft: "2px"}} className="material-icons">add_circle_outline</i> */}
                        </Link>
                        <input
                            type="text"
                            className="uk-input uk-border-rounded uk-width-1-5 annual-goal-buscar"
                            // onChange={(e) => searchChange(e.target.value)}
                            placeholder="Buscar..."
                        />
                    </div>

                    <div className="uk-card uk-padding">
                        <Grid
                            data={data}
                            loading={loader}
                            onPageChange={listar}
                            onSortChange={onSortChange}
                            page={page}
                            striped
                            hover
                        >
                            <TableHeaderColumn
                                isKey
                                dataField="id"
                                dataAlign="center"
                                dataSort
                                dataFormat={standardActions({
                                    editar: 'annual-goals',
                                    ver: 'annual-goals',
                                    eliminar,
                                })}
                            >
                                
                            </TableHeaderColumn>

                            <TableHeaderColumn
                                dataField="annual_goal"
                                dataSort
                                className="fuente_header_table"
                            >
                                Año
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="goal_quetzales"
                                dataSort
                                className="fuente_header_table"
                                dataFormat={cell => `Q. ${cell.toFixed(2)}`
                                }
                            >
                                Meta en quetzales
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField="is_active_goal"
                                dataSort
                                className="fuente_header_table"
                                dataFormat={cell => (cell ? 'Activa' : 'No Activa')
                                }
                            >
                                Estado de meta
                            </TableHeaderColumn>

                        </Grid>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
