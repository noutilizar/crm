import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {
    renderField,
    renderCurrency,
    renderSwitch,
} from '../Utils/renderField';
import './style.css';
import { Link } from 'react-router-dom';
import { required } from '../../../utility/validation';
import '../../../../style/fuentes.css';

const validetYear = (value) => {
    const today = new Date();
    const year = today.getFullYear();
    if (!value) {
        return "Requerido";
    }
    if (String(value).length > 4) {
        return "El ingresado año invalida";
    }
    if (parseInt(value, 10) < year) {
        return "No puede ingresar años antiguos";
    }
};
const parseNumber = (value) => {
    if (value) {
        return Number(value);
    }
    return '';
};
const FormStepOne = (props) => {
    const { handleSubmit, onChangeField, ver } = props;
    // function Cambio_label(texto){
    //     if (texto=="Desactivar la ultima meta activa"){
    //         return "Activar la ultima meta activa"
    //     }
    //     else{
    //         return "Desactivar la ultima meta activa"
    //     }
    // }
    return (
        <form
            onSubmit={handleSubmit}
            className="uk-card  uk-padding uk-margin-auto annualgoals-form "
        >
            {/* <div className="uk-grid uk-flex-between uk-child-width-expand"> */}
            <div className="uk-margin-bottom">
                <div className="annual-goals-divcol annual-goals-div-dereha">
                    <div className="uk-card uk-card-primary">
                        <label className="anual_info_general">
                            Información General
                        </label>
                    </div>
                    
                    <div className="uk-margin-bottom uk-margin-top">
                        <label htmlFor="annual_goal" className="anual_goals_anio">
                            Año de la meta
                        </label>
                        <Field
                            name="annual_goal"
                            className="uk-input uk-border-rounded uk-form-width-medium annualgoal-form-input1"
                            component={renderField}
                            validate={validetYear}
                            type="number"
                            disabled={ver}
                        />
                    </div>
                    <div className="uk-margin-bottom">
                        <label htmlFor="goal_quetzales" className="anual_goals_metanual">Meta anual</label>
                        <Field
                            name="goal_quetzales"
                            className="uk-input uk-border-rounded uk-form-width-medium annualgoal-form-input1"
                            parse={parseNumber}
                            component={renderCurrency}
                            validate={required}
                            onChange={e => console.log(e)}
                            type="number"
                            disabled={ver}
                        />
                    </div>
                    <fieldset className="uk-margin-bottom uk-fieldset">
                        <Field
                            name="is_active_goal"
                            component={renderSwitch}
                            label="Desactivar la ultima meta activa"
                            disabled={ver}
                            // onclick= {Cambio_label(label)}
                        />
                    </fieldset>
                </div>
                <div className="annual-goals-divcol annual-goals-div-izquierda">
                    <div className="uk-card uk-card-primary ">
                        <label className="anual_metasportrimestre">
                            Metas por Trimestre
                        </label>
                    </div>
                    <label className="anual_goals_primer_trimestre uk-margin-top">
                        Primer Trimestre
                    </label>
                    {/* <hr className="uk-margin-small" /> */}
                    <div className="uk-grid uk-flex-between uk-margin-bottom">
                    <div className="uk-width-1-2">
                            <label htmlFor="goal_first_trimester" className="anual_goals_primer_porcentaje">
                                Porcentaje
                            </label>
                            <Field
                                name="goal_first_trimester"
                                type="number"
                                parse={parseNumber}
                                className="uk-input uk-border-rounded uk-form-width-medium annualgoal-form-input"
                                validate={required}
                                component={renderField}
                                onChange={(e) => { onChangeField(e.target); }}
                                disabled={ver}
                            />
                        </div>
                        <div className="uk-width-1-2">
                            <label htmlFor="goal_first_quetzal" className="anual_goals_primer_equivalente">
                                Equivalente en Q.
                            </label>
                            <Field
                                name="goal_first_trimester_quetzal"
                                className="uk-input uk-border-rounded uk-form-width-medium annualgoal-form-input"
                                component={renderCurrency}
                                disabled
                            />
                        </div>
                    </div>
                    <label className="anual_goals_segundo_trimestre">
                        Segundo Trimestre
                    </label>
                    {/* <hr className="uk-margin-small" /> */}
                    <div className="uk-grid uk-flex-between uk-margin-bottom">
                    <div className="uk-width-1-2">
                            <label htmlFor="goal_second_trimester" className="anual_goals_segundo_porcentaje">
                                Porcentaje
                            </label>
                            <Field
                                name="goal_second_trimester"
                                type="number"
                                parse={parseNumber}
                                className="uk-input uk-border-rounded uk-form-width-medium annualgoal-form-input"
                                validate={required}
                                component={renderField}
                                onChange={(e) => { onChangeField(e.target); }}
                                disabled={ver}
                            />
                        </div>
                        <div className="uk-width-1-2">
                            <label htmlFor="goal_second_quezal" className="anual_goals_segundo_equivalente">
                            Equivalente en Q.
                            </label>
                            <Field
                                name="goal_second_trimester_quetzal"
                                className="uk-input uk-border-rounded uk-form-width-medium annualgoal-form-input"
                                component={renderCurrency}
                                disabled
                            />
                        </div>
                    </div>
                    <label className="anual_goals_tercer_trimestre">
                        Tercer Trimestre
                    </label>
                    {/* <hr className="uk-margin-small" /> */}
                    <div className="uk-grid uk-flex-between uk-margin-bottom">
                    <div className="uk-width-1-2">
                            <label htmlFor="goal_third_trimester" className="anual_goals_tercer_porcentaje">
                                Porcentaje
                            </label>
                            <Field
                                name="goal_third_trimester"
                                type="number"
                                parse={parseNumber}
                                className="uk-input uk-border-rounded uk-form-width-medium annualgoal-form-input"
                                validate={required}
                                component={renderField}
                                onChange={(e) => { onChangeField(e.target); }}
                                disabled={ver}
                            />
                        </div>
                        <div className="uk-width-1-2">
                            <label htmlFor="goal_third_quezal" className="anual_goals_tercer_equivalente">
                            Equivalente en Q.
                            </label>
                            <Field
                                name="goal_third_trimester_quetzal"
                                className="uk-input uk-border-rounded uk-form-width-medium annualgoal-form-input"
                                component={renderCurrency}
                                disabled
                            />
                        </div>
                    </div>
                    <label className="anual_goals_tercer_trimestre">
                        Cuarto Trimestre
                    </label>
                    {/* <hr className="uk-margin-small" /> */}
                    <div className="uk-grid uk-flex-between uk-margin-bottom">
                        <div className="uk-width-1-2">
                            <label htmlFor="goal_third_trimester" className="anual_goals_tercer_porcentaje">
                                Porcentaje
                            </label>
                            
                            <Field
                                name="goal_fourth_trimester"
                                type="number"
                                className="uk-input uk-border-rounded uk-form-width-medium annualgoal-form-input"
                                parse={parseNumber}
                                validate={required}
                                component={renderField}
                                onChange={(e) => { onChangeField(e.target); }}
                                disabled={ver}
                            />
                        </div>
                        <div className=" uk-width-1-2">
                        <label htmlFor="goal_third_quezal" className="anual_goals_tercer_equivalente">
                            Equivalente en Q.
                            </label>
                            <Field
                                name="goal_fourth_trimester_quetzal"
                                className="uk-input uk-border-rounded uk-form-width-medium annualgoal-form-input"
                                component={renderCurrency}
                                disabled
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="annual-form-button uk-flex uk-flex-between">
                <Link className="uk-button uk-button-secondary uk-border-rounded uk-button-small uk-flex" to="/annual-goals">
                    Cancelar
                    {/* <i style={{marginLeft: "2px"}} className="material-icons">cancel</i> */}
                </Link>
                <button
                    className="uk-button uk-button-primary uk-border-rounded uk-button-small uk-flex"
                    type="submit"
                >
                    Siguiente
                    {/* <i style={{marginLeft: "2px"}} className="material-icons">arrow_forward</i> */}
                </button>

            </div>
        </form>
    );
};

export default reduxForm({
    form: 'annualGoalsForm',
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
})(FormStepOne);
