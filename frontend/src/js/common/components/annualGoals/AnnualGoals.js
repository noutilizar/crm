import React, { Component } from "react";
import Stepper from "react-stepper-horizontal";
import FormStepOne from "./FormStepOne";
import FormStepTwo from "./FormStepTwo";
import "../../../../style/fuentes.css";
import './annualgoalstyle.css'
class Form extends Component {
    componentWillMount = () => {
        const { getBusinessLines, match, detalle } = this.props;
        if (match.params.id) {
            const { id } = match.params;
            detalle(id);
        } else {
            getBusinessLines();
        }
    };

    render() {
        const {
            step,
            nextPage,
            stepsTitle,
            previousPage,
            onChangeField,
            onChangeFieldArray,
            businessLines,
            location,
            crear,
            actualizar,
            match,
        } = this.props;
        const onSubmit = match.params.id ? actualizar : crear;
        return (
            <div>
               
                <br />
                <h4 className="anual_goals_nueva">NUEVA META ANUAL</h4>
                <div className="annualgoals-div">
                <Stepper steps={stepsTitle} activeStep={step}  activeColor={"#F4991A"}
                 defaultColor={"#FFFFFF"}  circleFontColor={"black"} completeColor={"#F4991A"} 
                  defaultBarColor={"#F4991A"} barStyle={"solid"} titleFontSize={"22"}
                  defaultTitleOpacity={"1"} completeBarColor={"#F4991A"}/>
                    {step === 0 && (
                        <FormStepOne
                            onChangeField={onChangeField}
                            onSubmit={nextPage}
                            handleSubmit={nextPage}
                            ver={location.pathname.includes("ver")}
                        />
                    )}
                    {step === 1 && (
                        <FormStepTwo
                            businessLines={businessLines}
                            previousPage={previousPage}
                            onSubmit={onSubmit}
                            actualizar={!!match.params.id}
                            onChangeFieldArray={onChangeFieldArray}
                            ver={location.pathname.includes("ver")}
                        />
                    )}
                </div>
            </div>
        );
    }
}

export default Form;
