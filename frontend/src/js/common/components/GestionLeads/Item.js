import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import Swal from 'sweetalert2';
/*iconos */
import file from '../../.././../assets/img/file.svg';
import edit from '../../.././../assets/img/edit.svg';
import trash from '../../.././../assets/img/delete.svg';
import check from '../../.././../assets/img/check-box-with-check-sign.svg';

import './style.css';

const getItemStyle = (isDragging, draggableStyle) => ({
    userSelect: "none",
    background: isDragging ? "rgb(8, 112, 216)" : "rgb(30, 135, 240)",
    ...draggableStyle,
});


const Item = (props) => {
    const { lead, index, detalleLead, eliminarLead, openModal } = props;

    const eliminar = (id) => {

        Swal.fire({
            title: '¿Eliminar?',
            text: '¡No podrá revertir esta acción!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: '¡Sí, eliminar!',
            cancelButtonText: 'No, cancelar',
            reverseButtons: true,
        }).then((result) => {
            if (result.value) {
                eliminarLead(id)
            }
        });
    };

    return (
        <Draggable draggableId={`${lead.id}`} index={index}>
            {
                (provided, snapshot) => (
                    <div
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        ref={provided.innerRef}
                        style={getItemStyle(
                            snapshot.isDragging,
                            provided.draggableProps.style
                        )}
                        className="container__item"
                    >

                        {lead.possibility_won == true
                            &&

                            <p>
                                {/*<i
                                    className="material-icons"
                                >
                                    
                                </i>*/} 
                                <img className="style-icons" src={check}></img>
                            </p>
                        }


                        <h3>
                            {lead.name}
                        </h3>
                        <p>Cierre {lead.closing_date}</p>

                      

                        <div className="uk-align-right">
                       
                            <a
                                onClick={() => {
                                    eliminar(lead.id)
                                }}
                            >
                                {/*<i className="material-icons" style={{ color: '#fa8080', marginRight: '5px' }}>delete</i>*/}
                                <img className="style-icons" src={trash}></img>
                            </a>

                            <a
                                onClick={() => {
                                    detalleLead(lead.id, false)
                                    openModal()
                                }}
                            >
                                {/*<i className="material-icons text-warning" style={{ color: 'white' }}>edit</i>*/}
                                <img className="style-icons" src={edit}></img>
                            </a>

                            <a
                                onClick={() => {
                                    detalleLead(lead.id, true)
                                    openModal()
                                }}
                            >
                                {/*<i className="material-icons" src={file} style={{ color: 'red' }}></i>*/}
                                <img className="style-icons" src={file}></img>
                            </a>


                        </div>

                        <p>
                            {}
                            {' '}
                            <br />
                            <br />
                            {' '}
                            {}
                        </p>

                    </div>

                )
            }

        </Draggable>

    );
};

export default Item;
