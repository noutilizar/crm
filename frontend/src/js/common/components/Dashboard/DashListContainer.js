import { connect } from 'react-redux';
import { actions } from '../../../redux/modules/dashboard/dashboard';
import DashList from './DashList';


const ms2p = (state) => {
    return {
        ...state.dashboard,
    };
};

const md2p = { ...actions };

export default connect(ms2p, md2p)(DashList);