import React, { Component } from "react";
import "../../../../style/fuentes.css";
import Grid from "../Utils/Grid";
import { standardActions } from "../Utils/Grid/StandardActions";
export default class AdministrativeModuleList extends Component {
    componentWillMount = () => {
        const { listar } = this.props;
        console.log(this.props.data);
        listar();
    };

    render() {
        const {
            data,
            loader,
            eliminar,
            listar,
            page,
        } = this.props;
        return (
            <React.Fragment>
                <br />
                <h4 className="contacts_title ">MODULOS ADMINISTRATIVOS </h4>

                <div className="uk-card  uk-margin-auto contact-divprincipal ">
                    <div className="list-button-budget uk-flex uk-flex-between uk-padding uk-padding-remove-bottom uk-margin-auto-top@s ">
                        <button className="uk-button uk-button-primary uk-border-rounded uk-margin-small-bottom uk-button-small uk-flex link-contact">
                            <a
                                className="uk-align-center"
                                href="/#/administrative-module/create"
                            >
                                
                                Crear Modulo
                            </a>
                        </button>
                        <input
                            className="uk-input uk-border-rounded uk-width-1-5"
                            placeholder="Buscar"
                            onChange={this.handleSearch}
                        />
                    </div>
                </div>
                
                <Grid
                    className="uk-padding "
                    data={data}
                    loading={loader}
                    onPageChange={listar}
                    page={page}
                    striped
                    hover
                >
                    <TableHeaderColumn
                        dataField="id"
                        dataAlign="center"
                        dataSort
                        dataFormat={standardActions({
                            editar: "administrative-module",
                            ver: "administrative-module",
                            eliminar,
                        })}
                    ></TableHeaderColumn>
                    
                    <TableHeaderColumn
                        isKey
                        dataField="id"
                        dataSort
                        className="fuente_header_table"
                    >
                       #Modulo
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        dataFormat={(cell) =>{
                            if(cell){
                            return cell.name_project
                            }
                            
                        }}
                        dataField="type_project"
                        dataSort
                        className="fuente_header_table"
                    >
                      Tipo Proyecto
                    </TableHeaderColumn>

                </Grid>

            </React.Fragment>
        );
    }
}


