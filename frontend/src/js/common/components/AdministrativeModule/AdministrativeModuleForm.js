import React, { Component } from 'react'
import CreateForm from './ModuleForm';
import '../../../../style/fuentes.css';
import "./adminmodule.css";

export default class AdministrativeModuleForm extends Component {

    componentWillMount = () => {

        const { match, detalle } = this.props;
        if (match.params.id) {
            const id = match.params.id;
            detalle(id);
        }
    }

    render() {
        const {
            getTipoProyecto,
            onSubmit,
            actualizar,
            match,
            location,
        } = this.props;
        const fn = match.params.id ? actualizar : onSubmit;
        const isActualizar = (match.params.id) ? true : false
        return (
            <div>
                <br />
                <h4 className="industrias_nueva_title ">
            {
                (isActualizar)
                    ? 'MODULO ADMINISTRATIVO'
                    : 'NUEVO MODULO ADMINISTRATIVO'
            }
        </h4>
                <div>

                </div>
                <div className="adminmodule-div">
                <CreateForm
                getTipoProyecto = {getTipoProyecto}
                onSubmit={fn}
                actualizar={!!match.params.id}
                ver={location.pathname.includes("ver") && true}
                
                />
            </div>
            </div>
        )
    }
}
