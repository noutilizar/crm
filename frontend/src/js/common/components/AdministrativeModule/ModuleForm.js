import React from "react";
import { Field, reduxForm, FieldArray } from "redux-form";
import { validate, validators } from "validate-redux-form";
import {
    renderField,
    AsyncSelectField,
} from "../Utils/renderField/renderField";
import "./adminmodule.css";
import svgDelete from '../../../../assets/img/delete.svg';

const CreateForm = (props) => {
    const {
        handleSubmit,
        ver,
        actualizar,
        getTipoProyecto,
        pristine,
        reset,
        submitting,
    } = props;

    const renderModule = ({ fields, meta: { error, submitFailed } }) => (
        <React.Fragment>
        <div>
                {submitFailed && error && <span>{error}</span>}       
        </div>
        <tbody>    
            
            {fields.map((modules, index) => (
                <tr key={index}>
                  {ver ? <div></div> :  <a className="px-2" style={{cursor: "pointer"}}>
                    <img className="icon_svg-table" src={svgDelete} style={{cursor: "pointer"}} onClick={() => fields.remove(index)}/>
                </a> }
                        
                            <td>
                                {/* <label className="form-adminmodule-module">
                                    NOMBRE 
                                </label> */}
                            <Field
                                name={`${modules}.name_expense`}
                                type="text"
                                component={renderField}
                                label="name_expense"
                                disabled={ver}
                            />
                            </td>
                        
                            <td>
                            <Field
                                name={`${modules}.number_hours`}
                                type="text"
                                component={renderField}
                                label="number_hours"
                                disabled={ver}
                            />
                            </td>
                            <td>
                            <Field
                                name={`${modules}.total_quantity`}
                                type="text"
                                component={renderField}
                                label="total_quantity"
                                disabled={ver}
                            />
                            </td>
                            <td>
                            <Field
                                name={`${modules}.rate_hour`}
                                type="text"
                                component={renderField}
                                label="rate_hour"
                                disabled={ver}
                            />
                            </td>
                            <td>
                            <Field
                                name={`${modules}.total_hours`}
                                type="text"
                                component={renderField}
                                label="total_hours"
                                disabled={ver}
                            />
                            </td>

                        {/* <FieldArray
                        name={`${member}.hobbies`}
                        component={renderHobbies}
                    /> */}
                    
                </tr>
            ))}
            <tr>
                <td colSpan={3}>
                {ver ? <div></div> :
                <div>
                    <button
                        className="uk-button uk-button-secondary uk-button-small uk-border-rounded uk-margin-left"
                        type="button"
                        disabled={pristine || submitting}
                        onClick={reset}
                    >
                        Limpiar todo
                    </button>
                </div> }
                </td>
                <td colSpan={1}></td>
                <td colSpan={3}>
                    {ver ? <div></div> :
                        <button
                            className="uk-button uk-button-primary uk-button-small uk-border-rounded uk-margin-left"
                            type="button"
                            onClick={() => fields.push({})}
                        >
                            Agregar modulo
                    </button>}
                </td>

            </tr>
        </tbody>
        </React.Fragment>
    );

    return (
        <form onSubmit={handleSubmit}>
            <fieldset className="uk-fieldset">
                <div className="uk-margin adminmodule-form">
                    <label className="px-4 uk-align-center form-adminmodule-label">
                        TIPO PROYECTO
                    </label>
                    <Field
                        name="type_project"
                        component={AsyncSelectField}
                        disabled={ver}
                        className="adminmodule-form-select "
                        loadOptions={getTipoProyecto}
                        type="text"
                    />
                </div>
                <br></br>
                <table className="table table-striped table-bordered grid-table-head">
                    <thead>
                        <tr>            
                            <th>
                                    
                            </th>
                            <th className="text-primary">NOMBRE</th>
                            <th className="text-primary">CANT. HORAS</th>
                            <th className="text-primary">CANTIDAD</th>
                            <th className="text-primary">TARIFA HORA</th>
                            <th className="text-primary">TOTAL</th>

                        </tr>
                    </thead>
                    <FieldArray name="modules" component={renderModule} />
                </table>
                                    

                <div className=" form-adminmodule-buttons">
                    <div className="button-container uk-display-block uk-float-left button-cancelar">
                        <a
                            className="uk-button uk-button-secondary uk-button-small uk-border-rounded "
                            type="button"
                            href="#/administrative-module"
                        >
                            Cancelar
                        </a>
                    </div>

                    <div className=" button-container uk-display-block uk-float-left  button-guardar">
                        {!ver && (
                            <button className="uk-button uk-button-primary uk-button-small uk-border-rounded uk-margin-left ">
                                {actualizar ? "Actualizar" : "Guardar"}
                            </button>
                        )}
                    </div>

                </div>
            </fieldset>
        </form>
    );
};

export default reduxForm({
    form: "createAdminModule", // a unique identifier for this form
})(CreateForm);
