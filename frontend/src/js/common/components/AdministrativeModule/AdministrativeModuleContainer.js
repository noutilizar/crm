import {connect} from "react-redux";
import {actions} from '../../../redux/modules/administrativeModule/administrativeModule';
import AdministrativeModuleForm from './AdministrativeModuleForm';
import AdministrativeModuleList from './AdministrativeModuleList';

const ms2p = (state) => {
    return {
        ...state.administrativeModule,
    };
};
const md2p = {...actions};

//= =================
// Conection List Annuals Goals
//= =================
const administrativeModuleList = connect(ms2p, md2p)(AdministrativeModuleList);

//= =========================
// Conection Create Annual Goal
//= ========================?
const administrativeModuleForm = connect(ms2p, md2p)(AdministrativeModuleForm);


export const ConnectionAdministrativeModule = {
    administrativeModuleList,
    administrativeModuleForm,
};
