import { handleActions } from 'redux-actions';
import { push } from "react-router-redux";
import { initialize as initializeForm } from 'redux-form';
import { NotificationManager } from "react-notifications";
import { api } from "api";
import BudgetModulesForm from '../../../common/components/BudgetModules/BudgetModulesForm';

const SET_DATA = 'SET_DATA';
const SET_LOADER = 'SET_LOADER';
const SET_REGISTER = 'SET_REGISTER';
const SET_PAGE = 'SET_PAGE';
const SET_PERMISSION='SET_PERMISSION';
const SET_LISTPERMISSION = 'SET_LISTPERMISSION';
const SET_PERMISOS_ROL='SET_PERMISOS_ROL';

export const setLoader = loader => ({
    type: SET_LOADER,
    loader,
});

const set_permission=(id) =>({
    type: SET_PERMISSION,
    id
})

const set_listPermission =( listpermission ) =>({
    type: SET_LISTPERMISSION,
    listpermission
})

const asignar_permiso=(id) => (dispatch) =>{

    dispatch(set_permission(id));
    
}

const listar = ( page = 1) => (dispatch) => {

    dispatch({type: SET_LOADER, loader: true});
    const params = { page }
    api.get('budget_modules', params).then((response)=>{
        console.log('response', response)
        
        dispatch({type: SET_DATA, data: response});
        dispatch({type: SET_PAGE, page: page});
    }).catch(() => {
    }).finally(()=>{
        dispatch({type: SET_LOADER, loader: false});
    });
};

// export const editar = (id) => (dispatch, getStore) => {
//     dispatch({type: SET_LOADER, loader: true});
//     api.get(`user/${id}`).then((response)=>{
       
//         dispatch(initializeForm('BudgetModulesForm', response));
//         dispatch({type: SET_REGISTER, register: response});
//     }).catch((error) => {
//         NotificationManager.error(error.detail, 'ERROR', 0);
//     }).finally(()=>{
//         dispatch({type: SET_LOADER, loader: false});
//     })
// }
const registrar = () => (dispatch, getStore) => {
    const formData = getStore().form.budgetmodulesForm.values;
    console.log('formData', formData)
    api.post('budget_modules', formData).then((response) => {
        console.log('resp', response)
        NotificationManager.success('Modulo registrado correctamente', 'Éxito', 1000);
        dispatch(push("/budget-modules"));
    }).catch((error) => {
        console.log(error);
        NotificationManager.error('Ocurrio un error al registrar el modulo', 'ERROR', 0);
    }).finally(() => {
        dispatch({type: SET_LOADER, loader: false});
    });


}

const actualizar = () => (dispatch, getStore) => {
    const estado = getStore();
    const formData = estado.form.budgetmodulesForm.values;

    api.put(`budget_modules/${formData.id}`, formData).then((response) => {
        NotificationManager.success('Modulo actualizado correctamente', 'Éxito', 1000);
        dispatch(push("/budget-modules"));
    }).catch((error) => {
        NotificationManager.error(error.detail, 'ERROR', 0);
    }).finally(() => {

    });
}

const eliminar = (id) => (dispatch, getStore) => {
    api.eliminar(`budget_modules/${id}`).then((response) => {
      
        NotificationManager.success('Modulo eliminado correctamente', 'Éxito', 1000);
        dispatch(listar());
    }).catch((error) => {
        NotificationManager.error(error.detail, 'ERROR', 0);
    }).finally(() => {

    });
}


const filterBudgetModules = search => (dispatch) => {
    dispatch({type: SET_LOADER, loader: true});

    api.get(`budget_modules/search/${search}`).then((response) => {
        dispatch(setData(response));
    }).catch((error) => {
        NotificationManager.error(error.detail, 'ERROR', 0);
    }).finally(() => {
        dispatch({type: SET_LOADER, loader: false});
    });
};

const getPermissions = (search='') => (dispatch, getStore) => {
    let permissions=[];
    
    api.get('permission').then((response)=>{    
        console.log(response.results)   
        dispatch( set_listPermission( response.results ) )

        response.results.forEach(item =>{
            permissions.push({name: item.name, id: item.id})
        })


    }).catch(()=>{
        return [];
    })

}
const handleChange = id =>(dispatch, getStore)=>{
    
    const { listpermission } = getStore().users
    const permisos = Array.from(listpermission) 
    permisos.forEach(item=>{
        const { values } = getStore().form.budgetmodulesForm
        const newState={
            ...values,
            [item.name]:false
        }
        dispatch(initializeForm("BudgetModulesForm", newState))
    })
    api.get(`role/${id.value}` ).then((response)=>{      
        if(response.name!="Personalizado"){             
            response.permissions.forEach(item =>{
               
               // getStore().form.BudgetModulesForm.values[item.name]=true
                const { values } = getStore().form.budgetmodulesForm
                const newState={
                    ...values,
                    [item.name]:true
                }
                dispatch(initializeForm("BudgetModulesForm", newState))
                dispatch({type: SET_PERMISOS_ROL, permisos_rol: true})
        }  
            )         
        }
        else
        {
            dispatch({type: SET_PERMISOS_ROL, permisos_rol: false})
        }
    }).catch(()=>{
    })
};

const detalle = id => (dispatch) => {
    dispatch(setLoader(true));

    api.get(`budget_modules/${id}`).then((response) => {
        dispatch(initializeForm('budgetmodulesForm', response));
    }).catch((error) => {
        NotificationManager.error(error.detail, 'ERROR', 0);
    }).finally(() => {
        dispatch(setLoader(false));
    });
};


/*const handleChange = id =>(dispatch, getStore)=>{
    let permissions=[]
    let permissions3=[]
    //const disabled
    api.get(`role/${id.value}` ).then((response)=>{      
        if(response.name!="Personalizado"){       
            response.permissions.forEach(item =>{
               permissions.push({name: item.name, value: item.id})
               permissions3.push({name: item.name, id: item.id, [item.name]:true})

               const { values } = getStore().form.BudgetModulesForm
                const newState={
                    ...values,
                        [item.name]:true               
                }
                console.log("permiso",response.permissions)
                dispatch(initializeForm("BudgetModulesForm", newState))
                dispatch({type: SET_PERMISOS_ROL, permisos_rol: true})
        }  
     
            ) 
            const { values } = getStore().form.BudgetModulesForm
            
            const newState={
                ...values,
                    permissions2:permissions3               
            }
            
            dispatch(initializeForm("BudgetModulesForm", newState))    
        }
        else{
            dispatch({type: SET_PERMISOS_ROL, permisos_rol: false})
            console.log("Es personalizado")  
        }
        response.permissions3=permissions3
       
        dispatch(initializeForm("BudgetModulesForm", newState))
    }).catch(()=>{
    })
    const { listpermission } = getStore().users
    const permisos = Array.from(listpermission) 
   
    permisos.forEach(item=>{
        const { values } = getStore().form.BudgetModulesForm
        const newState={
            ...values,
                [item.name]:false,
                
        }
        dispatch(initializeForm("BudgetModulesForm", newState))
    })
    
};*/



export const actions = {
    registrar,
    detalle,
    actualizar,
    eliminar,
    listar,
    // editar,
    filterBudgetModules,
    set_permission,
    getPermissions,
    handleChange,
    asignar_permiso,
};

export const reducers = {
    [SET_DATA]: (state, { data }) => {
        return {
            ...state,
            data,
        };
    },
    [SET_PERMISSION]: (state, { id }) => {
        return {
            ...state,
            permissions:[...state.permissions, id],
        };
    },
    [SET_PERMISOS_ROL]: (state, { permisos_rol }) => {
        return {
            ...state,
            permisos_rol,
        };
    },
    
    [SET_LOADER]: (state, { loader }) => {
        return {
            ...state,
            loader,
        };
    },

    [SET_REGISTER]: (state, { register }) => {
        return {
            ...state,
            register,
        };
    },

    [SET_PAGE]: (state, { page }) => {
        return {
            ...state,
            page,
        };
    },
    [SET_LISTPERMISSION]: (state, { listpermission }) => {
        return {
            ...state,
            listpermission,
        };
    },
};

export const initialState = {
    loader: false,
    data: {},
    register: null,
    page: 1,
    permissions:[],
    listpermission:[],
    permisos_rol:true,
};

export default handleActions(reducers, initialState);
