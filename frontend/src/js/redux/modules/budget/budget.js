/* eslint-disable no-param-reassign */
import { push } from 'react-router-redux';
import { initialize as initializeForm, change } from 'redux-form';
import { api } from 'api';
import { NotificationManager } from 'react-notifications';
import { handleActions } from 'redux-actions';

const LOADER = 'BUDGET_LOADER';
const DATA = 'BUDGET_DATA';
const ITEM = 'BUDGET_ITEM';
const PAGE = 'BUDGET_PAGE';
const ORDERING = 'BUDGET_ORDERING';
const SEARCH = 'BUDGET_SEARCH';
const STEP = 'BUDGETL_STEP';
const SETCOMMISSION_PERCENTAGE = 'SETCOMMISSION_PERCENTAGE';
const SETVENDEDOR_PERCENTAGE = 'SETVENDEDOR_PERCENTAGE';
const SPRINT = 'SPRINT';
const DESIGNERS = 'DESIGNERS';
const PROGRAMMERS ='PROGRAMMERS';
const TOTAL_HISTORYPOINTS = 'TOTAL_HISTORYPOINTS';
const HISTORY_POINTSERROR = 'HISTORY_POINTSERROR';
// const HOUR_FORWEEK='HOUR_FORWEEK' ;
// const  WEEK_FORMONTH='WEEK_FORMONTH' ;
// const =;
// const =;

//  total_history_points--- total puntos historia
//  Puntos de historia margen de erro---history_points_error 
// configuración de horas semanales---hour_for_week
// configuración de semanas del mes---week_for_month
// cantidad diseñadores 
// cantidad programadores

// -----------------------------------
// Pure Actions
// -----------------------------------
const setLoader = loader => ({
    type: LOADER,
    loader,
});

const setData = data => ({
    type: DATA,
    data,
});
const setItem = item => ({
    type: ITEM,
    item,
});
const setPage = page => ({
    type: PAGE,
    page,
});

const setOrdering = ordering => ({
    type: ORDERING,
    ordering,
});
const setSearch = search => ({
    type: SEARCH,
    search,
});
const setStep = step => ({
    type: STEP,
    step,
});

const setCommission_percentage = commission_percentage => ({
    type: SETCOMMISSION_PERCENTAGE,
    commission_percentage,
});
const setVendedor_percentage = vendedor_percentage => ({
    type: SETVENDEDOR_PERCENTAGE,
    vendedor_percentage,
});
const setSprint= sprint => ({
    type: SPRINT,
    sprint,
});
const setDesigner= designer => ({
    type: DESIGNERS,
    designer,
});
const setProgrammer= programmer => ({
    type: PROGRAMMERS,
    programmer,
});
const setTotal_HistoryPoints= total_historypoints => ({
    type: TOTAL_HISTORYPOINTS,
    total_historypoints,
});
const setTotal_HistoryPoints_Error= total_historypoints_error => ({
    type: HISTORY_POINTSERROR,
    total_historypoints_error,
});
// -----------------------------------
// Actions
// -----------------------------------



const detalle = (id) => (dispatch) => {
    dispatch(setLoader(true));

    api.get(`budget/${id}`)
        .then((response) => {
            console.log(response, "---------------")
            if (response.tipo_proyecto) {
                const tipo_proyecto1 = {
                    value: response.tipo_proyecto.id,
                    label: response.tipo_proyecto.name_project,
                };
                response.tipo_proyecto = tipo_proyecto1;
            }
            if (response.tipo_cliente) {
                const tipo_cliente = {
                    value: response.tipo_cliente.id,
                    label: response.tipo_cliente.name_type,
                };
                response.tipo_cliente = tipo_cliente;
            }
            if (response.canal_venta) {
                const canal_venta = {
                    value: response.canal_venta.id,
                    label: response.canal_venta.name,
                };
                response.canal_venta = canal_venta;
            }
            if (response.vendedor) {
                const vendedor = {
                    value: response.vendedor.id,
                    label: response.vendedor.nombre,
                };
                response.vendedor = vendedor;
            }

            const modules = {modules: response.budget_relation_modules, adminmodules: response.adminmodules}
            // const adminmodules = {modules: response.adminmodules} 
            dispatch(initializeForm("budgetForm2", {budget: response, modules: modules
                // , adminmodules: adminmodules
            }));
        })
        .catch((error) => {
            NotificationManager.error(error.detail, "ERROR", 0);
        })
        .finally(() => {
            dispatch(setLoader(false));
        });
};

const listar = (page = 1) => (dispatch) => {
    dispatch(setLoader(true));
    const params = { page };

    api.get('budget', params).then((response) => {
        dispatch(setData(response));
        dispatch(setPage(page));
    }).catch(() => {
    }).finally(() => {
        dispatch(setLoader(false));
    });
};

const formatData = ( values ) => {

    return {
        cantidad_programadores: values.cantidad_programadores,
        porcentaje_error: values.porcentaje_error,
        precio_nominal: values.precio_nominal,
        canal_venta: values.canal_venta.value,
        vendedor: values.vendedor.value,
        tipo_cliente: values.tipo_cliente.value,
        tipo_proyecto: values.tipo_proyecto.value,
        // lead : values.lead.value,
        cantidad_diseñadores: values.cantidad_diseñadores
    }

}
const onSubmit = () => (dispatch, getStore) => {
    let dataForm = getStore().form.budgetForm2.values;
    const modules = dataForm.modules.modules;
    const adminmodules = dataForm.modules.adminmodules;
    dataForm.modules = modules ? modules : [];
    dataForm.adminmodules = adminmodules ? adminmodules : [];
    api.post('budget', dataForm).then((response) => {
        NotificationManager.success('Presupuesto registrado correctamente', 'Éxito', 1000);
        dispatch(push('/presupuestos'));
    }).catch(() => {
        NotificationManager.error('Error de creacion', 'ERROR', 3000);
   
    });
};

const actualizar = () => (dispatch, getStore) => {
    const { values } = getStore().form.budgetForm2;
    let dataForm = getStore().form.budgetForm2.values;
    const modules = dataForm.modules.modules;
    const adminmodules = dataForm.modules.adminmodules;
    dataForm.modules = modules ? modules : [];
    dataForm.adminmodules = adminmodules ? adminmodules : [];
 
    console.log("aaaaaaaaaaaaaaaaaaaaaa", dataForm)

    api.put(`budget/${dataForm.budget.id}`, dataForm)
        .then(() => {
            NotificationManager.success(
                "Los modulos se actualizaron correctamente",
                "Éxito",
                1000
            );
            dispatch(push('/presupuestos'));
        })
        .catch(() => {
            NotificationManager.error(
                "Hubo error en la actualización",
                "ERROR",
                0
            );
        });
};

const searchChange = search => (dispatch) => {
    dispatch(setSearch(search));
    dispatch(listar());
};

const SetCommissionPercentage = commission_percentage =>(dispatch, getStore) => {
    dispatch(setCommission_percentage(commission_percentage));
};
const SetVendedorPercentage = vendedor_percentage =>(dispatch, getStore) => {
    dispatch(setVendedor_percentage(vendedor_percentage));
};
const SetSprint = sprint => (dispatch) => {
    dispatch(setSprint(sprint));
};
const SetDesigner = designer=> (dispatch) => {
    dispatch(setDesigner(designer));
};
const SetProgrammer = programmer => (dispatch) => {
    dispatch(setProgrammer(programmer));  
};
const SetTotal_HistoryPoints = total_historypoints => (dispatch) => {
    dispatch(setTotal_HistoryPoints(total_historypoints));
    
};
const SetTotal_HistoryPoints_Error= total_historypoints_error => (dispatch) => {
    dispatch(setTotal_HistoryPoints_Error(total_historypoints_error));
    
};

const setAceptado = k => (dispatch, getStore) => {
    dispatch(change(`budgetForm2`, `modules[${k}].aceptado`, true))
    
} 


export const actions = {
    listar,
    searchChange,
    onSubmit,
    SetCommissionPercentage,
    SetVendedorPercentage,
    SetSprint,
    SetDesigner,
    SetProgrammer,
    SetTotal_HistoryPoints,
    SetTotal_HistoryPoints_Error,
    detalle,
    actualizar,
};


// -----------------------------------
// Reducers
// -----------------------------------
const reducers = {
    [LOADER]: (state, { loader }) => {
        return {
            ...state,
            loader,
        };
    },
    [DATA]: (state, { data }) => {
        return {
            ...state,
            data,
        };
    },
    [ITEM]: (state, { item }) => {
        return {
            ...state,
            item,
        };
    },
    [PAGE]: (state, { page }) => {
        return {
            ...state,
            page,
        };
    },
    [ORDERING]: (state, { ordering }) => {
        return {
            ...state,
            ordering,
        };
    },
    [SEARCH]: (state, { search }) => {
        return {
            ...state,
            search,
        };
    },
    [STEP]: (state, { step }) => {
        return {
            ...state,
            step,
        };
    },
    [SETCOMMISSION_PERCENTAGE]: (state, {commission_percentage}) => {
        return {
            ...state,
            commission_percentage,
        };
    },

    [SETVENDEDOR_PERCENTAGE]: (state, {vendedor_percentage}) => {
        return {
            ...state,
            vendedor_percentage,
        };
    },

    [SPRINT]: (state, { sprint }) => {
        return {
            ...state,
            sprint,
        };
    },
    [DESIGNERS]: (state, { designer }) => {
        return {
            ...state,
            designer,
        };
    },
    [PROGRAMMERS]: (state, { programmer }) => {
        return {
            ...state,
            programmer,
        };
    },
    [TOTAL_HISTORYPOINTS]: (state, { total_historypoints }) => {
        return {
            ...state,
            total_historypoints,
        };
    },
    [HISTORY_POINTSERROR]: (state, { history_points_error }) => {
        return {
            ...state,
            history_points_error,
        };
    },

};



const initialState = {
    
    loader: false,
    data: {
        results: [],
        count: 0,
    },
    item: {},
    page: 1,
    ordering: '',
    search: '',
    commission_percentage: (0),
    vendedor_percentage: (0),
    sprint:(2),
    history_points_error:(0),
    total_historypoints:(20),
    programmer:(0),
    designer:(0),
};

export default handleActions(reducers, initialState);
