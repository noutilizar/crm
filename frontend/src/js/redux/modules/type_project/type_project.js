import { handleActions } from 'redux-actions';
import { push } from "react-router-redux";
import { initialize as initializeForm } from 'redux-form';
import { NotificationManager } from "react-notifications";
import { api } from "api";

const SUBMIT = 'TYPEPROJECT_SUBMIT';
const LOADER = 'TYPEPROJECT_LOADER';
const SET_DATA_CLIENTS = 'SET_DATA_PROJECTS';
const PAGE = 'SET_PAGE';

export const constants = {
    SUBMIT,
};

export const setLoader = loader => ({
    type: LOADER,
    loader,
});

const setData = data => ({
    type: SET_DATA_CLIENTS,
    data,
});

const setPage = page => ({
    type: PAGE,
    page,
});


/*actions*/

const typeClient = [
    { value: 1, label: 'Local'},
    { value: 2, label: 'Internacional'},
]


const formatData = ( values ) => {

    const { label } = values.name_type.value 
                    ? values.name_type 
                    : typeClient[ Number(values.client_type) - 1 || 0 ]

    return {
        name_project:values.name_project,
        name_type: label,
        average_sprint:values.average_sprint,
        error_range:values.error_range,
        month_support:values.month_support,
        hour_for_support_month:values.hour_for_support_month,
        hour_implementation:values.hour_implementation,
        hour_for_QA:values.hour_for_QA,
        hour_userManual:values.hour_userManual,
        hour_technicalManual:values.hour_technicalManual,
        hour_deployment:values.hour_deployment
    }

}

const onSubmit = () => (dispatch, getStore) => {

    const { values } = getStore().form.createTypeProject;

    const data = formatData(values)

    api.post('type_project', data).then((response) => {
        NotificationManager.success('Tipo de proyecto registrado correctamente', 'Éxito', 1000);
        dispatch(push('/type_project'));
    }).catch((error) => {
        console.log("ERROR:",error);
        NotificationManager.error('Verifica si el nombre no se repite', 'ERROR', 3000);
    });
};

const detalle = id => (dispatch) => {
    dispatch(setLoader(true));

    api.get(`type_project/${id}`).then((response) => {
        dispatch(initializeForm('createTypeProject', response));
    }).catch((error) => {
        NotificationManager.error(error.detail, 'ERROR', 0);
    }).finally(() => {
        dispatch(setLoader(false));
    });
};

const filterTypeClient = search => (dispatch) => {

    dispatch(setLoader(true));

    api.get(`type_project/search/${search}`).then((response) => {
        dispatch(setData(response));
    }).catch((error) => {
        /*NotificationManager.error(error.detail, 'ERROR', 0);*/
        NotificationManager.error('Hubo error en la busqueda', 'ERROR', 0);
    }).finally(() => {
        dispatch(setLoader(false));
    });
};

const listar = (page = 1) => (dispatch) => {
    console.log("ejecutando listar");
    dispatch(setLoader(true));
    const params = { page };

    api.get('type_project', params).then((response) => {
        console.log(response);
        dispatch(setData(response));
        dispatch(setPage(page));
    }).catch(() => {
    }).finally(() => {
        dispatch(setLoader(false));
    });
};

const eliminar = id => (dispatch) => {
    api.eliminar(`type_project/${id}`).then(() => {
        NotificationManager.success('Tipo de proyecto eliminado correctamente', 'Éxito', 1000);
        dispatch(listar());
    }).catch(() => {
        NotificationManager.error('Hubo error en la eliminación', 'ERROR', 0);
    });
};

const actualizar = () => (dispatch, getStore) => {
    const { values } = getStore().form.createTypeProject;
    
    const data = formatData(values)
    api.put(`type_project/${values.id}`, data).then(() => {
        NotificationManager.success('El tipo de proyecto se actualizó correctamente', 'Éxito', 1000);
        dispatch(push('/type_project'));
    }).catch(() => {
        NotificationManager.error('Hubo error en la actualización', 'ERROR', 0);
    });
};

export const actions = {
    onSubmit,
    listar,
    detalle,
    eliminar,
    actualizar,
    filterTypeClient,
};

export const reducers = {
    [LOADER]: (state, { loader }) => {
        return {
            ...state,
            loader,
        };
    },
    [SET_DATA_CLIENTS]: (state, { data }) => {
        return {
            ...state,
            data,
        };
    },
    [PAGE]: (state, { page }) => {
        return {
            ...state,
            page,
        };
    },
};

export const initialState = {
    loader: false,
    data: null,
    page: 1,
};

export default handleActions(reducers, initialState);
