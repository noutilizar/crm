import {handleActions} from 'redux-actions';
import {createReducer} from "../baseReducer/baseReducer";


// ------------------------------------
// Constants
// ------------------------------------

export const { reducers, initialState, actions } = createReducer(
    "sales_channel",
    "sales_channel",
    "SalesChannelForm",
    "/sales_channel",
);

export default handleActions(reducers, initialState);
