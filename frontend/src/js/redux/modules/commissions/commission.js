
import { handleActions } from 'redux-actions';
import { push } from "react-router-redux";
import { initialize as initializeForm } from 'redux-form';
import { NotificationManager } from "react-notifications";
import { api } from "api";
import _ from 'lodash'


const SUBMIT = 'COMMISSION_SUBMIT';
const LOADER = 'COMMISSION_LOADER';
const SET_DATA_COMMISSION = 'SET_DATA_COMMISSION';
const PAGE = 'SET_PAGE';
const OPEN_MODAL = 'OPEN_MODAL';

export const constants = {
    SUBMIT,
};

export const setLoader = loader => ({
    type: LOADER,
    loader,
});

const setData = data => ({
    type: SET_DATA_COMMISSION,
    data,
});

const setPage = page => ({
    type: PAGE,
    page,
});

const typeClient = [
    { value: 1, label: 'Local'},
    { value: 2, label: 'Internacional'},
]


const formatData = ( values ) => {

    const { label } = values.name_type.value 
                    ? values.name_type 
                    : typeClient[ Number(values.client_type) - 1 || 0 ]

    return {
        name_user:values.name_user.value,
        starting_range:values.starting_range,
        final_rank:values.final_rank,
        percentage_commission:values.percentage_commission,
        name_type: label,
    }

}



const detalle = (id) => (dispatch) => {



    /*dispatch(setLoader(true));*/

    api.get(`commissions/get_permissions_user`,{user:id}).then((response) => {
        const formData={}
        formData.commissions=response
        dispatch(initializeForm('createCommissions', formData));
        console.log("Probando detalle",formData);

    }).catch((error) => {
        NotificationManager.error(error.detail, 'ERROR', 0);
    }).finally(() => {
        /*dispatch(setLoader(false));*/
    });
};

const registrar = () => (dispatch, getStore) => {
    /*dispatch({type: SET_LOADER, loader: true});*/
    const form=getStore().form.createCommissions
    const formData=form.values
    //const data=formatData(values)
    console.log(formData);
    api.post('commissions', formData).then((response) => {
        NotificationManager.success(response.detail, 'Éxito', 1000);
        dispatch(push("/commission"));
    }).catch((error) => {
        console.log(error);
        NotificationManager.error('Error al crear las comisiones', 'ERROR', 0);
    }).finally(() => {
        /*dispatch({type: SET_LOADER, loader: false});*/
    });
}


const actualizar = () => (dispatch, getStore) => {
    /*const { values } = getStore().createConfigutarion;

    const data = formatData(values)*/

    const form=getStore().form.createCommissions
    const formatData=form.values

    api.put(`commissions/actualizar`,formatData).then(() => {
        NotificationManager.success('El contacto se actualizó correctamente', 'Éxito', 1000);
        /*dispatch(push('/configuration'));*/
        dispatch(detalle(formatData.name_user.value))
    }).catch(() => {
        NotificationManager.error('Hubo error en la actualización', 'ERROR', 0);
    });
};
const listar = (page = 1) => (dispatch) => {
    console.log("ejecutando listar");
    dispatch(setLoader(true));
    const params = { page };

    api.get('user', params).then((response) => {
        console.log(response);
        dispatch(setData(response));
        dispatch(setPage(page));
    }).catch(() => {
    }).finally(() => {
        dispatch(setLoader(false));
    });
};
export const actions={
    detalle,
    actualizar,
    registrar,
    listar,
}
export const reducers = {
    [LOADER]: (state, { loader }) => {
        return {
            ...state,
            loader,
        };
    },
    [SET_DATA_COMMISSION]: (state, { data }) => {
        return {
            ...state,
            data,
        };
    },
    [PAGE]: (state, { page }) => {
        return {
            ...state,
            page,
        };
    },
};

export const initialState = {
    loader:false,
    data:null

};

export default handleActions(reducers, initialState);