# Models
from django.db import models

from api.utils.baseModel import BaseModel


class PercentageGoals(BaseModel):
    percentage = models.FloatField()
    percentage_quetzal = models.FloatField()

    # RelationShip
    annual_goal = models.ForeignKey(
        'AnnualGoal', on_delete=models.CASCADE, related_name="percentage_goals")
    business_lines = models.ForeignKey(
        'BusinessLines', on_delete=models.CASCADE, related_name="percentage_goals")
