from django.db import models
from api.utils.baseModel import BaseModel

class TypeClient(BaseModel):
    """Model definition for administration type clients"""
    name_type = models.CharField(max_length=30, null=False, blank=False)
    rate_for_hour=models.FloatField(null=True, blank=True)
    rate_for_hour_priority_support=models.FloatField()
    rate_for_hour_support_normal=models.FloatField()
    rate_for_month=models.FloatField()

   