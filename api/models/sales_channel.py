# Models
from django.db import models
from api.utils.baseModel import BaseModel


class SalesChannel(BaseModel):
    name = models.CharField(max_length=40)
    commission_percentage = models.FloatField()

  