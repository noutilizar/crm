from django.db import models
from api.utils.baseModel import BaseModel


class AnnualGoal(BaseModel):
    annual_goal = models.IntegerField()
    goal_quetzales = models.FloatField()
    is_active_goal = models.BooleanField(default=False)
    goal_first_trimester = models.FloatField()
    goal_second_trimester = models.FloatField()
    goal_third_trimester = models.FloatField()
    goal_fourth_trimester = models.FloatField()
    goal_first_trimester_quetzal = models.FloatField()
    goal_second_trimester_quetzal = models.FloatField()
    goal_third_trimester_quetzal = models.FloatField()
    goal_fourth_trimester_quetzal = models.FloatField()

    def delete(self, *args):
        self.is_active = False
        self.save()
        return True
