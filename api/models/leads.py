"""Model Leads"""

from django.db import models
from api.utils.baseModel import BaseModel


class Leads(BaseModel):
    name = models.CharField(max_length=100)
    amount = models.FloatField()
    quetzal = models.FloatField()
    cycle_days = models.IntegerField(null=True)
    # creation_date = models.DateField()
    possibility_won = models.BooleanField(default=False, null=True)
    closing_date = models.DateField(blank=True, null=True)
    possibility_of_closure = models.IntegerField(null=True)
    description = models.CharField(max_length=150, null=True, blank=True, default='')

    # RelationShip
    sales_funnel = models.ForeignKey(
        'SalesFunnel', on_delete=models.CASCADE, related_name="leads",
        null=True)
    coin_lead = models.ForeignKey('ChangeCoin', on_delete=models.CASCADE, null=True)
    owner_lead = models.ForeignKey('User', on_delete=models.CASCADE, null=True)
    company = models.ForeignKey('Company', on_delete=models.CASCADE, null=True)
    contact = models.ForeignKey('Contact', on_delete=models.CASCADE, null=True)
    sales_channel = models.ForeignKey('SalesChannel', on_delete=models.CASCADE, null=True)
    business_stage = models.ForeignKey('StageSales', on_delete=models.CASCADE, null=True)
