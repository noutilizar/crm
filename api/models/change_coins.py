""" Model change_coins """
from django.db import models
from api.utils.baseModel import BaseModel


class ChangeCoin(BaseModel):
    coin_code = models.CharField(max_length=5)
    coin_type = models.CharField(max_length=50, null=True, blank=True)
    change_type = models.FloatField(null=True, blank=True)

    def __str__(self):
        return self.coin_type


    def delete(self, *args):
        self.is_active = False
        self.save()
        return True