""" Model administrative_expenses """
from django.db import models
from api.utils.baseModel import BaseModel

class IsActiveManager(models.Manager):
    def get_queryset(self):
        return super(IsActiveManager, self).get_queryset().filter(is_active=True)


class AdministrativeExpenses(BaseModel):
    name_expense = models.CharField(max_length=50, null=True, blank=True)
    number_hours = models.FloatField(blank=True, null=True)
    total_quantity = models.FloatField(blank=True, null=True)
    rate_hour = models.FloatField(blank=True, null=True)
    total_hours = models.FloatField(blank=True, null=True)

    # Relationship
    budget = models.ForeignKey('Budget', on_delete=models.CASCADE, related_name='adminmodules')
    administrative_expenses_header = models.ForeignKey('AdministrativeExpensesHeader', on_delete=models.CASCADE, related_name = 'adminmodules')
    objects = IsActiveManager()
    def __str__(self):
        return self.name_expense






    
    