""" Model administrative_expenses_header """
from django.db import models
from api.utils.baseModel import BaseModel


class AdministrativeExpensesHeader(BaseModel):
    type_project = models.ForeignKey('TypeProject', on_delete=models.CASCADE, blank=False, null=True, related_name = 'header_typeProject')