""" MODEL bitacora """

from django.db import models


class Bitacora(models.Model):
    previousData = models.TextField()
    currentData = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    # Relationship
    user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='bitacora')
    action = models.ForeignKey('Action', on_delete=models.CASCADE, related_name='bitacora')

    # description = models.CharField(max_length=255, null=True)


class Action(models.Model):
    name = models.CharField(max_length=75)
