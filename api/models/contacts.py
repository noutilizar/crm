""" Model contacts """
from django.db import models
from api.utils.baseModel import BaseModel


class Contact(BaseModel):
    name = models.CharField(max_length=120, null=True, blank=True)
    email = models.EmailField(max_length=50, null=True, blank=True)
    phone_staff = models.CharField(max_length=16, null=True, blank=True)
    phone_business = models.CharField(max_length=16, null=True, blank=True)
    position = models.CharField(max_length=40, null=True, blank=True)
    client_type = models.CharField(max_length=14, null=True, blank=True)

    # Relationship
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    life_cycles = models.ForeignKey('LifeCycle', on_delete=models.CASCADE)
    userowner = models.ForeignKey('User', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.first_name
