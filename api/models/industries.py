""" MODEL industries """

from django.db import models
from api.utils.baseModel import BaseModel


class Industry(BaseModel):
    name = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return self.name
