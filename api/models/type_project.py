""" Model Type Project """
from django.db import models
from api.utils.baseModel import BaseModel


class TypeProject(BaseModel):
    name_project=models.CharField(max_length=100, null=False, blank=False)
    name_type=models.CharField(max_length=14, null=False, blank=False)
    average_sprint=models.IntegerField()
    error_range=models.FloatField()
    month_support=models.IntegerField()
    hour_for_support_month=models.FloatField()
    hour_implementation=models.FloatField()
    hour_for_QA=models.FloatField()
    hour_userManual=models.FloatField()
    hour_technicalManual=models.FloatField()
    hour_deployment=models.FloatField()

    def __str__(self):
        return self.name_project


