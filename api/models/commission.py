"""Model Commissions"""

from django.db import models
from api.utils.baseModel import BaseModel

class Commission(BaseModel):
    # RelationShip
    name_user = models.ForeignKey('User',related_name='commissions_users', on_delete=models.CASCADE, null=False)
    
    starting_range=models.FloatField()
    final_rank=models.FloatField()
    percentage_commission=models.FloatField()
    name_type=models.CharField(max_length=14, null=False, blank=False)

    """def __str__(self):
        return str(self.starting_range)"""
    
