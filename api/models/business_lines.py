from django.db import models

from api.utils.baseModel import BaseModel


class BusinessLines(BaseModel):
    name = models.CharField(max_length=15)
    description = models.CharField(max_length=100, null=True, blank=True)
