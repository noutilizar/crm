""" Model budget_modules """
from django.db import models
from api.utils.baseModel import BaseModel


class BudgetModules(BaseModel):
    name_modules = models.CharField(max_length=50, null=True, blank=True)
    history_points = models.IntegerField(blank=True, null=True)
    history_points_error = models.IntegerField(blank=True, null=True)
    total_history_points = models.IntegerField(blank=True, null=True)
    total_start_hours = models.FloatField(blank=True, null=True)
    total_start_hours_error = models.FloatField(blank=True, null=True)
    total_hours = models.FloatField(blank=True, null=True)

    # Relationship
    # budget = models.ForeignKey('Budget', on_delete=models.CASCADE)

    def __str__(self):
        return self.name_modules