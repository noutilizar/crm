"""Model vendedor """
from django.db import models

class Vendedor(models.Model): 
    nombre = models.CharField(max_length=200)
    apellido= models.CharField(max_length=200)
    direccion= models.CharField(max_length=200)
    celular= models.CharField(max_length=200)
    email = models.EmailField(max_length=50)
    vendedor_percentage = models.FloatField(default=0)
    activo = models.BooleanField(default=True)
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)


    def delete(self, *args):
        self.activo = False
        self.save()
        self.active = False
        self.save()
        return True