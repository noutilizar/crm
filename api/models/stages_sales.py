# Models
from django.db import models
from api.utils.baseModel import BaseModel


class StageSales(BaseModel):
    name = models.CharField(max_length=40)
    leads_ordering = models.TextField(blank=True, null=True)
    percentage = models.FloatField(blank=True, null=True)

    # RelationShip
    sale_funnel = models.ForeignKey(
        'SalesFunnel', on_delete=models.CASCADE, related_name="stage_sales",
        blank=True, null=True)
    
    def __str__(self):
        return self.name
