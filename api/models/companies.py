"""Model Company"""

from django.db import models
from api.utils.baseModel import BaseModel


class Company(BaseModel):
    name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=16, blank=True)
    cities = models.CharField(max_length=12, blank=True)
    employee_number = models.IntegerField(blank=True, null=True)
    annual_income = models.FloatField(blank=True, null=True)
    description = models.CharField(max_length=40, blank=True)
    linkedIn_page = models.CharField(max_length=100, blank=True)
    facebook_page = models.CharField(max_length=100, blank=True)

    # Relationship
    industry = models.ForeignKey('Industry', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
