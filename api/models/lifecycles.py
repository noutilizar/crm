""" MODEL Customer life cycle """

from django.db import models
from api.utils.baseModel import BaseModel


class LifeCycle(BaseModel):
    name = models.CharField(max_length=30, null=False, blank=False)

    def __str__(self):
        return self.name
