# Models
from django.db import models

from api.utils.baseModel import BaseModel


class SalesFunnel(BaseModel):
    name = models.CharField(max_length=40)
    percentage_line = models.FloatField()
    ordering = models.TextField()

    # Relationships
    business_lines = models.ForeignKey(
        'BusinessLines', on_delete=models.CASCADE, related_name="business_sales")
    sales_channel = models.ForeignKey(
        'SalesChannel', on_delete=models.CASCADE, related_name="sales_channels")
