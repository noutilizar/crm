""" Model budget_modules """
from django.db import models
from api.utils.baseModel import BaseModel

class IsActiveManager(models.Manager):
    def get_queryset(self):
        return super(IsActiveManager, self).get_queryset().filter(is_active=True)

class BudgetRelationModule(BaseModel):
    name_modules = models.CharField(max_length=50, null=True, blank=True)
    history_points = models.IntegerField(blank=True, null=True)
    history_points_error = models.IntegerField(blank=True, null=True)
    total_history_points = models.IntegerField(blank=True, null=True)
    total_start_hours = models.FloatField(blank=True, null=True)
    total_start_hours_error = models.FloatField(blank=True, null=True)
    total_hours = models.FloatField(blank=True, null=True)

    # Relationship
    budget = models.ForeignKey('Budget', on_delete=models.CASCADE, related_name='budget_relation_modules')
    objects = IsActiveManager()
    
    def __str__(self):
        return self.name_modules


    
   