""" Model administrative_expenses_detail"""
from django.db import models
from api.utils.baseModel import BaseModel


class IsActiveManager(models.Manager):
    def get_queryset(self):
        return super(IsActiveManager, self).get_queryset().filter(is_active=True)


class AdministrativeExpensesDetail(BaseModel):
    name_expense = models.CharField(max_length=50, null=True, blank=True)
    number_hours = models.FloatField(blank=True, null=True)
    total_quantity = models.FloatField(blank=True, null=True)
    rate_hour = models.FloatField(blank=True, null=True)
    total_hours = models.FloatField(blank=True, null=True)

    # Relationship
    administrative_expenses_header = models.ForeignKey('AdministrativeExpensesHeader', on_delete=models.CASCADE, related_name = 'modules')

    objects = IsActiveManager()
    