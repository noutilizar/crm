from django.db import models
from api.utils.baseModel import BaseModel


class Tasks(BaseModel):
    name = models.CharField(max_length=40)
    description = models.CharField(max_length=150, null=True, blank=True, default='')
    limit_date = models.DateField(blank=True, null=True)

    # RelationShip
    lead = models.ForeignKey(
        'Leads', on_delete=models.CASCADE, related_name="tasks",
        blank=True, null=True)
