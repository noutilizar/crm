"""Model presupuesto """
from django.db import models

class Budget(models.Model): 
    tipo_cliente = models.ForeignKey('TypeClient', on_delete=models.CASCADE, blank=False, null=True)
    tipo_proyecto= models.ForeignKey('TypeProject', on_delete=models.CASCADE, blank=False, null=True)
    lead =models.ForeignKey('Leads', on_delete=models.CASCADE, blank=False, null=True)
    cantidad_programadores = models.IntegerField(default=0) 
    cantidad_diseñadores = models.IntegerField(default=0) 
    porcentaje_error = models.FloatField(default=0)
    precio_nominal = models.FloatField(default=0)
    canal_venta = models.ForeignKey('SalesChannel', on_delete=models.CASCADE, blank=False, null=True)
    vendedor = models.ForeignKey('Vendedor', on_delete=models.CASCADE, blank=False, null=True)


    porcentaje_comisionCanalVenta = models.FloatField(default=0, blank=True)
    porcentaje_comisionVendedor = models.FloatField(default=0, blank=True)
   
   
    total_comisionCanal = models.FloatField(default=0, blank=True)
    total_comisionVendedor = models.FloatField(default=0, blank=True)
    tiempo_total = models.IntegerField(default=0, blank=True) 
    semanas_proyecto = models.IntegerField(default=0, blank=True) 
    meses_proyecto= models.IntegerField(default=0, blank=True) 
    total_horasProyecto = models.IntegerField(default=0, blank=True) 
    total_semanasMargenError = models.IntegerField(default=0, blank=True)
    total_horasMargenError = models.IntegerField(default=0, blank=True) 
    precio_baseDesarrollo = models.FloatField(default=0, blank=True)
    
    gastos_adminitrativos = models.FloatField(default=0, blank=True)
    total_horasAdministrativas = models.IntegerField(default=0, blank=True) 
    
    total_semanasAdministrativas = models.IntegerField(default=0, blank=True) 
    total_mesesAdministrativas= models.IntegerField(default=0, blank=True) 
    
    duracion_totalProyecto= models.IntegerField(default=0, blank=True) 
    precio_total = models.FloatField(default=0, blank=True)
