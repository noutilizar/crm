""" Sales Funnel ViewSet """

import json
import pdb
# Django
from django_filters.rest_framework import DjangoFilterBackend
from django.db import transaction

from copy import deepcopy

# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status, filters, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated

# Model
from api.models import SalesFunnel, BusinessLines, SalesChannel, PercentageGoals, StageSales

# Serializers
from api.serializers import SalesFunnelRegistroSerializer, SalesFunnelSerializer, ReadFunnelSerializer

# Utils
from api.utils.bitacora import RegistroBitacora


class SalesFunnelViewSet(viewsets.ModelViewSet):
    queryset = SalesFunnel.objects.filter(is_active=True)

    filter_backends = (DjangoFilterBackend,
                       filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ("name", 'business_lines', 'sales_channel')
    search_fields = ("name", 'business_lines', 'sales_channel')
    ordering_fields = ("name", 'business_lines', 'sales_channel')

    def get_serializer_class(self):
        """Define serializer for API"""
        if self.action == 'list' or self.action == 'retrieve':
            return SalesFunnelSerializer
        else:
            return SalesFunnelRegistroSerializer

    def get_permissions(self):
        """" Define permisos para este recurso """
        if self.action == "create" or self.action == "token":
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    def perform_destroy(self, salesFunnel):
        """ Desactivate a Sales Funnel when it is deleted """
        salesFunnel.is_active = False
        salesFunnel.save()
        StageSales.objects.filter(
            sale_funnel_id=salesFunnel).update(is_active=False)

    @action(detail=False, methods=['get'], url_path='search/(?P<name>[-a-zA-Z0-9_]+)')
    def search(self, request, name):

        salesFunnels = SalesFunnel.objects.filter(
            is_active=True, name__contains=name)
        page = self.paginate_queryset(salesFunnels)
        serializer = self.get_serializer(page, many=True)

        return self.get_paginated_response(serializer.data)

    def create(self, request, *args, **kwargs):
        try:
            with transaction.atomic():
                data = request.data
                etapas = data.get('etapas')
                stringEtapas = json.dumps(etapas)
                serializer = SalesFunnelRegistroSerializer(data=request.data)
                if(serializer.is_valid()):
                    id_line = data.get('business_lines').get('value')
                    businessLines = BusinessLines.objects.get(pk=id_line)
                    id_channel = data.get('sales_channel').get('value')
                    salesChannel = SalesChannel.objects.get(pk=id_channel)
                    salesFunnel = SalesFunnel.objects.create(
                        business_lines=businessLines,
                        sales_channel=salesChannel,
                        name=data.get('name'),
                        percentage_line=data.get('percentage_line'),
                    )
                    if salesFunnel:
                        ordering = []
                        for sale in etapas:
                            stage_sale = StageSales.objects.create(
                                name=sale['name'],
                                percentage=float(sale['percentage']))
                            stage_sale.sale_funnel = salesFunnel
                            stage_sale.leads_ordering = json.dumps([])
                            stage_sale.save()
                            ordering.append(stage_sale.id)
                        salesFunnel.ordering = json.dumps(ordering)
                        salesFunnel.save()

                    # RegistroBitacora.crearEmpresa(request.user, serializer.data)
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        try:
            instancia = self.get_object()
            data = request.data
            print("data: ", data)
            etapas = data.get('etapas')
            stringEtapas = json.dumps(etapas)
            serializer = SalesFunnelRegistroSerializer(data=request.data)
            if(serializer.is_valid()):
                id_line = data.get('business_lines').get('value')
                businessLines = BusinessLines.objects.get(pk=id_line)

                id_channel = data.get('sales_channel').get('value')
                salesChannel = SalesChannel.objects.get(pk=id_channel)

                instancia.business_lines = businessLines
                instancia.sales_channel = salesChannel
                instancia.name = data.get('name')
                instancia.percentage_sale = data.get('percentage_line')
                instancia.save()
                if instancia:
                    ordering = []
                    for stage in etapas:
                        try:
                            stage_sale = StageSales.objects.get(pk=stage['id'])
                            stage_sale.percentage = float(stage['percentage'])
                            stage_sale.sale_funnel = instancia
                            stage_sale.save()
                            ordering.append(stage_sale.id)
                        except StageSales.DoesNotExist:
                            stage_sale = StageSales.objects.create(
                                name=stage['name'])
                            stage_sale.percentage = float(stage['percentage'])
                            stage_sale.leads_ordering = json.dumps([])
                            stage_sale.sale_funnel = instancia
                            stage_sale.save()
                            ordering.append(stage_sale.id)
                    instancia.ordering = json.dumps(ordering)
                    instancia.save()

                    # RegistroBitacora.crearEmpresa(request.user, serializer.data, json_anterior.data)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'], url_path='search/(?P<name>[-a-zA-Z0-9_]+)')
    def search(self, request, name):

        business = SalesFunnel.objects.filter(
            is_active=True, name__contains=name)
        page = self.paginate_queryset(business)
        serializer = SalesFunnelSerializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    @action(detail=False, methods=['get'])
    def funnels_list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = ReadFunnelSerializer(queryset, many=True)
        return Response(serializer.data)
