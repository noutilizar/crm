""" Industries ViewSet """

# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response


# Model
from api.models import Tasks

# Serializers
from api.serializers import TaskModelSerializer, TaskRegisterSerializer

from django.db import transaction

class TaskViewSet(viewsets.ModelViewSet):

    queryset = Tasks.objects.filter(is_active=True)

    def get_serializer_class(self):
        """Define serializer for API"""
        if self.action == 'list' or self.action == 'retrieve':
            return TaskModelSerializer
        else:
            return TaskRegisterSerializer

    def perform_destroy(self, task):
        """ Desactivate an industry when it is deleted """
        task.is_active = False
        task.save()

    @action(detail=False, methods=['post'])
    def filterTasks(self, request):

        lead_id = request.data    
        query = Tasks.objects.filter(lead=lead_id)
        page = self.paginate_queryset(query)
        serializer = TaskModelSerializer(page, many=True)
        return self.get_paginated_response(serializer.data)
