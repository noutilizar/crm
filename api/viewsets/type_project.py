"""ViewSet Type of Project"""
# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

#model 
from api.models.type_project import TypeProject
#serializer
from api.serializers.type_project import TypeProjectModelSerializer,TypeProjectRegisterSerializer

from django.db import transaction

class TypeProjectViewSet(viewsets.ModelViewSet):

    queryset=TypeProject.objects.filter(is_active=True)

    def get_serializer_class(self):
        """Define serializer for API"""
        if self.action == 'list' or self.action == 'retrieve':
            return TypeProjectModelSerializer
        else:
            return TypeProjectRegisterSerializer

    def perfom_destroy(self,type_project):
        """ Desactivate an type_project when it is deleted """
        type_project.is_active = False
        type_project.save()

     
    @action(detail=False, methods=['get'], url_path='search/(?P<name_project>[-a-zA-Z0-9_ ]+)')
    def search(self, request, name_project ):
        
        typeprojects = TypeProject.objects.filter(is_active=True, name_project__contains=name_project)
        page = self.paginate_queryset(typeprojects)
        serializer = self.get_serializer(page, many=True)

        return self.get_paginated_response(serializer.data)

    
            



