""" Companies ViewSet """

# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status, filters, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated

# Model
from api.models import Company

# Serializers
from api.serializers import CompanyModelSerializer, CompanyReadModelSerializer, CompanyRetrieveModelSerializer

# Utils
from api.utils.bitacora import RegistroBitacora


class CompanyViewSet(viewsets.ModelViewSet):

    queryset = Company.objects.filter(is_active=True)

    def perform_destroy(self, company ):
        """ Desactivate a company when it is deleted """
        company.is_active=False
        company.save()

    def get_serializer_class(self):

        if self.action == 'list':
            return CompanyReadModelSerializer
        
        elif self.action == 'retrieve':
            return CompanyRetrieveModelSerializer
        
        return CompanyModelSerializer
    
    @action(detail=False, methods=['get'], url_path='search/(?P<name>[-a-zA-Z0-9_]+)')
    def search(self, request, name ):
        
        companies = Company.objects.filter(is_active=True, name__contains=name)
        page = self.paginate_queryset(companies)
        serializer = CompanyReadModelSerializer(page, many=True)

        return self.get_paginated_response(serializer.data)
