"""Lead Management ViewSets"""
import json
import datetime
# rest_framework
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action

# models
from api.models import Leads, SalesFunnel, StageSales, Leads

# serializer
from api.serializers import LeadModelSerializer, ReadSalesFunnelSerializer


class LeadManagementViewset(viewsets.ReadOnlyModelViewSet):
    """View Set """

    queryset = SalesFunnel.objects.filter(is_active=True)
    serializer_class = ReadSalesFunnelSerializer

    @action(detail=False, methods=['post'])
    def move_leads_stages(self, request, *args, **kwargs):
        """Guarda el movimiento de los leads dentro de las diferentes etapas de ventas"""
        start_stage = request.data['start_stage']
        finish_stage = request.data['finish_stage']
        stage_s = StageSales.objects.get(pk=start_stage['id'])
        stage_f = StageSales.objects.get(pk=finish_stage['id'])
        stage_s.leads_ordering = json.dumps(start_stage['leads_ordering'])
        stage_f.leads_ordering = json.dumps(finish_stage['leads_ordering'])
        stage_s.save()
        stage_f.save()

        #Update lead
        lead = Leads.objects.get(pk=request.data['lead'])
        lead.business_stage = stage_f
        lead.possibility_of_closure = stage_f.percentage
        lead.save()

        # Actualiza los dias de ciclo de vida segun el ultimo movimiento de etapa
        if(lead.business_stage != 'Perdido' or lead.business_stage != 'Ganado'):
            lead.cycle_days = (lead.modified - lead.created).days
            lead.save()

        return Response(status=status.HTTP_202_ACCEPTED)

    @action(detail=False, methods=['post'])
    def move_leads(self, request, *args, **kwargs):
        """Guarda el movimiento de un lead dentro de la misma etapa de venta"""
        stage = request.data
        stage_move = StageSales.objects.get(pk=stage['id'])
        stage_move.leads_ordering = json.dumps(stage['leads_ordering'])
        stage_move.save()
        return Response(status=status.HTTP_202_ACCEPTED)


    @action(detail=False, methods=['post'])
    def filtros(self, request, *args, **kwargs):
        """Filtrado de los Leads"""
        embudo_id = request.data.get('embudo_id')
        print("filtros ", request.data)
        embudo = SalesFunnel.objects.get(pk=embudo_id)
        query = embudo.leads.filter(
            is_active=True).values('id', 'name','closing_date','possibility_won')
        
        if request.data.get("user_id", 0) != 0:
            user_id = request.data['user_id']
            query=query.filter(owner_lead=user_id)

        if request.data.get("option")=="created":
            if request.data.get("date_initial") and request.data.get("date_finish") :
                date_initial=request.data['date_initial']
                date_finish= request.data['date_finish']
            query=query.filter(created__range=[date_initial, date_finish])

        if request.data.get("option")=="activity":
            if request.data.get("date_initial") and request.data.get("date_finish") :
                date_initial=request.data['date_initial']
                date_finish= request.data['date_finish']
            query=query.filter(modified__range=[date_initial, date_finish])

        if query.exists():
            return Response({'leads': [x for x in query]})
        return Response({'leads': []})

