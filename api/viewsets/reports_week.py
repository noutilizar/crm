""" Repors of Lead ViewSet """

# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response


# Model
from api.models import Leads, AnnualGoal, BuenasNoticias, SalesFunnel, StageSales

# Serializers
from api.serializers.leads import LeadModelSerializer, LeadReportSerializer

# Utils
from django.db.models import Sum, Count
from datetime import datetime

class ReportWeekViewSet(viewsets.ModelViewSet):
    queryset = Leads.objects.filter(is_active=True)
    serializer_class = LeadModelSerializer

    @action( detail=False, methods=['post'])
    def registrarnoticia(self, request):

        if BuenasNoticias.objects.filter( created__week=request.data.get('semana'),
                                          created__year=request.data.get('anio') ).exists():

            noticia =  BuenasNoticias.objects.get( created__week=request.data.get('semana'),
                                                    created__year=request.data.get('anio') )
            noticia.text = request.data.get('text')
            noticia.save()

        else:
            BuenasNoticias.objects.create(text=request.data.get('text'))
        
        return Response(status=status.HTTP_200_OK)


    # ===================================
    #  Obtiene todos los lead que se 
    #  registraron en la semana
    # ===================================
    @action( detail=False, methods=['get'])
    def leadsweek(self, request):

        query = Leads.objects.filter(is_active=True, 
            created__week= request.GET.get('semana'),
            created__year= request.GET.get('anio'))
            
        mount = query.aggregate(total=Sum('quetzal'))

        page = self.paginate_queryset(query)
        serializer = LeadReportSerializer(page, many=True)

        serializerr = serializer.data
        serializerr.append(mount)

        return self.get_paginated_response(serializerr)
    
    # ===================================
    #  Obtiene todos los lead con mayor
    #  posibilidad de cierre en la semana
    # ===================================
    @action( detail=False, methods=['get'])
    def leadsweekwon(self, request):

        query = Leads.objects.filter(is_active=True, 
            possibility_won=True, 
            created__week= request.GET.get('semana'),
            created__year= request.GET.get('anio'))

        mount = query.aggregate(total=Sum('quetzal'))

        page = self.paginate_queryset(query)
        serializer = LeadReportSerializer(page, many=True)

        serializerr = serializer.data
        serializerr.append(mount)

        return self.get_paginated_response(serializerr)
    
    # ----------------------------------------------------
    #  SEMANAL
    #  Obtiene cantidad de lead y la suma total del dinero
    # ----------------------------------------------------
    @action( detail=False, methods=['post'])
    def ventasemanal(self, request):

        sum_semanal = Leads.objects.filter(
            is_active=True,
            created__week=request.data.get('semana'),
            created__year=request.data.get('anio'),
            business_stage__in=[x.id for x in StageSales.objects.filter(name='Ganado')]
            ).aggregate(total_quetzals=Sum('quetzal'))

        try:
            text = BuenasNoticias.objects.get(
                created__week=request.data.get('semana'),
                created__year=request.data.get('anio') )
            text = text.text

        except BuenasNoticias.DoesNotExist:
            text = ''

        return Response({
                'semanal': sum_semanal['total_quetzals'],
                'text': text
            }, status=status.HTTP_200_OK)
    
    # ----------------------------------------------------
    #  MENSUAL
    #  Obtiene la suma total del dinero
    # ----------------------------------------------------
    @action( detail=False, methods=['post'])
    def metamensual(self, request):

        try:

            anio = AnnualGoal.objects.get(annual_goal=request.data.get('anio'))
            mes = request.data.get('mes')
            trimrestre = 0

            # Obtiene el trimestre del mes actual
            if mes >= 1 and mes <= 3:
                trimrestre = anio.goal_first_trimester_quetzal
            elif mes >= 4 and mes <= 6: 
                trimrestre = anio.goal_second_trimester_quetzal
            elif mes >= 7 and mes <= 9: 
                trimrestre = anio.goal_third_trimester_quetzal
            elif mes >= 10 and mes <= 12: 
                trimrestre = anio.goal_fourth_trimester_quetzal

            # Otiene la suma de todos los leads ganados en el mes
            count_sum_mensual = Leads.objects.filter(
                    is_active=True,
                    created__month=mes,
                    business_stage__in=[x.id for x in StageSales.objects.filter(name='Ganado')]
                ).aggregate(total_quetzals=Sum('quetzal'))
                
            mensual = round(trimrestre/3,2)
            porcentaje_mensual = 0

            if count_sum_mensual['total_quetzals']:
                porcentaje_mensual = round((count_sum_mensual['total_quetzals']/mensual)*100,2)


            data_mensual = {
                'meta_mensual': mensual,
                'total': count_sum_mensual['total_quetzals'],
                'porcentaje': porcentaje_mensual
            }

            return Response({
                'mensual': data_mensual,
            }, status=status.HTTP_200_OK)

        except AnnualGoal.DoesNotExist:
            return Response({
                'mensual': {
                    'meta_mensual': 0,
                    'total': 0,
                    'porcentaje': 0
                }
            }, status=status.HTTP_200_OK)


    # ----------------------------------------------------
    #  ANUAL
    # Obtiene cantidad de lead y la suma total del dinero
    # ----------------------------------------------------
    @action( detail=False, methods=['post'])
    def metaanual(self, request):
        try:

            anio = AnnualGoal.objects.get(annual_goal=request.data.get('anio'))

            # Obtiene todos los leas desde el 1 de enero hasta la fecha actual
            lead_anual = Leads.objects.filter(
                is_active=True,
                created__year=request.data.get('anio'),
                created__lte=datetime.now())

            total_lead = lead_anual.count() 

            # El total en dinero de los lead ganados hasta la fecha
            sum_anual = lead_anual.filter(
                business_stage__in=[x.id for x in StageSales.objects.filter(name='Ganado')]
                ).aggregate(total_quetzals=Sum('quetzal'))

            meta_anual = anio.goal_quetzales
            porcentaje_alcanzado = 0

            # Saca el porcentaje logrado
            if sum_anual['total_quetzals']:
                alcanzado_anual = sum_anual['total_quetzals']
                porcentaje_alcanzado = round(( alcanzado_anual / meta_anual)*100,2)

                restante_anual = round( (meta_anual - alcanzado_anual ), 2)
                restante_porcentaje = round( (restante_anual / meta_anual )*100, 2)
            
            data_anual = {
                'meta_anual': meta_anual,
                'total_lead': total_lead,
                'alcanzado_anual': alcanzado_anual,
                'porcentaje_alcanzado': porcentaje_alcanzado,
                'restante_anual': restante_anual,
                'restante_porcentaje': restante_porcentaje
            }

            return Response({
                'anual': data_anual,
            }, status=status.HTTP_200_OK)

        except AnnualGoal.DoesNotExist:
            return Response({
                'anual':  {
                    'meta_anual': 0,
                    'total_lead': 0,
                    'total_quetzals': 0,
                    'porcentaje': 0
                }
            }, status=status.HTTP_200_OK)