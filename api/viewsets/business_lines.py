from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from api.models import BusinessLines
from api.serializers import (
    BusinessLSerializer,
    BusinessLRegisterSerializer,
    EditBusinessLinesSerializer)
#from api.utils.bitacora import RegistroBitacora
from django.db import transaction
from copy import deepcopy


class BusinessLViewset(viewsets.ModelViewSet):
    queryset = BusinessLines.objects.filter(is_active=True)
    filter_backends = (DjangoFilterBackend,
                       filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ("name",)
    search_fields = ("name",)
    ordering_fields = ("name",)

    def get_serializer_class(self):
        """Define serializer for API"""
        if self.action == 'list' or self.action == 'retrieve':
            return BusinessLSerializer
        elif self.action in ['update']:
            return EditBusinessLinesSerializer
        else:
            return BusinessLRegisterSerializer

    def get_permissions(self):
        """" Define permisos para este recurso """
        if self.action == "create" or self.action == "token":
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    def create(self, request, *args, **kwargs):
        try:
            with transaction.atomic():
                data = request.data
                serializer = BusinessLRegisterSerializer(data=request.data)
                if (serializer.is_valid()):
                    businessLines = BusinessLines.objects.create(
                        name=data.get('name'),
                        description=data.get('description'),
                    )
                    
                    businessLineSerializer = BusinessLSerializer(businessLines)
                    #RegistroBitacora.crearLineaNegocio(request.user, serializer.data)
                    return Response(businessLineSerializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        try:
            instancia = self.get_object()
            anterior = deepcopy(instancia)
            json_anterior = EditBusinessLinesnSerializer(anterior)

            # print("instancia: ", instancia.__dict__)
            data = request.data
            serializer = EditBusinessLinesnSerializer(data=request.data)
            if (serializer.is_valid()):
                instancia.name = data.get('name')
                instancia.description = data.get('description')
                instancia.save()

                #RegistroBitacora.crearLineaNegocio(request.user, serializer.data, json_anterior.data)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        try:
            instancia = self.get_object()
            json = BusinessLRegisterSerializer(instancia)
            #RegistroBitacora.eliminarLineaNegocio(request.user, json.data)
            instancia.is_active = False
            instancia.save()

            return Response("", status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_204_NO_CONTENT)

    @action(detail=False, methods=['get'], url_path='search/(?P<name>[-a-zA-Z0-9_]+)')
    def search(self, request, name):

        business = BusinessLines.objects.filter(
            is_active=True, name__contains=name)
        page = self.paginate_queryset(business)
        serializer = BusinessLSerializer(page, many=True)
        return self.get_paginated_response(serializer.data)
    