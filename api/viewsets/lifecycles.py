""" ViewSet Customer life cycle """

# Django REST framework
from rest_framework import viewsets

# Model
from api.models import LifeCycle

# Serializers
from api.serializers.lifecycles import LifeCycleModelSerializer

class LifeCycleViewSet( viewsets.ReadOnlyModelViewSet ):

    queryset = LifeCycle.objects.all()
    serializer_class = LifeCycleModelSerializer