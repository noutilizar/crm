""" Industries ViewSet """

# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response


# Model
from api.models.industries import Industry

# Serializers
from api.serializers.industries import IndustryModelSerializer, IndustryRegisterSerializer

from django.db import transaction

class IndustryViewSet(viewsets.ModelViewSet):

    queryset = Industry.objects.filter(is_active=True)

    def get_serializer_class(self):
        """Define serializer for API"""
        if self.action == 'list' or self.action == 'retrieve':
            return IndustryModelSerializer
        else:
            return IndustryRegisterSerializer

    def perform_destroy(self, industry):
        """ Desactivate an industry when it is deleted """
        industry.is_active = False
        industry.save()

    @action(detail=False, methods=['get'], url_path='search/(?P<name>[-a-zA-Z0-9_ ]+)')
    def search(self, request, name):

        industries = Industry.objects.filter(is_active=True, name__contains=name)
        page = self.paginate_queryset(industries)
        serializer = self.get_serializer(page, many=True)

        return self.get_paginated_response(serializer.data)
