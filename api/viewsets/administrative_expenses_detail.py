
# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response


# Model
from api.models.administrative_expenses_detail import AdministrativeExpensesDetail
from  api.models.type_project import TypeProject

# Serializers
from api.serializers.administrative_expenses_detail import AdministrativeExpensesDetailList, AdministrativeExpensesDetailCreate

class AdministrativeExpensesDetail(viewsets.ModelViewSet):

    queryset = AdministrativeExpensesDetail.objects.filter(is_active=True)

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrieve':
            return AdministrativeExpensesDetailList
        else:
            return AdministrativeExpensesDetailCreate
