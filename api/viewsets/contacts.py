""" Contact ViewSet """

# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response


# Model
from api.models.contacts import Contact 

# Serializers
from api.serializers.contacts import ContactModelSerializer, ContactReadModelSerializer, ContactRetrieveModelSerializer

class ContactViewSet( viewsets.ModelViewSet ):

    queryset = Contact.objects.filter(is_active=True)

    def perform_destroy(self, contact ):
        """ Desactivate an industry when it is deleted """
        contact.is_active=False
        contact.save()

    def get_serializer_class(self):

        if self.action == 'list':
            return ContactReadModelSerializer
        
        elif self.action == 'retrieve':
            return ContactRetrieveModelSerializer
        
        return ContactModelSerializer
    
    @action(detail=False, methods=['get'], url_path='search/(?P<name>[-a-zA-Z0-9_ ]+)')
    def search(self, request, name ):
        
        contacts = Contact.objects.filter(is_active=True, name__contains=name)
        page = self.paginate_queryset(contacts)
        serializer = ContactReadModelSerializer(page, many=True)

        return self.get_paginated_response(serializer.data)