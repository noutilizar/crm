import json
from django.core.files import File
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters, viewsets
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.settings import api_settings
from api.models import BudgetModules
from api.serializers import BudgetModulesSerializer, BudgetModulesRegistroSerializer


class BudgetModulesViewSet(viewsets.ModelViewSet):
    queryset = BudgetModules.objects.filter(is_active=True)

    # filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    # filter_fields = ("name", "commission_percentage")
    # search_fields = ("name", "commission_percentage")
    # ordering_fields = ("name", "commission_percentage")

    def get_serializer_class(self):
        """Define serializer for API"""
        if self.action == 'list' or self.action == 'retrieve':
            return BudgetModulesSerializer
        else:
            return BudgetModulesRegistroSerializer

    def get_permissions(self):
        """" Define permisos para este recurso """
        if self.action == "create" or self.action == "token":
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]
        

    def create(self, request, *args, **kwargs):
        pass
        # try:
        #     data = request.data
        #     serializer = BudgetModulesRegistroSerializer(data=request.data)
        #     if(serializer.is_valid()):                
        #         BudgetModules.objects.create(
        #             name_modules = data.get('name_modules'),
        #             history_points = data.get('history_points'),
        #             history_points_error = data.get('history_points_error'),
        #             total_history_points = data.get('total_history_points'),
        #             total_start_hours = data.get('total_start_hours'),
        #             total_start_hours_error = data.get('total_start_hours_error'),
        #             total_hours = data.get('total_hours'),
        #             # budget = data.get('budget'),
        #         )                                
        #         return Response(serializer.data, status=status.HTTP_201_CREATED)        
        #     else:
        #         Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        # except Exception as e:
        #     return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        try:
            instancia = self.get_object()
            data = request.data
            serializer = BudgetModulesRegistroSerializer(data=request.data)
            if(serializer.is_valid()):                                
                instancia.name_modules = data.get('name_modules')                
                instancia.history_points = data.get('history_points')                
                instancia.history_points_error = data.get('history_points_error')                
                instancia.total_history_points = data.get('total_history_points')                
                instancia.total_start_hours = data.get('total_start_hours')                
                instancia.total_start_hours_error = data.get('total_start_hours_error')
                instancia.total_hours = data.get('total_hours')
                # instancia.budget = data.get('budget')
                instancia.save()                              
                return Response(serializer.data, status=status.HTTP_201_CREATED)        
            else:
                Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        try: 
            instancia=self.get_object()
            instancia.is_active = False
            instancia.save()
            return Response({'': str(e)}, status=status.HTTP_208_OK)
        except Exception as e:
               return Response({'detail': str(e)}, status=status.HTTP_204_NO_CONTENT)