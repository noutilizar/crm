"""ViewSet Type of Clients"""
# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

#model 
from api.models.type_clients import TypeClient
#Serializer
from api.serializers.type_clients import TypeClientModelSerializer, TypeClientRegisterSerializer
from django.db import transaction
class TypeClientViewSet(viewsets.ModelViewSet):

    queryset=TypeClient.objects.filter(is_active=True)
        
    def get_serializer_class(self):
        """Define serializer for API"""
        if self.action == 'list' or self.action == 'retrieve':
            return TypeClientModelSerializer
        else:
            return TypeClientRegisterSerializer

    def perfom_destroy(self,type_client):
        """ Desactivate an type_client when it is deleted """
        type_client.is_active = False
        type_client.save()

     
    @action(detail=False, methods=['get'], url_path='search/(?P<name_type>[-a-zA-Z0-9_ ]+)')
    def search(self, request, name_type ):
        
        typeclients = TypeClient.objects.filter(is_active=True, name_type__contains=name_type)
        page = self.paginate_queryset(typeclients)
        serializer = self.get_serializer(page, many=True)

        return self.get_paginated_response(serializer.data)
        