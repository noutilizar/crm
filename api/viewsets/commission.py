# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from django.db import transaction
#model 

from api.models.commission import Commission 
from api.models.users import User

#serializers 

from api.serializers.commission import CommissionModelSerializer,CommissionRegisterSerializer

class CommissionViewSet(viewsets.ModelViewSet):

    queryset=Commission.objects.filter(is_active=True)

    def get_serializer_class(self):
        """Define serializer for API"""
        if self.action == 'list' or self.action == 'retrieve':
            return CommissionModelSerializer
        else:
            return CommissionRegisterSerializer

    def perfom_destroy(self,commission):
        """ Desactivate an commission when it is deleted """
        commission.is_active = False
        commission.save()

     
    @action(detail=False, methods=['get'], url_path='search/(?P<name_user>[-a-zA-Z0-9_ ]+)')
    def search(self, request, name_user ):
        
        commissions = Commission.objects.filter(is_active=True, name_user__contains=name_user)
        page = self.paginate_queryset(commissions)
        serializer = self.get_serializer(page, many=True)

        return self.get_paginated_response(serializer.data)

    def create(self, request):
        try:
            with transaction.atomic():
                data=request.data
                commissions=data.get('commissions')
                id_user=data.get('name_user').get('value')
                user=User.objects.get(pk=id_user)


                for commission in commissions:
                    Commission.objects.create(
                        name_user=user,
                        starting_range=commission.get('starting_range'),
                        final_rank=commission.get('final_rank'),
                        percentage_commission=commission.get('percentage_commission'),
                        name_type=commission.get('name_type')
                    )
            
            return Response({'detail':'permisos asociados correctamente'}, status=status.HTTP_200_OK)
            
        except Exception as e:
            return Response({'detail':str(e)}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['put'])
    def actualizar(self,request):

        try:
            data=request.data
            print("probando actualizar backend ",data)
            commissions=data.get('commissions')
            print("Nombre usuario",data.get('name_user'))
            id_user=data.get('name_user').get('value')
            print("Id Usuario",id_user)
            user=User.objects.get(pk=id_user)
            print("Id Usuario",id_user)
            user.commissions_users.all().delete()

            for commission in commissions:
                Commission.objects.create(
                    name_user=user,
                    starting_range=commission.get('starting_range'),
                    final_rank=commission.get('final_rank'),
                    percentage_commission=commission.get('percentage_commission'),
                    name_type=commission.get('name_type')
                )
                
            return Response({'detail':'permisos asociados correctamente'}, status=status.HTTP_200_OK)

        except Exception as e:
            return Response({'detail':str(e)},status=status.HTTP_400_BAD_REQUEST)
            
            

    @action(detail=False, methods=['get'])
    def get_permissions_user(self,request):
        try:
            id_user=request.query_params.get('user')
            name_user=User.objects.get(pk=id_user)
            commissions=Commission.objects.filter(
                name_user=id_user
            )
            serializer=CommissionModelSerializer(commissions,many=True)

            return Response(serializer.data,status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'detail':str(e)}, status=status.HTTP_400_BAD_REQUEST)

        
    



        