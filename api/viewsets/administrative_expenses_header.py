
# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from django.db import transaction

# Model
from api.models.administrative_expenses_header import AdministrativeExpensesHeader as AdministrativeExpensesHeaderModel
from  api.models.type_project import TypeProject
from api.models.administrative_expenses_detail import AdministrativeExpensesDetail

# Serializers
from api.serializers.administrative_expenses_header import AdministrativeExpensesHeaderList, AdministrativeExpensesHeaderCreate
from api.serializers.administrative_expenses_detail import AdministrativeExpensesDetailList

class AdministrativeExpensesHeader(viewsets.ModelViewSet):
    
    
    queryset = AdministrativeExpensesHeaderModel.objects.filter(is_active=True)
    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrieve':
            return AdministrativeExpensesHeaderList
        else:
            return AdministrativeExpensesHeaderCreate

    def update(self, request, pk):
        administrative_expense_header = AdministrativeExpensesHeaderModel.objects.get(pk=pk)
        administrative_expense_header.modules.update(is_active=False)
        try:
            data = request.data
            with transaction.atomic():
                id_tipoProyecto = data.get('type_project').get('value')
                type_project = TypeProject.objects.get(pk =id_tipoProyecto)
                administrative_expense_header.type_project = type_project
                for module in data.get('modules'):
                    AdministrativeExpensesDetail.objects.create(
                        administrative_expenses_header = administrative_expense_header,
                        name_expense = module.get('name_expense'),
                        number_hours = module.get('number_hours'),
                        total_quantity = module.get('total_quantity'),
                        rate_hour  = module.get('rate_hour'),
                        total_hours = module.get('total_hours'),
                    )

                administrative_expense_header.save()
                serializer = AdministrativeExpensesHeaderCreate(administrative_expense_header)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
          return Response({"detail": str(e)}, status= status.HTTP_400_BAD_REQUEST)

    



    def create(self, request):
        try:
            data = request.data
            with transaction.atomic():
                id_tipoProyecto = data.get('type_project').get('value')
                type_project = TypeProject.objects.get(pk=id_tipoProyecto)
                header = AdministrativeExpensesHeaderModel.objects.create(
                        type_project = type_project
                    )
                for module in data.get('modules'):
                    AdministrativeExpensesDetail.objects.create(
                        administrative_expenses_header = header,
                        name_expense = module.get('name_expense'),
                        number_hours = module.get('number_hours'),
                        total_quantity = module.get('total_quantity'),
                        rate_hour  = module.get('rate_hour'),
                        total_hours = module.get('total_hours'),
                    )
                serializer = AdministrativeExpensesHeaderCreate(header)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({"detail": str(e)}, status= status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'])
    def get_gastos(self, request):
        try:
            data =request.query_params
            type_project_id = data.get('type_project_id')
            type_project = TypeProject.objects.get(pk=type_project_id)
            queryset = AdministrativeExpensesDetail.objects.filter(
                administrative_expenses_header__type_project = type_project
            )
            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = AdministrativeExpensesDetailList(page, many=True)
                return self.get_paginated_response(serializer.data)
        except Exception as e:
            print(str(e))
            return Response({"detail": str(e)}, status= status.HTTP_400_BAD_REQUEST)

 