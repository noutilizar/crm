""" Vendedor ViewSet """
# Django REST framework
from rest_framework import viewsets, status
from rest_framework.response import Response


# Model
from api.models.vendedor import Vendedor

# Serializers
from api.serializers.vendedor import VendedorCreate, VendedorList

class VendedorViewSet(viewsets.ModelViewSet):

    queryset = Vendedor.objects.filter()

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrieve':
            return VendedorList
        else:
            return VendedorCreate

    def perform_destroy(self, vendedor ):
        vendedor.is_active=False
        vendedor.save()