# Django
from django_filters.rest_framework import DjangoFilterBackend
from django.db import transaction

from copy import deepcopy

# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status, filters, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
# Model
from api.models import StageSales, SalesFunnel

# Serializers
from api.serializers import StageSalesRegistroSerializer, StageSalesSerializer


class StageSalesViewSet(viewsets.ModelViewSet):

    queryset = StageSales.objects.filter(is_active=True)

    filter_backends = (DjangoFilterBackend,
                       filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ("name",)
    search_fields = ("name",)
    ordering_fields = ("name",)

    def get_serializer_class(self):
        """Define serializer for API"""
        if self.action == 'list' or self.action == 'retrieve':
            return StageSalesSerializer
        else:
            return StageSalesRegistroSerializer

    def get_permissions(self):
        """" Define permisos para este recurso """
        if self.action == "create" or self.action == "token":
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    def perform_destroy(self, stageSale):
        """ Desactivate a Sales Funnel when it is deleted """
        stageSale.is_active = False
        stageSale.save()
