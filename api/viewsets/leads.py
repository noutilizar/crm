""" Lead ViewSet """
# json
import json

from django.db.models import Sum, Avg, Count, F
from datetime import datetime
from django.db.models.functions import Cast, ExtractDay, TruncDate
from datetime import datetime

# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response


# Model
from api.models import Leads, StageSales

# Serializers
from api.serializers.leads import LeadModelSerializer, LeadReadSerializer


class LeadViewSet(viewsets.ModelViewSet):

    queryset = Leads.objects.filter(is_active=True)
    # serializer_class = LeadModelSerializer

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return LeadReadSerializer
        return LeadModelSerializer

    def create(self, request, *args, **kwargs):
        stage = request.data.get('business_stage')

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        lead = serializer.save()
        # agregando el lead a la etapa de venta
        stage_sale = StageSales.objects.get(pk=stage)
        leads_ordering = json.loads(stage_sale.leads_ordering)
        leads_ordering.append(lead.id)
        stage_sale.leads_ordering = json.dumps(leads_ordering)
        stage_sale.save()
        return Response(status=status.HTTP_201_CREATED)

    def update(self, request, pk=None):

        lead = self.get_object()
        stage = request.data.get('business_stage')

        serializer = self.get_serializer(lead, data=request.data)
        serializer.is_valid(raise_exception=True)

        stage_sale_ant = lead.business_stage

        # Verifica si se cambio de etapa de negocio
        if stage_sale_ant.id != stage:
            stage_sale_ac = StageSales.objects.get(pk=stage)

            # La cadena se convierte a un array
            leads_ordering_an = json.loads(stage_sale_ant.leads_ordering)
            leads_ordering_ac = json.loads(stage_sale_ac.leads_ordering)

            # el lead se cambia de etapa de negocio
            leads_ordering_an.remove(lead.id)
            leads_ordering_ac.append(lead.id)
            stage_sale_ant.leads_ordering = json.dumps(leads_ordering_an)
            stage_sale_ac.leads_ordering = json.dumps(leads_ordering_ac)
            stage_sale_ant.save()
            stage_sale_ac.save()

        serializer.save()

        return Response(status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):

        lead = self.get_object()

        # Se elimina el lead en la etapa de negocio
        stage_sale = lead.business_stage
        leads_ordering = json.loads(stage_sale.leads_ordering)
        leads_ordering.remove(lead.id)
        stage_sale.leads_ordering = json.dumps(leads_ordering)
        stage_sale.save()

        lead.is_active = False
        lead.save()

        return Response(status=status.HTTP_200_OK)

    # Lista los leads segun los filtros que se aplican
    @action(detail=False, methods=['get'])
    def filterFunnel(self, request):
        
        # Obtiene los leads segun el embudo de venta
        query = Leads.objects.filter(
            sales_funnel=request.GET.get('embudoId'), 
            is_active=True)

        # Condicion que obtiene los leads o no segun el usuario
        if request.GET.get("user_id", 0) != 0:
            user_id = request.GET.get('user_id')
            query = query.filter(owner_lead=user_id)

        # Condicion que obtiene o no los leads segun el canal de venta
        if request.GET.get("canal_id", 0) != 0:
            canal_id = request.GET['canal_id']
            query = query.filter(sales_channel=canal_id)

        # Condicion que obtiene o no los leads segun el rango de quetzales
        if request.GET.get("option") == "amount":
            if request.GET.get("data_initial") and request.GET.get("data_finish"):
                data_initial = request.GET.get("data_initial")
                data_finish = request.GET.get("data_finish")
                query = query.filter(
                    quetzal__range=[data_initial, data_finish])
        
        # Condicion que obtiene o no los leads segun el rango de dias de seguimiento
        if request.GET.get("option") == "days":
            if request.data.get("data_initial") and request.GET.get("data_finish"):
                data_initial = request.GET.get("data_initial")
                data_finish = request.GET.get("data_finish")
                query = query.filter(
                    cycle_days__range=[data_initial, data_finish])

        page = self.paginate_queryset(query)
        serializer = LeadReadSerializer(page, many=True)

        return self.get_paginated_response(serializer.data)
    
    # Obtiene la cantidad de lead que tiene cada etapa
    @action(detail=False, methods=['post'])
    def quantityStages(self, request):
        funnel = request.data
        data = []
        for t in StageSales.objects.filter(sale_funnel=request.data, is_active=True):
            quantity = Leads.objects.filter(business_stage= t.id, is_active=True).aggregate(Count('name'))
            data.append({
                'name': t.name,
                'total': quantity['name__count']
            })    
        print(data)

        return Response({
            'etapas': data
        }, status=status.HTTP_200_OK)

    # Obtiene las estadisticas en base a los lead que se filtran
    @action(detail=False, methods=['post'])
    def statsReports(self, request):

        funnel = request.data.get('embudoId')
        stage_won = StageSales.objects.get(sale_funnel=funnel, name='Ganado')
        stage_lost = StageSales.objects.get(sale_funnel=funnel, name='Perdido')

        # Obtiene los leads segun el embudo de venta
        lead_total = Leads.objects.filter(
            is_active=True,
            sales_funnel=funnel)

        # Obtiene los leads segun el embudo de venta y la etapa "Ganado"
        lead_won = Leads.objects.filter(
            is_active=True,
            sales_funnel=funnel,
            business_stage=stage_won.id)
        
        # Obtiene los leads segun el embudo de venta y la etapa "Perdido"
        lead_lost = Leads.objects.filter(
            is_active=True,
            sales_funnel=funnel,
            business_stage=stage_lost.id)

        # Condicion para filtar o no por usuario
        if request.data.get("user_id", 0) != 0:
            user = request.data.get('user_id')
            lead_total = lead_total.filter(owner_lead=user)
            lead_won = lead_won.filter(owner_lead=user)
            lead_lost = lead_lost.filter(owner_lead=user)

        # Condicion para filtar o no por canal
        if request.data.get("canal_id", 0) != 0:
            canal = request.data.get('canal_id')
            lead_total = lead_total.filter(sales_channel=canal)
            lead_won = lead_won.filter(sales_channel=canal)
            lead_lost = lead_lost.filter(sales_channel=canal)

        # Condicion para filtar o no por quetzales
        if request.data.get("option") == "amount":
            if request.data.get("data_initial") and request.data.get("data_finish"):
                data_initial = request.data.get("data_initial")
                data_finish = request.data.get("data_finish")
                lead_total = lead_total.filter(
                    quetzal__range=[data_initial, data_finish])
                lead_won = lead_won.filter(
                    quetzal__range=[data_initial, data_finish])
                lead_lost = lead_lost.filter(
                    quetzal__range=[data_initial, data_finish])

        # Condicion para filtar o no por dias de seguimiento
        if request.data.get("option") == "days":
            if request.data.get("data_initial") and request.data.get("data_finish"):
                data_initial = request.data['data_initial']
                data_finish = request.data['data_finish']
                lead_total = lead_total.filter(
                    cycle_days__range=[data_initial, data_finish])
                lead_won = lead_won.filter(
                    cycle_days__range=[data_initial, data_finish])
                lead_lost = lead_lost.filter(
                    cycle_days__range=[data_initial, data_finish])


        lead_total = lead_total.aggregate(
            monto_total=Sum('quetzal'), count=Count('quetzal'))
        lead_won = lead_won.aggregate(
            monto_stage=Sum('quetzal'), count=Count('quetzal'), days_average=Avg('cycle_days'))
        lead_lost = lead_lost.aggregate(
            days_average=Avg('cycle_days'), count=Count('amount'))

        if lead_won['count'] != 0:
            percentage_count = (lead_won['count'] / lead_total['count'])*100
            percentage_money = (lead_won['monto_stage'] / lead_total['monto_total'])*100
            won_days_average = round(lead_won['days_average'],0)

            percentage_count = round(percentage_count, 2)
            percentage_money = round(percentage_money, 2)
            print('Hay leads ganados')
        else:
            percentage_count = 0
            percentage_money = 0
            won_days_average = 0
            print('No hay lead ganados')
        
        if lead_lost['count'] != 0:
            lost_days_average = round(lead_lost['days_average'],0)
        else:
            lost_days_average = 0

        return Response({
            'monto_total': "{:,.2f}".format(lead_total['monto_total']),
            'percentage_count': percentage_count,
            'percentage_money': percentage_money,
            'won_days_average': won_days_average,
            'lost_days_average': lost_days_average
        }, status=status.HTTP_200_OK)
