""" Budget ViewSet """
# Django REST framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from django.db import transaction


# Model
from api.models.budgets import Budget
from api.models.vendedor import Vendedor
from api.models.sales_channel import SalesChannel
from api.models.type_project import TypeProject
from api.models.type_clients import TypeClient
from api.models.budget_modules import BudgetModules
from api.models.configurations import Configuration
from api.models.administrative_expenses import AdministrativeExpenses
from api.models.budget_relation_module import BudgetRelationModule
from api.models.administrative_expenses_header import AdministrativeExpensesHeader
from api.models.administrative_expenses_detail import AdministrativeExpensesDetail

# Serializers
from api.serializers.budgets import BudgetCreate, BudgetList, BudgetCreateAll


class BudgetViewSet(viewsets.ModelViewSet):

    queryset = Budget.objects.filter()

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrieve':
            return BudgetList
        else:
            return BudgetCreateAll

    def create(self, request):
        try:
            data = request.data
            modules = data["modules"]
            modulesAdmin = data["adminmodules"]
            module = modules
            mod = data.pop('modules')
            data = data["budget"]
            vendedor_new = data.get("vendedor").get("value")
            canal_venta = data.get("canal_venta").get("value")
            tipo_cliente = data.get("tipo_cliente").get("value")
            tipo_proyecto = data.get("tipo_proyecto").get("value")
            vendedor_new = Vendedor.objects.get(pk=vendedor_new)
            canal_venta = SalesChannel.objects.get(pk=canal_venta)
            tipo_cliente = TypeClient.objects.get(pk=tipo_cliente)
            tipo_proyecto = TypeProject.objects.get(pk=tipo_proyecto)
            """"CREACION DE BUDGET"""
            budget = Budget.objects.create(
                tipo_proyecto=tipo_proyecto,
                vendedor=vendedor_new,
                tipo_cliente=tipo_cliente,
                canal_venta=canal_venta,
                cantidad_programadores=data.get("cantidad_programadores"),
                cantidad_diseñadores=data.get("cantidad_diseñadores"),
                porcentaje_error=data.get("porcentaje_error"),
                precio_nominal=data.get("precio_nominal"),
                porcentaje_comisionCanalVenta=canal_venta.commission_percentage,
                porcentaje_comisionVendedor=vendedor_new.vendedor_percentage,
            )
            """PROMEDIO DE SPRINT"""
            sprint = tipo_proyecto.average_sprint
            """ CREACION DE MODULOS"""
            # for module in modules:
            if(type(modules) == list):
                if(len(modules) == 0):
                    pass
                else:
                    with transaction.atomic():
                        for module in modules:
                            BudgetRelationModule.objects.create(
                                budget=budget,
                                name_modules=module['name_modules'],
                                history_points=module['history_points'],
                                history_points_error=module['history_points_error'],
                                total_history_points=module['total_history_points'],
                                total_start_hours=module['total_start_hours'],
                                total_start_hours_error=module['total_start_hours_error'],
                                total_hours=module['total_hours'],)
            else:
                BudgetRelationModule.objects.create(
                    budget=budget,
                    name_modules=modules['name_modules'],
                    history_points=modules['history_points'],
                    history_points_error=modules['history_points_error'],
                    total_history_points=modules['total_history_points'],
                    total_start_hours=modules['total_start_hours'],
                    total_start_hours_error=modules['total_start_hours_error'],
                    total_hours=modules['total_hours'],)

            """CREACION DE ADMIN MODULES"""
            # administrative_expense_header = AdministrativeExpensesHeader.objects.create(
            #             type_project = tipo_proyecto
            #         )   
            administrative_expense_header = AdministrativeExpensesHeader.objects.get(type_project = tipo_proyecto)          
            if(type(modulesAdmin) == list):
                if(len(modulesAdmin) == 0):
                    pass
                else:
                    with transaction.atomic():
                        for module in modulesAdmin:
                            AdministrativeExpenses.objects.create(
                            administrative_expenses_header = administrative_expense_header,
                            name_expense = module.get('name_expense'),
                            number_hours = module.get('number_hours'),
                            total_quantity = module.get('total_quantity'),
                            rate_hour  = module.get('rate_hour'),
                            total_hours = module.get('total_hours'),
                            budget = budget,
                             )

            else:
                AdministrativeExpenses.objects.create(
                        administrative_expenses_header = administrative_expense_header,
                        name_expense = modulesAdmin.get('name_expense'),
                        number_hours = modulesAdmin.get('number_hours'),
                        total_quantity = modulesAdmin.get('total_quantity'),
                        rate_hour  = modulesAdmin.get('rate_hour'),
                        total_hours = modulesAdmin.get('total_hours'),
                        budget = budget,
                        )
       

            """TOTAL DE PUNTOS DE HISTORIA"""
            pk_object = budget.pk
            objetos = BudgetRelationModule.objects.filter(budget=pk_object)
            cantidad_modules = len(objetos)
            if(cantidad_modules > 0):
                if(cantidad_modules == 1):
                    puntos_historia = objetos[0].total_history_points
                    puntos_historiaerror = objetos[0].history_points_error
                else:
                    puntos_historia = 0
                    puntos_historiaerror = 0
                    for point in objetos:
                        puntos_historia = + point.total_history_points
                        puntos_historiaerror = + point.history_points_error
            else:
                puntos_historia = 0
                puntos_historiaerror = 0
            
            """CONFIGURACION DE SEMANAS Y HORAS"""
            configuracion = Configuration.objects.all().first()
            config_semanasmes = configuracion.week_for_month
            config_horasemana = configuracion.hour_for_week
            """TOTAL TIEMPO ESTIMADO"""
            tiempo_total = puntos_historia/sprint
            budget.tiempo_total = tiempo_total
            """SEMANAS DEL PROYECTO"""
            semanas_proyecto = (
                tiempo_total)/(int(budget.cantidad_programadores) + int(budget.cantidad_diseñadores))
            budget.semanas_proyecto = semanas_proyecto
            """MESES DEL PROYECTO """
            meses_proyecto = semanas_proyecto / config_semanasmes
            budget.meses_proyecto = meses_proyecto
            """TOTAL HORAS PROYECTO -----------------------------------------------------------"""
            total_horasProyecto = semanas_proyecto * config_horasemana
            budget.total_horasProyecto = total_horasProyecto
            """TOTAL SEMANAS MARGEN ERROR """
            total_semanasMargenError = puntos_historiaerror/sprint
            budget.total_semanasMargenError = total_semanasMargenError
            """TOTAL HORAS MARGEN ERROR """
            total_horasMargenError = total_semanasMargenError / config_horasemana
            budget.total_horasMargenError = total_horasMargenError
            """PRECIO BASE DESARROLLO------------------------------------------------------- """
            precio_nominal = float(budget.precio_nominal)
            precio_baseDesarrollo = precio_nominal*meses_proyecto
            budget.precio_baseDesarrollo = precio_baseDesarrollo



            # """GASTOS ADMIN """
            objetos2 = AdministrativeExpensesHeader.objects.filter(type_project=budget.tipo_proyecto)
            cantidad_modules = len(objetos2)
            gastos_adminitrativos =0
            total_horasAdministrativas=0
            
            if(cantidad_modules>0):
                
                if(cantidad_modules == 1):
                    obj_detail = AdministrativeExpensesDetail.objects.filter(administrative_expenses_header = objetos2[0].pk)
                    for module in obj_detail:
                        gastos_adminitrativos = + module.total_quantity
                        total_horasAdministrativas = + module.total_hours
                        print("----------------", module.total_quantity, module.total_hours) 
                else:         
                    for point in objetos2:
                        obj_detail = AdministrativeExpensesDetail.objects.filter(administrative_expenses_header = point.pk)
                        for module in obj_detail:
                            gastos_adminitrativos = + module.total_quantity
                            total_horasAdministrativas = + module.total_hours
                            print("----------------", module.total_quantity, module.total_hours) 
            else:
                gastos_adminitrativos =0
                total_horasAdministrativas=0                              
            print("ssssssssssssssssssssssssssssssssssss", objetos2)

            budget.gastos_adminitrativos=  gastos_adminitrativos
            """TOTAL HORAS ADMIN """
            budget.total_horasAdministrativas=  total_horasAdministrativas
     

            """TOTAL SEMANAS ADMIN-------------------------------------- --------------------"""
            if(semanas_proyecto == 0):
                total_semanasAdministrativas = 0
            else:
                # total_semanasAdministrativas = 0
                total_semanasAdministrativas= total_horasAdministrativas/semanas_proyecto
            print("aaaaafdsfsddddddfff-------------------------------")
            budget.total_semanasAdministrativas = total_semanasAdministrativas
            """DURACION TOTAL PROYECTO """
            duracion_totalProyecto = semanas_proyecto + total_semanasMargenError + total_semanasAdministrativas
            budget.duracion_totalProyecto = duracion_totalProyecto
            """TOTAL COMISION CANAL """
            total_comisionCanal = precio_baseDesarrollo*canal_venta.commission_percentage
            budget.total_comisionCanal = total_comisionCanal
            """TOTAL COMISION VENDEDOR """
            total_comisionVendedor = precio_baseDesarrollo*vendedor_new.vendedor_percentage
            budget.total_comisionVendedor = total_comisionVendedor
            """PRECIO TOTAL """
            precio_total = precio_baseDesarrollo + gastos_adminitrativos + total_comisionVendedor+total_comisionCanal
            budget.precio_total = precio_total
            budget.save()

            serializer = BudgetCreateAll(budget)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({"detail": str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk):
        budget = Budget.objects.get(pk=pk)
        budget.budget_relation_modules.update(is_active=False)
        budget.adminmodules.update(is_active=False)
        try:
            data = request.data
            modules = data["modules"]
            modulesAdmin = data["adminmodules"]
            module = modules
            mod = data.pop('modules')
            data = data["budget"]
            vendedor_new = data.get("vendedor").get("value")
            canal_venta = data.get("canal_venta").get("value")
            tipo_cliente = data.get("tipo_cliente").get("value")
            tipo_proyecto = data.get("tipo_proyecto").get("value")
            vendedor_new = Vendedor.objects.get(pk=vendedor_new)
            canal_venta = SalesChannel.objects.get(pk=canal_venta)
            tipo_cliente = TypeClient.objects.get(pk=tipo_cliente)
            tipo_proyecto = TypeProject.objects.get(pk=tipo_proyecto)
            """"CREACION DE BUDGET"""
            budget = Budget.objects.create(
                tipo_proyecto=tipo_proyecto,
                vendedor=vendedor_new,
                tipo_cliente=tipo_cliente,
                canal_venta=canal_venta,
                cantidad_programadores=data.get("cantidad_programadores"),
                cantidad_diseñadores=data.get("cantidad_diseñadores"),
                porcentaje_error=data.get("porcentaje_error"),
                precio_nominal=data.get("precio_nominal"),
                porcentaje_comisionCanalVenta=canal_venta.commission_percentage,
                porcentaje_comisionVendedor=vendedor_new.vendedor_percentage,
            )
            """PROMEDIO DE SPRINT"""
            sprint = tipo_proyecto.average_sprint
            """ CREACION DE MODULOS"""
            # for module in modules:
            if(type(modules) == list):
                if(len(modules) == 0):
                    pass
                else:
                    with transaction.atomic():
                        for module in modules:
                            BudgetRelationModule.objects.create(
                                budget=budget,
                                name_modules=module['name_modules'],
                                history_points=module['history_points'],
                                history_points_error=module['history_points_error'],
                                total_history_points=module['total_history_points'],
                                total_start_hours=module['total_start_hours'],
                                total_start_hours_error=module['total_start_hours_error'],
                                total_hours=module['total_hours'],)
            else:
                BudgetRelationModule.objects.create(
                    budget=budget,
                    name_modules=modules['name_modules'],
                    history_points=modules['history_points'],
                    history_points_error=modules['history_points_error'],
                    total_history_points=modules['total_history_points'],
                    total_start_hours=modules['total_start_hours'],
                    total_start_hours_error=modules['total_start_hours_error'],
                    total_hours=modules['total_hours'],)

            """CREACION DE ADMIN MODULES"""
            # administrative_expense_header = AdministrativeExpensesHeader.objects.create(
            #             type_project = tipo_proyecto
            #         )   
            administrative_expense_header = AdministrativeExpensesHeader.objects.get(type_project = tipo_proyecto)          
            if(type(modulesAdmin) == list):
                if(len(modulesAdmin) == 0):
                    pass
                else:
                    with transaction.atomic():
                        for module in modulesAdmin:
                            AdministrativeExpenses.objects.create(
                            administrative_expenses_header = administrative_expense_header,
                            name_expense = module.get('name_expense'),
                            number_hours = module.get('number_hours'),
                            total_quantity = module.get('total_quantity'),
                            rate_hour  = module.get('rate_hour'),
                            total_hours = module.get('total_hours'),
                            budget = budget,
                             )

            else:
                AdministrativeExpenses.objects.create(
                        administrative_expenses_header = administrative_expense_header,
                        name_expense = modulesAdmin.get('name_expense'),
                        number_hours = modulesAdmin.get('number_hours'),
                        total_quantity = modulesAdmin.get('total_quantity'),
                        rate_hour  = modulesAdmin.get('rate_hour'),
                        total_hours = modulesAdmin.get('total_hours'),
                        budget = budget,
                        )
       

            """TOTAL DE PUNTOS DE HISTORIA"""
            pk_object = budget.pk
            objetos = BudgetRelationModule.objects.filter(budget=pk_object)
            cantidad_modules = len(objetos)
            if(cantidad_modules > 0):
                if(cantidad_modules == 1):
                    puntos_historia = objetos[0].total_history_points
                    puntos_historiaerror = objetos[0].history_points_error
                else:
                    puntos_historia = 0
                    puntos_historiaerror = 0
                    for point in objetos:
                        puntos_historia = + point.total_history_points
                        puntos_historiaerror = + point.history_points_error
            else:
                puntos_historia = 0
                puntos_historiaerror = 0
            
            """CONFIGURACION DE SEMANAS Y HORAS"""
            configuracion = Configuration.objects.all().first()
            config_semanasmes = configuracion.week_for_month
            config_horasemana = configuracion.hour_for_week
            """TOTAL TIEMPO ESTIMADO"""
            tiempo_total = puntos_historia/sprint
            budget.tiempo_total = tiempo_total
            """SEMANAS DEL PROYECTO"""
            semanas_proyecto = (
                tiempo_total)/(int(budget.cantidad_programadores) + int(budget.cantidad_diseñadores))
            budget.semanas_proyecto = semanas_proyecto
            """MESES DEL PROYECTO """
            meses_proyecto = semanas_proyecto / config_semanasmes
            budget.meses_proyecto = meses_proyecto
            """TOTAL HORAS PROYECTO -----------------------------------------------------------"""
            total_horasProyecto = semanas_proyecto * config_horasemana
            budget.total_horasProyecto = total_horasProyecto
            """TOTAL SEMANAS MARGEN ERROR """
            total_semanasMargenError = puntos_historiaerror/sprint
            budget.total_semanasMargenError = total_semanasMargenError
            """TOTAL HORAS MARGEN ERROR """
            total_horasMargenError = total_semanasMargenError / config_horasemana
            budget.total_horasMargenError = total_horasMargenError
            """PRECIO BASE DESARROLLO------------------------------------------------------- """
            precio_nominal = float(budget.precio_nominal)
            precio_baseDesarrollo = precio_nominal*meses_proyecto
            budget.precio_baseDesarrollo = precio_baseDesarrollo



            # """GASTOS ADMIN """
            objetos2 = AdministrativeExpensesHeader.objects.filter(type_project=budget.tipo_proyecto)
            cantidad_modules = len(objetos2)
            gastos_adminitrativos =0
            total_horasAdministrativas=0
            
            if(cantidad_modules>0):
                
                if(cantidad_modules == 1):
                    obj_detail = AdministrativeExpensesDetail.objects.filter(administrative_expenses_header = objetos2[0].pk)
                    for module in obj_detail:
                        gastos_adminitrativos = + module.total_quantity
                        total_horasAdministrativas = + module.total_hours
                        print("----------------", module.total_quantity, module.total_hours) 
                else:         
                    for point in objetos2:
                        obj_detail = AdministrativeExpensesDetail.objects.filter(administrative_expenses_header = point.pk)
                        for module in obj_detail:
                            gastos_adminitrativos = + module.total_quantity
                            total_horasAdministrativas = + module.total_hours
                            print("----------------", module.total_quantity, module.total_hours) 
            else:
                gastos_adminitrativos =0
                total_horasAdministrativas=0                              
            print("ssssssssssssssssssssssssssssssssssss", objetos2)

            budget.gastos_adminitrativos=  gastos_adminitrativos
            """TOTAL HORAS ADMIN """
            budget.total_horasAdministrativas=  total_horasAdministrativas
     

            """TOTAL SEMANAS ADMIN-------------------------------------- --------------------"""
            if(semanas_proyecto == 0):
                total_semanasAdministrativas = 0
            else:
                # total_semanasAdministrativas = 0
                total_semanasAdministrativas= total_horasAdministrativas/semanas_proyecto
            print("aaaaafdsfsddddddfff-------------------------------")
            budget.total_semanasAdministrativas = total_semanasAdministrativas
            """DURACION TOTAL PROYECTO """
            duracion_totalProyecto = semanas_proyecto + total_semanasMargenError + total_semanasAdministrativas
            budget.duracion_totalProyecto = duracion_totalProyecto
            """TOTAL COMISION CANAL """
            total_comisionCanal = precio_baseDesarrollo*canal_venta.commission_percentage
            budget.total_comisionCanal = total_comisionCanal
            """TOTAL COMISION VENDEDOR """
            total_comisionVendedor = precio_baseDesarrollo*vendedor_new.vendedor_percentage
            budget.total_comisionVendedor = total_comisionVendedor
            """PRECIO TOTAL """
            precio_total = precio_baseDesarrollo + gastos_adminitrativos + total_comisionVendedor+total_comisionCanal
            budget.precio_total = precio_total
            budget.save()

            serializer = BudgetCreateAll(budget)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({"detail": str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk):
        budget = Budget.objects.get(pk=pk)
        budget.budget_relation_modules.update(is_active=False)
        budget.adminmodules.update(is_active=False)
        try:
            data = request.data
            modules = data["modules"]
            modulesAdmin = data["adminmodules"]
            module = modules
            mod = data.pop('modules')
            data = data["budget"]
            vendedor_new = data.get("vendedor").get("value")
            canal_venta = data.get("canal_venta").get("value")
            tipo_cliente = data.get("tipo_cliente").get("value")
            tipo_proyecto = data.get("tipo_proyecto").get("value")
            vendedor_new = Vendedor.objects.get(pk=vendedor_new)
            canal_venta = SalesChannel.objects.get(pk=canal_venta)
            tipo_cliente = TypeClient.objects.get(pk=tipo_cliente)
            tipo_proyecto = TypeProject.objects.get(pk=tipo_proyecto)
            """"CREACION DE BUDGET"""
            budget.tipo_proyecto=tipo_proyecto
            budget.vendedor=vendedor_new
            budget.tipo_cliente=tipo_cliente
            budget.canal_venta=canal_venta
            budget.cantidad_programadores=data.get("cantidad_programadores")
            budget.cantidad_diseñadores=data.get("cantidad_diseñadores")
            budget.porcentaje_error=data.get("porcentaje_error")
            budget.precio_nominal=data.get("precio_nominal")
            budget.porcentaje_comisionCanalVenta=canal_venta.commission_percentage
            budget.porcentaje_comisionVendedor=vendedor_new.vendedor_percentage
            """PROMEDIO DE SPRINT"""
            sprint = tipo_proyecto.average_sprint
            print("----------------SIPASO")
            """ CREACION DE MODULOS"""
            # for module in modules:
            if(type(modules) == list):
                if(len(modules) == 0):
                    pass
                else:
                    with transaction.atomic():
                        for module in modules:
                            BudgetRelationModule.objects.create(
                                budget=budget,
                                name_modules=module['name_modules'],
                                history_points=module['history_points'],
                                history_points_error=module['history_points_error'],
                                total_history_points=module['total_history_points'],
                                total_start_hours=module['total_start_hours'],
                                total_start_hours_error=module['total_start_hours_error'],
                                total_hours=module['total_hours'],)
            else:
                BudgetRelationModule.objects.create(
                    budget=budget,
                    name_modules=modules['name_modules'],
                    history_points=modules['history_points'],
                    history_points_error=modules['history_points_error'],
                    total_history_points=modules['total_history_points'],
                    total_start_hours=modules['total_start_hours'],
                    total_start_hours_error=modules['total_start_hours_error'],
                    total_hours=modules['total_hours'],)

            """CREACION DE ADMIN MODULES"""
            administrative_expense_header = AdministrativeExpensesHeader.objects.get(type_project = tipo_proyecto)          
            if(type(modulesAdmin) == list):
                if(len(modulesAdmin) == 0):
                    pass
                else:
                    with transaction.atomic():
                        for module in modulesAdmin:
                            AdministrativeExpenses.objects.create(
                            administrative_expenses_header = administrative_expense_header,
                            name_expense = module.get('name_expense'),
                            number_hours = module.get('number_hours'),
                            total_quantity = module.get('total_quantity'),
                            rate_hour  = module.get('rate_hour'),
                            total_hours = module.get('total_hours'),
                            budget = budget,
                             )

            else:
                AdministrativeExpenses.objects.create(
                        administrative_expenses_header = administrative_expense_header,
                        name_expense = modulesAdmin.get('name_expense'),
                        number_hours = modulesAdmin.get('number_hours'),
                        total_quantity = modulesAdmin.get('total_quantity'),
                        rate_hour  = modulesAdmin.get('rate_hour'),
                        total_hours = modulesAdmin.get('total_hours'),
                        budget = budget,
                        )
       

            """TOTAL DE PUNTOS DE HISTORIA"""
            pk_object = budget.pk
            objetos = BudgetRelationModule.objects.filter(budget=pk_object)
            cantidad_modules = len(objetos)
            if(cantidad_modules > 0):
                if(cantidad_modules == 1):
                    puntos_historia = objetos[0].total_history_points
                    puntos_historiaerror = objetos[0].history_points_error
                else:
                    puntos_historia = 0
                    puntos_historiaerror = 0
                    for point in objetos:
                        puntos_historia = + point.total_history_points
                        puntos_historiaerror = + point.history_points_error
            else:
                puntos_historia = 0
                puntos_historiaerror = 0
            
            """CONFIGURACION DE SEMANAS Y HORAS"""
            configuracion = Configuration.objects.all().first()
            config_semanasmes = configuracion.week_for_month
            config_horasemana = configuracion.hour_for_week
            """TOTAL TIEMPO ESTIMADO"""
            tiempo_total = puntos_historia/sprint
            budget.tiempo_total = tiempo_total
            """SEMANAS DEL PROYECTO"""
            semanas_proyecto = (
                tiempo_total)/(int(budget.cantidad_programadores) + int(budget.cantidad_diseñadores))
            budget.semanas_proyecto = semanas_proyecto
            """MESES DEL PROYECTO """
            meses_proyecto = semanas_proyecto / config_semanasmes
            budget.meses_proyecto = meses_proyecto
            """TOTAL HORAS PROYECTO -----------------------------------------------------------"""
            total_horasProyecto = semanas_proyecto * config_horasemana
            budget.total_horasProyecto = total_horasProyecto
            """TOTAL SEMANAS MARGEN ERROR """
            total_semanasMargenError = puntos_historiaerror/sprint
            budget.total_semanasMargenError = total_semanasMargenError
            """TOTAL HORAS MARGEN ERROR """
            total_horasMargenError = total_semanasMargenError / config_horasemana
            budget.total_horasMargenError = total_horasMargenError
            """PRECIO BASE DESARROLLO------------------------------------------------------- """
            precio_nominal = float(budget.precio_nominal)
            precio_baseDesarrollo = precio_nominal*meses_proyecto
            budget.precio_baseDesarrollo = precio_baseDesarrollo



            # """GASTOS ADMIN """
            objetos2 = AdministrativeExpensesHeader.objects.filter(type_project=budget.tipo_proyecto)
            cantidad_modules = len(objetos2)
            gastos_adminitrativos =0
            total_horasAdministrativas=0
            
            if(cantidad_modules>0):
                
                if(cantidad_modules == 1):
                    obj_detail = AdministrativeExpensesDetail.objects.filter(administrative_expenses_header = objetos2[0].pk)
                    for module in obj_detail:
                        gastos_adminitrativos = + module.total_quantity
                        total_horasAdministrativas = + module.total_hours
                        print("----------------", module.total_quantity, module.total_hours) 
                else:         
                    for point in objetos2:
                        obj_detail = AdministrativeExpensesDetail.objects.filter(administrative_expenses_header = point.pk)
                        for module in obj_detail:
                            gastos_adminitrativos = + module.total_quantity
                            total_horasAdministrativas = + module.total_hours
                            print("----------------", module.total_quantity, module.total_hours) 
            else:
                gastos_adminitrativos =0
                total_horasAdministrativas=0                              
            print("ssssssssssssssssssssssssssssssssssss", objetos2)

            budget.gastos_adminitrativos=  gastos_adminitrativos
            """TOTAL HORAS ADMIN """
            budget.total_horasAdministrativas=  total_horasAdministrativas
     

            """TOTAL SEMANAS ADMIN-------------------------------------- --------------------"""
            if(semanas_proyecto == 0):
                total_semanasAdministrativas = 0
            else:
                # total_semanasAdministrativas = 0
                total_semanasAdministrativas= total_horasAdministrativas/semanas_proyecto
            print("aaaaafdsfsddddddfff-------------------------------")
            budget.total_semanasAdministrativas = total_semanasAdministrativas
            """DURACION TOTAL PROYECTO """
            duracion_totalProyecto = semanas_proyecto + total_semanasMargenError + total_semanasAdministrativas
            budget.duracion_totalProyecto = duracion_totalProyecto
            """TOTAL COMISION CANAL """
            total_comisionCanal = precio_baseDesarrollo*canal_venta.commission_percentage
            budget.total_comisionCanal = total_comisionCanal
            """TOTAL COMISION VENDEDOR """
            total_comisionVendedor = precio_baseDesarrollo*vendedor_new.vendedor_percentage
            budget.total_comisionVendedor = total_comisionVendedor
            """PRECIO TOTAL """
            precio_total = precio_baseDesarrollo + gastos_adminitrativos + total_comisionVendedor+total_comisionCanal
            budget.precio_total = precio_total
            budget.save()

            serializer = BudgetCreateAll(budget)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({"detail": str(e)}, status=status.HTTP_400_BAD_REQUEST)