"""Manage Leads"""

# rest_framework
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

# modelos
from api.models import SalesFunnel,


class ManageLeadsViewset(viewsets.GenericViewSet):
    """Will manage all the request from de manage leads view"""
    @action(detail=False, methods=['get'])
    def stages_sales(self, request, *args, *kwargs):
