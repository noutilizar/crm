# rest framework
from rest_framework import viewsets
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import AllowAny
from rest_framework import status
# django
from django.db import transaction
# models
from api.models import AnnualGoal, PercentageGoals, BusinessLines
# Utils
from api.utils.bitacora import RegistroBitacora
from copy import deepcopy
# serializer
from api.serializers import (ReadAnnualGoalSerializer,
                             AnnualGoalSerializer,
                             PercentageGoalSerializer)


class AnnualGoalViewSet(viewsets.ModelViewSet):
    queryset = AnnualGoal.objects.filter(is_active=True)
    filter_backends = (SearchFilter, OrderingFilter, DjangoFilterBackend)
    filter_fields = ('annual_goal', 'is_active_goal')
    search_fields = ('annual_goal')
    ordering_fields = ('annual_goal', 'is_active_goal')
    permission_classes = [AllowAny]

    def get_serializer_class(self):
        """Setting serializer """
        if self.action == "retrieve":
            return ReadAnnualGoalSerializer
        return AnnualGoalSerializer

    def create(self, request, *args, **kwargs):
        try:
            with transaction.atomic():
                lines_data = request.data['lines']
                annual_data = request.data
                del annual_data['lines']
                goal_serializer = AnnualGoalSerializer(data=annual_data)
                percentage_serializer = PercentageGoalSerializer(
                    data=lines_data, many=True)
                if (goal_serializer.is_valid() and percentage_serializer.is_valid()):
                    goal = goal_serializer.data
                    if goal['is_active_goal']:
                        queryset = AnnualGoal.objects.filter(
                            is_active_goal=True)
                        if queryset.exists():
                            for g in queryset:
                                g.is_active_goal = False
                                g.save()
                    annual_goal = AnnualGoal.objects.create(
                        annual_goal=goal['annual_goal'],
                        goal_quetzales=goal['goal_quetzales'],
                        is_active_goal=goal['is_active_goal'],
                        goal_first_trimester=goal['goal_first_trimester'],
                        goal_first_trimester_quetzal=goal['goal_first_trimester_quetzal'],
                        goal_second_trimester=goal['goal_second_trimester'],
                        goal_second_trimester_quetzal=goal['goal_second_trimester_quetzal'],
                        goal_third_trimester=goal['goal_third_trimester'],
                        goal_third_trimester_quetzal=goal['goal_third_trimester_quetzal'],
                        goal_fourth_trimester=goal['goal_fourth_trimester'],
                        goal_fourth_trimester_quetzal=goal['goal_fourth_trimester_quetzal']
                    )
                    for line in lines_data:
                        business_line = BusinessLines.objects.get(
                            pk=line['id'])

                        percentage_goal = PercentageGoals.objects.create(
                            percentage=line['percentage'], percentage_quetzal=line['percentage_quetzal'],
                            annual_goal=annual_goal, business_lines=business_line
                        )
                    # Registro a bitacora
                    # annual_serializer = AnnualGoalSerializer(annual_goal)
                    # RegistroBitacora.register(self.request.user,annual_serializer.data)
                    return Response(goal_serializer.data,
                                    status=status.HTTP_201_CREATED)
                else:
                    return Response({'detail': "Error al guardar"},
                                    status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'detail': str(e)},
                            status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):
        try:
            with transaction.atomic():
                instance = self.get_object()
                previous = deepcopy(instance)
                previous_data = AnnualGoalSerializer(previous)
                lines_data = request.data['lines']
                annual_data = request.data
                del annual_data['lines']
                goal_serializer = AnnualGoalSerializer(data=annual_data)
                percentage_serializer = PercentageGoalSerializer(
                    data=lines_data, many=True)
                if (goal_serializer.is_valid() and
                        percentage_serializer.is_valid()):
                    goal = goal_serializer.data
                    if goal['is_active_goal']:
                        queryset = AnnualGoal.objects.filter(
                            is_active_goal=True)
                        if queryset.exists():
                            for g in queryset:
                                g.is_active_goal = False
                                g.save()
                    instance.annual_goal = goal['annual_goal']
                    instance.goal_quetzales = goal['goal_quetzales']
                    instance.is_active_goal = goal['is_active_goal']
                    instance.goal_first_trimester = goal['goal_first_trimester']
                    instance.goal_first_trimester_quetzal = goal['goal_first_trimester_quetzal']
                    instance.goal_second_trimester = goal['goal_second_trimester']
                    instance.goal_second_trimester_quetzal = goal['goal_second_trimester_quetzal']
                    instance.goal_third_trimester = goal['goal_third_trimester']
                    instance.goal_third_trimester_quetzal = goal['goal_third_trimester_quetzal']
                    instance.goal_fourth_trimester = goal['goal_fourth_trimester']
                    instance.goal_fourth_trimester_quetzal = goal['goal_fourth_trimester_quetzal']
                    instance.save()
                    for line in lines_data:
                        percentage_goal = PercentageGoals.objects.get(
                            business_lines=line['id'],
                            annual_goal=instance.id)
                        percentage_goal.percentage = line['percentage']
                        percentage_goal.percentage_quetzal = line['percentage_quetzal']
                        percentage_goal.save()
                    # Registro bitacora
                    """ annual_serializer = AnnualGoalSerializer(isinstance)
                    RegistroBitacora.register(self.request.user,
                                              annual_serializer.data,
                                              previous_data.data) """
                    return Response(goal_serializer.data, status=status.HTTP_200_OK)
                else:
                    return Response({'detail': "Datos a actualizar enviados incorrectamente"},
                                    status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(str(e))
            return Response({'detail': str(e)},
                            status=status.HTTP_404_NOT_FOUND)

    def perform_destroy(self, instance):
        instance.delete()
        instance.save()
        # Registro a bitacora
        # serializer = AnnualGoalSerializer(instance)
        # RegistroBitacora.register(self.request.user, serializer.data)
