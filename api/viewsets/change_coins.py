"""Viewsets"""
# rest_framework
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework import viewsets
# models
from rest_framework.response import Response
from api.models import ChangeCoin
from api.utils.bitacora import RegistroBitacora
# seralizers
from api.serializers import ChangeCoinSerializer
from api.serializers import ReadChangeCoinSerializer, EditChangeCoinSerializer
# copy
from copy import deepcopy


class ChangeCoinViewSets(viewsets.ModelViewSet):
    queryset = ChangeCoin.objects.filter(is_active=True)
    filter_backends = (SearchFilter, OrderingFilter, DjangoFilterBackend)
    search_fields = ('coin_type', 'coin_code')
    ordering_fields = ('coin_type', 'coin_code')

    def get_serializer_class(self):
        """Define serializer for API"""
        if self.action in ['create']:
            return ChangeCoinSerializer
        elif self.action in ['update']:
            return EditChangeCoinSerializer
        else:
            return ReadChangeCoinSerializer

    def create(self, request, *args, **kwargs):
        coin_type = request.data['coin_type']['label']
        coin_code = request.data['coin_type']['value']
        request.data['coin_type'] = coin_type
        request.data['coin_code'] = coin_code
        return super(ChangeCoinViewSets, self).create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save()
        # Guardaremos la accion en bitacora
        # RegistroBitacora.register(self.request.user, serializer.data)

    def update(self, request, *args, **kwargs):
        coin_type = request.data['coin_type']['label']
        coin_code = request.data['coin_type']['value']
        request.data['coin_type'] = coin_type
        request.data['coin_code'] = coin_code
        return super(ChangeCoinViewSets, self).update(request, *args, **kwargs)

    def perform_update(self, serializer):
        previous = deepcopy(self.get_object())
        previous_data = EditChangeCoinSerializer(previous)
        serializer.save()
        # RegistroBitacora.register(
        # self.request.user,serializer.data, previous_data.data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        coin_type = {
            'label': serializer.data['coin_type'], 'value': serializer.data['coin_code']}
        change_type = serializer.data['change_type']
        data = {'id': serializer.data['id'],
                'change_type': change_type, 'coin_type': coin_type}
        return Response(data)

    def perform_destroy(self, instance):
        instance.delete()
        instance.save()
        #serializer = ReadChangeCoinSerializer(instance)
        #RegistroBitacora.register(self.request.user, serializer.data)
