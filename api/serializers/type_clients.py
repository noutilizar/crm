"""Type of Clients Serelializer"""
# rest framework

from rest_framework import serializers
from rest_framework.validators import UniqueValidator 
#model
from api.models.type_clients import TypeClient

class TypeClientModelSerializer( serializers.ModelSerializer ):

    class Meta:
        model  = TypeClient
        fields = '__all__'

class TypeClientRegisterSerializer(serializers.ModelSerializer):

    """name_type = serializers.CharField(
        validators=[
            UniqueValidator(queryset=TypeClient.objects.filter(is_active=True), message='The name must be unique')]
    )"""
    class Meta:
        model = TypeClient
        fields = (
            "name_type",
            "rate_for_hour",
            "rate_for_hour_priority_support",
            "rate_for_hour_support_normal",
            "rate_for_month",
        )