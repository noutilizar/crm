""" SERIALIZER Leads """

# Djnago REST framework
from rest_framework import serializers


# Model
from api.models.leads import Leads

class LeadModelSerializer( serializers.ModelSerializer ):
    
    class Meta:
        model  = Leads
        fields = '__all__'

class LeadReadSerializer( serializers.ModelSerializer ):

    sales_funnel = serializers.SerializerMethodField('obj_sales_funnel')
    coin_lead = serializers.SerializerMethodField('obj_coin_lead')
    owner_lead = serializers.SerializerMethodField('obj_owner_lead')
    company = serializers.SerializerMethodField('obj_company')
    contact = serializers.SerializerMethodField('obj_contact')
    sales_channel = serializers.SerializerMethodField('obj_sales_channel')
    business_stage = serializers.SerializerMethodField('obj_business_stage')

    class Meta:
        model = Leads
        fields = (
            'name',
            'amount',
            'quetzal',
            'closing_date',
            'possibility_of_closure',
            'description',
            'sales_funnel',
            'coin_lead',
            'owner_lead',
            'company',
            'contact',
            'sales_channel',
            'business_stage',
            'cycle_days'
            )

    def obj_sales_funnel(self, obj):
        return {'value': obj.sales_funnel.id, 'label': obj.sales_funnel.name}


    def obj_coin_lead(self, obj):
        return {
                'value': obj.coin_lead.id, 
                'label': obj.coin_lead.coin_type, 
                'change_type': obj.coin_lead.change_type
            }

    def obj_owner_lead(self, obj):
        return {'value': obj.owner_lead.id, 'label': obj.owner_lead.username}

    def obj_company(self, obj):
        return {'value': obj.company.id, 'label': obj.company.name}

    def obj_contact(self, obj):
        return {'value': obj.contact.id, 'label': obj.contact.name}

    def obj_sales_channel(self, obj):
        return {'value': obj.sales_channel.id, 
                'label': obj.sales_channel.name, 
                'percentage': obj.sales_channel.commission_percentage}

    def obj_business_stage(self, obj):
        return {'value': obj.business_stage.id, 'label': obj.business_stage.name}

class LeadReportSerializer( serializers.ModelSerializer ):

    business_stage = serializers.StringRelatedField()

    class Meta:
        model = Leads
        fields = ('name','business_stage','quetzal')