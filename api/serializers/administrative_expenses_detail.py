""" SERIALIZER ADMINISTRATIVE_EXPENSES_DETAIL """

# Django Rest Framework
from rest_framework import serializers

# Model
from api.models.administrative_expenses_detail import AdministrativeExpensesDetail

class AdministrativeExpensesDetailList(serializers.ModelSerializer):

    class Meta:
        model  = AdministrativeExpensesDetail
        fields = '__all__'



class AdministrativeExpensesDetailCreate(serializers.ModelSerializer):

    class Meta:
        model  = AdministrativeExpensesDetail
        fields = (
            'name_expense',
            'number_hours',
            'total_quantity',
            'rate_hour',
            'total_hours',
            'administrative_expenses_header',
            
        )

