from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from api.models.budgets import Budget
from api.models.administrative_expenses_header import AdministrativeExpensesHeader
from api.models.administrative_expenses import AdministrativeExpenses
#Serializer
from api.serializers.budget_relation_module import BudgetRelationModulesSerializer
from api.serializers.administrative_expenses_detail import AdministrativeExpensesDetail
from api.serializers.administrative_expenses import AdministrativeExpensesSerializer, AdministrativeExpensesSerializerList
class BudgetCreate(ModelSerializer):
    """ Serializer para crear y actualizar presupuestos"""
    class Meta:
        model = Budget
        fields = ['lead','tipo_cliente','tipo_proyecto','cantidad_programadores','porcentaje_error',
        'precio_nominal','canal_venta', 'vendedor']

class BudgetCreateAll(ModelSerializer):
    """ Serializer para crear y actualizar presupuestos"""
    class Meta:
        model = Budget
        fields = ['pk','lead','tipo_cliente','tipo_proyecto','tipo_cliente','cantidad_programadores','cantidad_diseñadores','porcentaje_error',
        'precio_nominal','canal_venta', 'vendedor', 
        'porcentaje_comisionCanalVenta',
        'porcentaje_comisionVendedor',
        
        'total_comisionCanal','total_comisionVendedor',
        
        'tiempo_total','semanas_proyecto',
        'meses_proyecto',


        'total_horasProyecto',


        'total_semanasMargenError',
        'total_horasMargenError',
        

        'precio_baseDesarrollo',
        
        
        'gastos_adminitrativos',
        'total_horasAdministrativas',
        
        
        'total_semanasAdministrativas', 
        
        
        'total_mesesAdministrativas',


        
        'duracion_totalProyecto','precio_total']

        
        



class BudgetList(ModelSerializer):
    """Serializer para listar presupuestos """
    budget_relation_modules =BudgetRelationModulesSerializer(many = True)
    
    class Meta:
        model= Budget
        fields = ['id','lead','tipo_cliente','tipo_proyecto','cantidad_programadores','porcentaje_error', 'cantidad_diseñadores',
        'precio_nominal','canal_venta', 'vendedor', 'duracion_totalProyecto', 'precio_total', 'budget_relation_modules',
        'adminmodules'
        ]
        depth = 1


