from rest_framework.serializers import ModelSerializer
from api.models.vendedor import Vendedor

class VendedorCreate(ModelSerializer):
    """ Serializer para crear y actualizar """
    class Meta:
        model = Vendedor
        fields = ['nombre','apellido','direccion','celular',
        'email', 'vendedor_percentage']

class VendedorList(ModelSerializer):
    """Serializer para listar """
    class Meta:
        model= Vendedor
        fields = ['id','nombre','apellido', 'vendedor_percentage']
        