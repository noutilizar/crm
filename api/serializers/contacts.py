""" SERIALIZER Contacts """

# Djnago REST framework
from rest_framework import serializers
from rest_framework.validators import UniqueValidator 

# Model
from api.models.contacts import Contact 

class ContactModelSerializer( serializers.ModelSerializer ):

    class Meta:
        model  = Contact
        fields = '__all__'

class ContactRetrieveModelSerializer( serializers.ModelSerializer ):

    company = serializers.SerializerMethodField("obj_company")
    life_cycles = serializers.SerializerMethodField("obj_life_cycles")
    userowner = serializers.SerializerMethodField("obj_userowner")


    class Meta:
        model  = Contact
        fields = (
            'id',
            'name',
            'email',
            'phone_staff',
            'phone_business',
            'position',
            'client_type',
            'company',
            'life_cycles',
            'userowner'
        )
    
    def obj_company(self, obj):
        if obj.company:
            return {'value': obj.company.id, 'label': obj.company.name}
        return {}


    def obj_life_cycles(self, obj):
        if obj.life_cycles:
            return {'value': obj.life_cycles.id, 'label': obj.life_cycles.name}
        return {}

    def obj_userowner(self, obj):
        if obj.userowner:
            return {'value': obj.userowner.id, 'label': obj.userowner.username}
        return {}
    




class ContactReadModelSerializer( serializers.ModelSerializer ):

    company = serializers.StringRelatedField(read_only=True)
    life_cycles = serializers.StringRelatedField(read_only=True)
    userowner = serializers.StringRelatedField(read_only=True)


    class Meta:
        model  = Contact
        fields = (
            'id',
            'name',
            'email',
            'phone_staff',
            'phone_business',
            'position',
            'client_type',
            'company',
            'life_cycles',
            'userowner'
        )