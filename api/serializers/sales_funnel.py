import json
from rest_framework import serializers
from api.models import SalesFunnel, PercentageGoals
from django.db.models import F


class SalesFunnelSerializer(serializers.ModelSerializer):
    business_lines = serializers.SerializerMethodField("getBusinessLine")
    sales_channel = serializers.SerializerMethodField("getSalesChannel")
    stages_sales = serializers.SerializerMethodField(source="stage_sales")
    ordering = serializers.SerializerMethodField(source="ordering")

    class Meta:
        model = SalesFunnel
        fields = '__all__'

    def getBusinessLine(self, obj):
        return {'value': obj.business_lines.id, 'label': obj.business_lines.name}

    def getSalesChannel(self, obj):
        return {'value': obj.sales_channel.id, 'label': obj.sales_channel.name}

    def get_stages_sales(self, data):
        query = data.stage_sales.filter(is_active=True).values(
            'id', 'name', 'percentage', 'leads_ordering')
        if query.exists():
            return query
        data = []
        return data

    def get_ordering(self, data):
        ordering = json.loads(data.ordering)
        return ordering


class SalesFunnelRegistroSerializer(serializers.ModelSerializer):
    class Meta:
        model = SalesFunnel
        fields = ('name', 'percentage_line')


class ReadFunnelSerializer(serializers.ModelSerializer):

    class Meta:
        model = SalesFunnel
        fields = ('id', 'name')
