""" SERIALIZER ADMINISTRATIVE_EXPENSES """

# Django Rest Framework
from rest_framework import serializers

# Model
from api.models.administrative_expenses import AdministrativeExpenses

class AdministrativeExpensesSerializer(serializers.ModelSerializer):

    class Meta:
        model  = AdministrativeExpenses
        fields = '__all__'


class AdministrativeExpensesRegistroSerializer(serializers.ModelSerializer):

    class Meta:
        model  = AdministrativeExpenses
        fields = (
            'name_expense',
            'number_hours',
            'total_quantity',
            'rate_hour',
            'total_hours',
            'budget',
        )
class AdministrativeExpensesSerializerList(serializers.ModelSerializer):
    
    class Meta:
        model  = AdministrativeExpenses
        fields = '__all__'
