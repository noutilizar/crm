""" SERIALIZER Customer life cycle """

# Djnago REST framework
from rest_framework import serializers

# Model
from api.models.lifecycles import  LifeCycle

class LifeCycleModelSerializer( serializers.ModelSerializer ):

    class Meta:
        model  = LifeCycle
        fields = ('id', 'name')