""" SERIALIZER Companies """

# Djnago REST framework
from rest_framework import serializers

# Model
from api.models.companies import  Company

class CompanyModelSerializer( serializers.ModelSerializer ):

    class Meta:
        model  = Company
        fields = '__all__'

class CompanyRetrieveModelSerializer( serializers.ModelSerializer ):

    industry = serializers.SerializerMethodField("obj_industry")

    class Meta:
        model  = Company
        fields = (
            'id',
            'name',
            'cities',
            'employee_number',
            'annual_income',
            'phone_number',
            'description',
            'linkedIn_page',
            'industry',
            'facebook_page'
        )
    
    def obj_industry(self, obj):
        return {'value': obj.industry.id, 'label': obj.industry.name}


class CompanyReadModelSerializer( serializers.ModelSerializer ):

    industry = serializers.StringRelatedField(read_only=True)


    class Meta:
        model  = Company
        fields = (
            'id',
            'name',
            'cities',
            'employee_number',
            'annual_income',
            'phone_number',
            'description',
            'linkedIn_page',
            'industry',
            'facebook_page'
        )
