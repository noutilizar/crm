"""Serializer"""
# rest framewrok
from rest_framework import serializers
from api.models import ChangeCoin


class ChangeCoinSerializer(serializers.ModelSerializer):
    def validate(self, data):
        coin_code = data['coin_code']
        query = ChangeCoin.objects.filter(is_active=True, coin_code=coin_code)
        if query.exists():
            raise serializers.ValidationError('Tipo de Cambio ya registrado!')
        return data

    class Meta:
        model = ChangeCoin
        fields = ('coin_type', 'coin_code', 'change_type')


class ReadChangeCoinSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChangeCoin
        fields = "__all__"


class EditChangeCoinSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChangeCoin
        fields = "__all__"
