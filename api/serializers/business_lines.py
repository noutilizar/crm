from rest_framework import serializers
from api.models import BusinessLines
from rest_framework.validators import UniqueValidator
import pdb

class BusinessLSerializer(serializers.ModelSerializer):
    percentage_line = serializers.SerializerMethodField(source="percentage_goals")

    def get_percentage_line(self,data):
        valor = 0
        queryset = data.percentage_goals.filter(annual_goal__is_active_goal=True).values('percentage')
        if queryset.exists():
            valor = queryset[0]['percentage']
        return valor
    class Meta:
        model = BusinessLines
        fields = '__all__'

    
class BusinessLRegisterSerializer(serializers.ModelSerializer):

    name = serializers.CharField(
        validators=[
            UniqueValidator(queryset=BusinessLines.objects.filter(is_active=True), message='The name must be unique')]
    )
    class Meta:
        model = BusinessLines
        fields = ('name', 'description')

class EditBusinessLinesSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessLines
        fields = ('name', 'description')