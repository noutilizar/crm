# rest framework

from rest_framework import serializers

# model
from api.models import AnnualGoal, PercentageGoals
# django
from django.db.models import F

import pdb


class ReadAnnualGoalSerializer(serializers.ModelSerializer):
    lines = serializers.SerializerMethodField(
        source='percentage_goals')

    def get_lines(self, data):
        """Getting array data of lines"""
        data_array = data.percentage_goals.all(
        ).values('percentage',
                 'percentage_quetzal'
                 ).annotate(
            id=F('business_lines__id'),
            name=F('business_lines__name'))
        return data_array

    class Meta:
        model = AnnualGoal
        fields = ('id', 'annual_goal', 'goal_quetzales',
                  'is_active_goal', 'goal_first_trimester',
                  'goal_second_trimester', 'goal_third_trimester',
                  'goal_fourth_trimester', 'goal_first_trimester_quetzal',
                  'goal_second_trimester_quetzal',
                  'goal_third_trimester_quetzal',
                  'goal_fourth_trimester_quetzal', 'lines',)
        ordering = ['-is_active_goal', '-annual_goal']


class AnnualGoalSerializer(serializers.ModelSerializer):

    class Meta:
        model = AnnualGoal
        fields = "__all__"


class PercentageGoalSerializer(serializers.ModelSerializer):

    class Meta:
        model = PercentageGoals
        fields = ('percentage', 'percentage_quetzal')
