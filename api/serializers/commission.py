"""Serializer Commissions"""

from rest_framework import serializers
#model
from api.models.commission import Commission 

class CommissionModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Commission
        fields='__all__'

class CommissionRegisterSerializer(serializers.ModelSerializer):

    name_user = serializers.SerializerMethodField('obj_name_user')

    class Meta:
        model=Commission
        fields=(
            'name_user',
            'starting_range',
            'final_rank',
            'percentage_commission',
            'name_type'
        )

    def obj_name_user(self, obj):
        return {'value': obj.name_user.id, 'label': obj.name_user.username}

