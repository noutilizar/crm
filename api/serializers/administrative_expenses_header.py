""" SERIALIZER ADMINISTRATIVE_EXPENSES_HEADER """

# Django Rest Framework
from rest_framework import serializers

# Model
from api.models.administrative_expenses_header import AdministrativeExpensesHeader
from api.models.administrative_expenses_detail import AdministrativeExpensesDetail
#Serializer
from api.serializers.administrative_expenses_detail import AdministrativeExpensesDetailList

class AdministrativeExpensesHeaderList(serializers.ModelSerializer):
    modules = AdministrativeExpensesDetailList(many = True)
    class Meta:
        model  = AdministrativeExpensesHeader
        fields = '__all__'
        depth = 1 

class AdministrativeExpensesHeaderCreate(serializers.ModelSerializer):

    class Meta:
        model  = AdministrativeExpensesHeader
        fields = '__all__'

