from .change_coins import (
    ChangeCoinSerializer,
    ReadChangeCoinSerializer,
    EditChangeCoinSerializer)
from .sales_channel import (
    SalesChannelRegistroSerializer,
    SalesChannelSerializer)
from .change_coins import (
    ChangeCoinSerializer,
    ReadChangeCoinSerializer,
    EditChangeCoinSerializer)
from .business_lines import (
    BusinessLSerializer,
    BusinessLRegisterSerializer,
    EditBusinessLinesSerializer)
from .annuals_goals import (
    ReadAnnualGoalSerializer,
    AnnualGoalSerializer,
    PercentageGoalSerializer)
from .user import UserSerializer, UserReadSerializer, UserPermissionReadSerializer
from .sales_funnel import SalesFunnelSerializer, SalesFunnelRegistroSerializer, ReadFunnelSerializer
from .stage_sales import StageSalesSerializer, StageSalesRegistroSerializer
from .industries import IndustryModelSerializer, IndustryRegisterSerializer
from .companies import CompanyModelSerializer, CompanyReadModelSerializer, CompanyRetrieveModelSerializer
from .leads import LeadModelSerializer
from .lead_managements import ReadSalesFunnelSerializer
from .tasks import TaskModelSerializer, TaskRegisterSerializer
from .type_clients import TypeClientModelSerializer,TypeClientRegisterSerializer
from .configurations import ConfigurationSerializer,ConfigurationRegisterSerializer
from .type_project import TypeProjectModelSerializer,TypeProjectRegisterSerializer
from .commission import CommissionModelSerializer,CommissionRegisterSerializer
from .budgets import BudgetCreate, BudgetList
from .vendedor import VendedorCreate, VendedorList
from .administrative_expenses import AdministrativeExpensesSerializer, AdministrativeExpensesRegistroSerializer
from .budget_modules import BudgetModulesSerializer, BudgetModulesRegistroSerializer
from .budget_relation_module import BudgetRelationModulesSerializer 
from .administrative_expenses_header import AdministrativeExpensesHeaderList, AdministrativeExpensesHeaderCreate
from .administrative_expenses_detail import AdministrativeExpensesDetailList, AdministrativeExpensesDetailCreate
