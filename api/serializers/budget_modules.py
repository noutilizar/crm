""" SERIALIZER BUDGET_MODULES """

# Django Rest Framework
from rest_framework import serializers

# Model
from api.models.budget_modules import BudgetModules

class BudgetModulesSerializer(serializers.ModelSerializer):

    class Meta:
        model  = BudgetModules
        fields = '__all__'


class BudgetModulesRegistroSerializer(serializers.ModelSerializer):

    class Meta:
        model  = BudgetModules
        fields = (
            'name_modules',
            'history_points',
            'history_points_error',
            'total_history_points',
            'total_start_hours',
            'total_start_hours_error',
            'total_hours',
        )