"""Serializer Tasks"""

# Django REST framework
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

# Model
from api.models import Tasks

class TaskModelSerializer( serializers.ModelSerializer ):

    lead = serializers.SerializerMethodField('getLead')
    
    class Meta:
        model  = Tasks
        fields = '__all__'

    def getLead(self, obj):
        return {'value': obj.lead.id, 'label': obj.lead.name}


class TaskRegisterSerializer( serializers.ModelSerializer ):

    name = serializers.CharField(
        validators=[
            UniqueValidator(queryset=Tasks.objects.filter(is_active=True), message='The name must be unique')]
    )
    
    class Meta:
        model  = Tasks
        fields = '__all__'
