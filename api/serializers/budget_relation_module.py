""" SERIALIZER BUDGET_RELATION_MODULES """

# Django Rest Framework
from rest_framework import serializers

# Model
from api.models.budget_relation_module import BudgetRelationModule

class BudgetRelationModulesSerializer(serializers.ModelSerializer):

    class Meta:
        model  = BudgetRelationModule
        fields = '__all__'
