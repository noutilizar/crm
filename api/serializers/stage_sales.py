from rest_framework import serializers
from api.models import StageSales


class StageSalesSerializer(serializers.ModelSerializer):
    sale = serializers.SerializerMethodField("getSale")

    class Meta:
        model = StageSales
        fields = '__all__'

    def getSale(self, obj):
        return {'value': obj.sale.id, 'label': obj.sale.name}

class StageSalesRegistroSerializer(serializers.ModelSerializer):
    class Meta:
        model = StageSales
        fields = ('name', 'order')
