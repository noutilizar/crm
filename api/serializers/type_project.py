"""Type Project Serializer"""

#rest framework
from rest_framework import serializers
from rest_framework.validators import UniqueValidator 

#model

from api.models.type_project import TypeProject

class TypeProjectModelSerializer( serializers.ModelSerializer ):

    class Meta:
        model  = TypeProject
        fields = '__all__'


class TypeProjectRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model=TypeProject
        fields=(
            'name_project',
            'name_type',
            'average_sprint',
            'error_range',
            'month_support',
            'hour_for_support_month',
            'hour_implementation',
            'hour_for_QA',
            'hour_userManual',
            'hour_technicalManual',
            'hour_deployment'
        )
