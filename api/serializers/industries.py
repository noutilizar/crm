""" SERIALIZER industries """

# Djnago REST framework
from rest_framework import serializers
from rest_framework.validators import UniqueValidator 

# Model
from api.models.industries import Industry

class IndustryModelSerializer( serializers.ModelSerializer ):

    class Meta:
        model  = Industry
        fields = '__all__'

class IndustryRegisterSerializer(serializers.ModelSerializer):

    name = serializers.CharField(
        validators=[
            UniqueValidator(queryset=Industry.objects.filter(is_active=True), message='The name must be unique')]
    )
    class Meta:
        model = Industry
        fields = '__all__'