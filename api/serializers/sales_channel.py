from rest_framework import serializers
from api.models import SalesChannel


class SalesChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = SalesChannel
        fields = '__all__'


class SalesChannelRegistroSerializer(serializers.ModelSerializer):
    class Meta:
        model = SalesChannel
        fields = ('name', 'commission_percentage')
