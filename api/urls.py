from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token
from django.conf.urls import url
from api import viewsets


router = DefaultRouter()
router.register(r'user', viewsets.UserViewset)
router.register(r'config/change-coin', viewsets.ChangeCoinViewSets)
router.register(r'business-lines', viewsets.BusinessLViewset)
router.register(r'industry', viewsets.IndustryViewSet)
router.register(r'sales-funnel', viewsets.SalesFunnelViewSet)
router.register(r'sales_channel', viewsets.SalesChannelViewset)
router.register(r'stage-sale', viewsets.StageSalesViewSet)
router.register(r'role', viewsets.RoleViewSet)

router.register(r'role', viewsets.RoleViewSet)
router.register(r'permission', viewsets.PermissionViewSet)
router.register(r'lifecycle', viewsets.LifeCycleViewSet)
router.register(r'company', viewsets.CompanyViewSet)
router.register(r'contacts', viewsets.ContactViewSet)
router.register(r'leads', viewsets.LeadViewSet)
router.register(r'tasks', viewsets.TaskViewSet)
router.register(r'reportweek', viewsets.ReportWeekViewSet)

router.register(r'sales_channel', viewsets.SalesChannelViewset)
router.register(r'annual-goal', viewsets.AnnualGoalViewSet)
router.register(r'lead-management', viewsets.LeadManagementViewset)
router.register(r'type_client',viewsets.TypeClientViewSet)
router.register(r'configurations',viewsets.ConfigurationViewSet)
router.register(r'type_project',viewsets.TypeProjectViewSet)
router.register(r'commissions',viewsets.CommissionViewSet)
router.register(r'budget',viewsets.BudgetViewSet)
router.register(r'vendedor',viewsets.VendedorViewSet)

router.register(r'administrative_expenses',viewsets.AdministrativeExpensesViewSet)
router.register(r'budget_modules',viewsets.BudgetModulesViewSet)
router.register(r'budget_relation_modules',viewsets.BudgetRelationModulesViewSet)


router.register(r'administrative_expenses_detail',viewsets.AdministrativeExpensesDetail)
router.register(r'administrative_expenses_header',viewsets.AdministrativeExpensesHeader)
urlpatterns = [
    path('api/', include(router.urls)),
    url(r"^api/token", obtain_auth_token, name="api-token"),
    path('api-auth/', include('rest_framework.urls')),
]
